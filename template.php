<?php
	require "system/fungsi_waktu.php";
	require "system/koneksi.php";
	require "system/z_setting.php";
?>

<!DOCTYPE html>
<html>
<head>

	<?php require "system/seo_v4.php";?>

	<link rel="icon" type="image/x-icon" href="images/<?= $iconWebsite; ?>" />

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" id="font-awesome-css" href="assets/css/font-awesome.min.css" type="text/css" media="all">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">

</head>
<body>

	<?php require "inc/menu.php"; ?>
	<?php require "inc/content.php"; ?>
	<?php require "inc/footer.php"; ?>

	<!-- GetButton.io widget -->
	<script type="text/javascript">
	    (function () {
	        var options = {
	            whatsapp: "+62816907019", // WhatsApp number
	            call_to_action: "Bantuan", // Call to action
	            position: "left", // Position may be 'right' or 'left'
	        };
	        var proto = document.location.protocol, host = "getbutton.io", url = proto + "//static." + host;
	        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
	        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
	        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
	    })();
	</script>
	<!-- /GetButton.io widget -->

	<!-- Asset -->

	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>

	<script src="https://www.google.com/recaptcha/api.js" async defer></script>

	<script type="text/javascript">

		// Popover
		$(document).ready(function(){
  			$('[data-toggle="popover"]').popover();   
		});
		// Popover

	</script>

	<!-- Asset -->

</body>
</html>