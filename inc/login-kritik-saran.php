<?php

    session_start();

    // Include file gpconfig
    include_once "gpconfig-kritik-saran.php";

    if(isset($_GET["code"])){
        $gclient->authenticate($_GET["code"]);
        $_SESSION["token"] = $gclient->getAccessToken();
        header("Location: " . filter_var($redirect_url, FILTER_SANITIZE_URL));
    }

    if (isset($_SESSION["token"])) {
        $gclient->setAccessToken($_SESSION["token"]);
    }

    if ($gclient->getAccessToken()) {

        require "../system/koneksi.php";

        // Get vote profile data from google
        $gpuserprofile = $google_oauthv2->userinfo->get();

        $ambilNama = $gpuserprofile["given_name"]." ".$gpuserprofile["family_name"]; // Ambil nama dari Akun Google
        $namaAkun   = $ambilNama;
        $email = $gpuserprofile["email"]; // Ambil email Akun Google nya

        // Ambil nama dari kata sebelum simbol @ pada email
        $ex     = explode("@", $email); // Pisahkan berdasarkan "@"
        $nama   = $ex[0]; // Ambil kata pertama
        $extension_email   = $ex[1]; // Ambil kata pertama
        $nim    = substr($nama, -10);
        $nimTIF = substr($nim, 5, 2);

        // Mengecek apakah mahasiswa UAD atau Bukan!!!
        if ($extension_email!="webmail.uad.ac.id") {
            echo "<script>alert('Mohon gunakan Email UAD!!!'); window.location = 'https://kpump-tif.arpateam.com/inc/gagal-login-email.php';</script>";
            exit();
        }

        // Mengecek apakah Mahasiswa Prodi Teknik Informatika atau Bukan!!!
        if ($nimTIF!="18") {
             echo "<script>alert('Anda Bukan Termasuk Prodi Teknik Informatika!!!'); window.location = 'https://kpump-tif.arpateam.com/inc/gagal-login-email.php';</script>";
            exit();
        }else{
            $CekStatusMhs = $pdo->query("SELECT data_mahasiswa_aktif.nim FROM data_mahasiswa_aktif WHERE nim='$nim'");
            $resultCekStatusMhs = $CekStatusMhs->fetch(PDO::FETCH_ASSOC);

            if (empty($resultCekStatusMhs['nim'])) {
                echo "<script>alert('Status Mahasiswa anda Non-Aktif. Anda tidak mempunyai Hak Pilih!!!'); window.location = 'https://kpump-tif.arpateam.com/inc/gagal-login-email.php';</script.php';</script>";
                exit();
            }
        }

        $_SESSION["email"]  = $email;
        $_SESSION["nim"]    = $nim;
        $_SESSION["nama"]   = $namaAkun;

        header('location: https://kpump-tif.arpateam.com/kritik-saran');

    }else{
        $authUrl = $gclient->createAuthUrl();
        header("location: ".$authUrl);
    }

?>