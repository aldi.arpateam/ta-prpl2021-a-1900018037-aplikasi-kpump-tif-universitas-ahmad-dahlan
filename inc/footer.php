<section style="height: 100%; width: 100%; box-sizing: border-box; background-color: #FFFFFF;">

    <style>

        @import url('https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap');
        hr.hr-footer-2-2 {
            margin: 0;
            border: 0;
            border-top: 1px solid rgba(0, 0, 0, 0.1);
        }
        .border-color-footer-2-2 {
            color: #c7c7c7;
        }
        .footer-info-space-footer-2-2 {
            padding-top: 3rem;
        }
        .list-footer-footer-2-2 {
            padding: 5rem 1rem 6rem 1rem;
        }
        .info-footer-footer-2-2 {
            padding-left: 1rem;
            padding-right: 1rem;
            padding-bottom: 3rem;
        }

        .info-footer-footer-2-2 a{
            text-decoration: none;
            font-weight: bolder;
            color: #c7c7c7;
        }
    </style>

    <div  style="font-family: 'Poppins', sans-serif;">

        <div class="border-color-footer-2-2 info-footer-footer-2-2">
            <div class="">              
                <hr class="hr-footer-2-2">
            </div>
            <div class="mx-auto d-flex flex-column flex-lg-row align-items-center footer-info-space-footer-2-2">
                <nav class="mx-auto d-flex flex-wrap align-items-center justify-content-center footer-responsive-space-footer-2-2">
                    <p style="margin: 0">Dibuat oleh Tim Programmer <a href="https://arpateam.com">#ARPATEAM</a></p>
                </nav>
            </div>
        </div>
    </div>

</section>