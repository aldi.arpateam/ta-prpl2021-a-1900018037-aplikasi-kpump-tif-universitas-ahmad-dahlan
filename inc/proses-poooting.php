<?php

	// Proses untuk melakukan pooting & datanya akan di simpan ke dalam database

	// Memulai sesi login
	session_start();

	$session_email	= $_SESSION['email'];

	$portal1 	= $pdo->query("SELECT * FROM vote WHERE email='$session_email'");
	$rportal1 	= $portal1->fetch(PDO::FETCH_ASSOC);

	$portal2 	= $pdo->query("SELECT * FROM vote_backup WHERE email='$session_email'");
	$rportal2 	= $portal2->fetch(PDO::FETCH_ASSOC);

	$portal3 	= $pdo->query("SELECT * FROM vote_backup_ke_3 WHERE email='$session_email'");
	$rportal3 	= $portal3->fetch(PDO::FETCH_ASSOC);

	// Kita cek apakah user sudah login atau belum
	// Cek nya dengan cara cek apakah terdapat session username atau tidak
	if(empty($rportal1['email'])){ // Jika tidak ada session username berarti dia belum login
		echo "<script>window.location = 'https://kpump-tif.arpateam.com';</script>";
		die();
	}
	if(empty($rportal2['email'])){ // Jika tidak ada session username berarti dia belum login
		echo "<script>window.location = 'https://kpump-tif.arpateam.com';</script>";
		die();
	}
	if(empty($rportal3['email'])){ // Jika tidak ada session username berarti dia belum login
		echo "<script>window.location = 'https://kpump-tif.arpateam.com';</script>";
		die();
	}


	// Proses pootingnya mulai dari sini

	$email 	= $_POST['in_email'];
	$wasup	= $_POST['in_wasup'];
	$wifi	= $_POST['in_wifi'];

	// Proses pilihannya
	if (empty($wasup) AND empty($wifi)) {
		$pilihan	= 3;
	}elseif (!empty($wasup) AND empty($wifi)) {
		$pilihan	= 1;
	}elseif (empty($wasup) AND !empty($wifi)) {
		$pilihan	= 2;
	}elseif (!empty($wasup) AND !empty($wifi)) {
		$pilihan	= 3;
	}

	try {

		// Input ke vote
		$sql = "UPDATE vote SET hasil_vote = :pilihan WHERE email = :email";
		$statement = $pdo->prepare($sql);
		$statement->bindParam(":email", $email, PDO::PARAM_INT);
		$statement->bindParam(":pilihan", $pilihan, PDO::PARAM_STR);
		$count = $statement->execute();

		// Input ke vote_backup
			$sql_backup = "UPDATE vote_backup SET hasil_vote = :pilihan WHERE email = :email";
			$statement_backup = $pdo->prepare($sql_backup);
			$statement_backup->bindParam(":email", $email, PDO::PARAM_INT);
			$statement_backup->bindParam(":pilihan", $pilihan, PDO::PARAM_STR);
			$count = $statement_backup->execute();

		// Input ke vote_backup_ke_3
			$sql_backup_ke_3 = "UPDATE vote_backup_ke_3 SET hasil_vote = :pilihan WHERE email = :email";
			$statement_backup_ke_3 = $pdo->prepare($sql_backup_ke_3);
			$statement_backup_ke_3->bindParam(":email", $email, PDO::PARAM_INT);
			$statement_backup_ke_3->bindParam(":pilihan", $pilihan, PDO::PARAM_STR);
			$count = $statement_backup_ke_3->execute();

		echo "<script>alert('Terimakasih sudah menyuarakan Hak Anda!'); window.location = 'https://kpump-tif.arpateam.com/vote'</script>";
				
	}catch(PDOException $e){
		echo "<script>window.alert('Gagal input, silahkan hubungi Tim Kami!'); window.location(history.back(-1))</script>";
	}