<?php

    // Halaman kritk & saran
    // Halaman ini juga di gunakan untuk memproses kritik & saran yang di masukkan oleh user

    session_start();

    $session_email  = $_SESSION['email'];
    $nim            = $_SESSION['nim'];
    $nama           = $_SESSION['nama'];

    // Kita cek apakah user sudah login atau belum
    // Cek nya dengan cara cek apakah terdapat session username atau tidak
    if(empty($session_email)){ // Jika tidak ada session username berarti dia belum login
        echo "<script>window.location = 'https://kpump-tif.arpateam.com';</script>";
        die();
    }
    if(empty($session_email)){ // Jika tidak ada session username berarti dia belum login
        echo "<script>window.location = 'https://kpump-tif.arpateam.com';</script>";
        die();
    }
    if(empty($session_email)){ // Jika tidak ada session username berarti dia belum login
        echo "<script>window.location = 'https://kpump-tif.arpateam.com';</script>";
        die();
    }


    // Cek apakah formnya sudah di isi?
    if (isset($_POST['submit'])) {

        // PROSES INPUTAN
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $secret = '6Lfc-rEUAAAAAN3W5ELKRg2RHEczj9f3xr2lx2mV';
        $response = file_get_contents($url.'?secret='.$secret.'&response='.$_POST['g-recaptcha-response'].'&remoteip='.$_SERVER['REMOTE_ADDR']);

        $data = json_decode($response);

        if ($data->success == true) {

            $kritik_saran   = $_POST['in_kritik_saran'];

            try {

                $stmt = $pdo->prepare("INSERT INTO kritik_saran
                        (nim,nama,kritik_saran,waktu)
                        VALUES(:nim,:nama,:kritik_saran, now() )" );
                            
                $stmt->bindParam(":nim", $nim, PDO::PARAM_STR);
                $stmt->bindParam(":nama", $nama, PDO::PARAM_STR);
                $stmt->bindParam(":kritik_saran", $kritik_saran, PDO::PARAM_STR);

                $count = $stmt->execute();

                $insertId = $pdo->lastInsertId();

                // Jika inputan berhasil

?>

<div class="p-4" style="font-family: 'Poppins', sans-serif;">    
    <div class="mx-auto d-flex align-items-center justify-content-center flex-column">
        <img class="img-empty-3-3" src="http://api.elements.buildwithangga.com/storage/files/2/assets/Empty%20State/EmptyState3/Empty-3-1.png" alt="">                       
        <div class="text-center w-100">
            <h1 class="title-text-empty-3-3">Terimakasih!</h1>
            <p class="caption-text-empty-3-3">Kritik & Saran ada berhasil masuk <br />ke sistem kami :-)</p>
        </div>
    </div>
</div>

<?php

            }catch(PDOException $e){
                echo "<script>window.alert('Gagal input, silahkan hubungi Tim Kami!'); window.location(history.back(-1))</script>";
                exit();
            }
        } else {
            echo "<script>window.alert('Captcha Salah!');window.location=('javascript:history.go(-1)');</script>";
        }

        // AKHIR PROSES INPUTAN
    }else{

?>

<div class="container">

    <div class="row">

        <div class="col-12">

            <form method="POST" action="" class="was-validated">
                    
                <!-- Modal body -->
                <div class="modal-body">

                    <div class="alert alert-primary alert-dismissible fade show" role="alert">
                        <i class="fa fa-file-text"></i> Yuk beri kritik & saran terhadap website ini!
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>

                    <div class="form-floating">
                        <textarea name="in_kritik_saran" class="form-control is-invalid" placeholder="Tulis kritik & saran anda disini..." id="floatingTextarea2" style="height: 150px" required></textarea>
                        <label for="floatingTextarea2">Tulis kritik & saran anda disini...</label>
                    </div>

                    <div class="text-center mt-4">
                        <div class="g-recaptcha" data-sitekey="6Lfc-rEUAAAAABsNHZg_fNWRJgdG50edntZlyo_t"></div>
                    </div>

                </div>
            
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" name="submit" class="btn btn-block btn-primary">Kirim <i class="fa fa-paper-plane"></i></button>
                </div>

            </form>

        </div>

    </div>

</div>

<?php } ?>