<?php

    error_reporting(0);
    session_start();

    // Include file gpconfig
    include_once "gpconfig.php";
    require "../system/fungsi_waktu.php";

    // Apakah sudah waktunya pemilihan?
    if (strtotime($sekarang)<strtotime($waktu_mulai) OR strtotime($sekarang)>strtotime($waktu_berakhir)) {
        echo "<script>alert('Bukan Saatnya Pemilihan!!!');</script>";
        // Membuang session yang ada
        $_SESSION = [];
        session_destroy();
        session_unset();
        echo "<script>window.history.go(-1);</script>";
        exit();
    }

    if(isset($_GET["code"])){
        $gclient->authenticate($_GET["code"]);
        $_SESSION["token"] = $gclient->getAccessToken();
        header("Location: " . filter_var($redirect_url, FILTER_SANITIZE_URL));
    }

    if (isset($_SESSION["token"])) {
        $gclient->setAccessToken($_SESSION["token"]);
    }

    if ($gclient->getAccessToken()) {

        require "../system/koneksi.php";

        // Get vote profile data from google
        $gpuserprofile = $google_oauthv2->userinfo->get();

        $nama = $gpuserprofile["given_name"]." ".$gpuserprofile["family_name"]; // Ambil nama dari Akun Google
        $namaAkun   = $nama;
        $email = $gpuserprofile["email"]; // Ambil email Akun Google nya

        // Buat query untuk mengecek apakah data vote dengan email tersebut sudah ada atau belum
        $sql = $pdo->prepare("SELECT * FROM vote WHERE email=:a");
        $sql->bindParam(":a", $email);
        $sql->execute(); // Eksekusi querynya

        $vote = $sql->fetch(); // Ambil datanya dari hasil query tadi

        if(empty($vote)){ // Jika User dengan email tersebut belum ada
            // Ambil nama dari kata sebelum simbol @ pada email
            $ex     = explode("@", $email); // Pisahkan berdasarkan "@"
            $nama   = $ex[0]; // Ambil kata pertama
            $extension_email   = $ex[1]; // Ambil kata pertama
            $nim    = substr($nama, -10);
            $nimTIF = substr($nim, 5, 2);
            $hasil_vote = NULL;
            
            // Mengecek apakah mahasiswa UAD atau Bukan!!!

            if ($extension_email!="webmail.uad.ac.id") {
                echo "<script>alert('Mohon gunakan Email UAD!!!'); window.location = 'https://kpump-tif.arpateam.com/inc/gagal-login-email.php';</script>";
                exit();
            }

            // Mengecek apakah Mahasiswa Prodi Teknik Informatika atau Bukan!!!

            if ($nimTIF!="18") {
                echo "<script>alert('Anda Bukan Termasuk Prodi Teknik Informatika!!!'); window.location = 'https://kpump-tif.arpateam.com/inc/gagal-login-email.php';</script>";
                exit();
            }else{
                $CekStatusMhs = $pdo->query("SELECT data_mahasiswa_aktif.nim FROM data_mahasiswa_aktif WHERE nim='$nim'");
                $resultCekStatusMhs = $CekStatusMhs->fetch(PDO::FETCH_ASSOC);

                if (empty($resultCekStatusMhs['nim'])) {
                    echo "<script>alert('Status Mahasiswa anda Non-Aktif. Anda tidak mempunyai Hak Pilih!!!'); window.location = 'https://kpump-tif.arpateam.com/inc/gagal-login-email.php';</script>";
                    exit();
                }else{
                    // Lakukan insert data vote
                    $sql = $pdo->prepare("INSERT INTO vote(nim, nama, email, hasil_vote, waktu) VALUES(:nim, :nama, :email, :hasil_vote, now())");
                    $sql->bindParam(":nim", $nim);
                    $sql->bindParam(":nama", $namaAkun);
                    $sql->bindParam(":email", $email);
                    $sql->bindParam(":hasil_vote", $hasil_vote);
                    $sql->execute(); // Eksekusi query insert

                    // Lakukan insert data vote_backup
                    $sqlBackup = $pdo->prepare("INSERT INTO vote_backup(nim, nama, email, hasil_vote, waktu) VALUES(:nim, :nama, :email, :hasil_vote, now())");
                    $sqlBackup->bindParam(":nim", $nim);
                    $sqlBackup->bindParam(":nama", $namaAkun);
                    $sqlBackup->bindParam(":email", $email);
                    $sqlBackup->bindParam(":hasil_vote", $hasil_vote);
                    $sqlBackup->execute(); // Eksekusi query insert

                    // Lakukan insert data vote_backup_ke_3
                    $sqlBackupKe3 = $pdo->prepare("INSERT INTO vote_backup_ke_3(nim, nama, email, hasil_vote, waktu) VALUES(:nim, :nama, :email, :hasil_vote, now())");
                    $sqlBackupKe3->bindParam(":nim", $nim);
                    $sqlBackupKe3->bindParam(":nama", $namaAkun);
                    $sqlBackupKe3->bindParam(":email", $email);
                    $sqlBackupKe3->bindParam(":hasil_vote", $hasil_vote);
                    $sqlBackupKe3->execute(); // Eksekusi query insert
                }
            }

        }else{
            $nim        = $vote["nim"]; // Ambil nim pada tabel vote
            $nama       = $vote["nama"]; // Ambil nama pada tabel vote
            $hasil_vote = $vote["hasil_vote"]; // Ambil Hasil Vote pada tabel vote
            $waktu      = $vote["waktu"]; // Ambil Hasil Vote pada tabel vote
        }

        $_SESSION["nim"]        = $nim;
        $_SESSION["nama"]       = $nama;
        $_SESSION["email"]      = $email;
        $_SESSION["hasil_vote"] = $hasil_vote;
        $_SESSION["waktu"]      = $waktu;

        header('location: https://kpump-tif.arpateam.com/vote');
    } else {
        $authUrl = $gclient->createAuthUrl();
        header("location: ".$authUrl);
    }

?>