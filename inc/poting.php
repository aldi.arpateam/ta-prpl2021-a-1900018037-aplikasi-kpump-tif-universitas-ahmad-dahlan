<?php

	session_start();
	$session_email	= $_SESSION['email'];

	$portal1 = $pdo->query("SELECT * FROM vote WHERE email='$session_email'");
	$rportal1 = $portal1->fetch(PDO::FETCH_ASSOC);

	$portal2 = $pdo->query("SELECT * FROM vote_backup WHERE email='$session_email'");
	$rportal2 = $portal2->fetch(PDO::FETCH_ASSOC);

	$portal3 = $pdo->query("SELECT * FROM vote_backup_ke_3 WHERE email='$session_email'");
	$rportal3 = $portal3->fetch(PDO::FETCH_ASSOC);

	$hasilVote	= $rportal1['hasil_vote'];

	// Kita cek apakah user sudah login atau belum
	// Cek nya dengan cara cek apakah terdapat session username atau tidak
	if(empty($rportal1['email'])){ // Jika tidak ada session username berarti dia belum login
		echo "<script>window.location = 'https://kpump-tif.arpateam.com';</script>";
		die();
	}
	if(empty($rportal2['email'])){ // Jika tidak ada session username berarti dia belum login
		echo "<script>window.location = 'https://kpump-tif.arpateam.com';</script>";
		die();
	}
	if(empty($rportal3['email'])){ // Jika tidak ada session username berarti dia belum login
		echo "<script>window.location = 'https://kpump-tif.arpateam.com';</script>";
		die();
	}

?>

<!-- Jika Belum Melakukan Voting -->
<?php if (empty($hasilVote)): ?>
<div class="container px-4">
	<!-- Form untuk pooting -->
	<form action="proses-vote" method="POST" class="row justify-content-md-center shadow my-4">

		<div class="col-12 alert alert-warning btn-block alert-dismissible fade show rounded-0" role="alert">
  			<strong><i class="fa fa-info-circle"></i> Hallo <?= $rportal1['nama']; ?>!</strong> Anda belum melakukan voting. Mari suarakan hak anda!</strong>.
			<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
		</div>

		<div class="col-12 col-lg-10 text-center my-4" style="color: #223668;">
			<img src="images/asset-images/kop-surat-suara.jpg" class="img-fluid mb-2" alt="Gambar Kop Surat Suara">
			<h1 class="d-none d-lg-block">KETUA DAN WAKIL KETUA HMTIF</h1>
			<h2 class="d-none d-lg-block">TAHUN 2021</h2>
			<h3 class="d-block d-lg-none">KETUA DAN WAKIL KETUA HMTIF</h3>
			<h4 class="d-block d-lg-none">TAHUN 2021</h4>
		</div>

		<input type="hidden" name="in_email" value="<?= $_SESSION['email']; ?>">

		<div class="w-100"></div>

		<div class="col-6 col-lg-5">
			<input type="checkbox" id="wasup" name="in_wasup">
			<label class="forLabel" for="wasup">
				<img src="images/assets-paslon/paslon-01.jpg" class="img-fluid" alt="Gambar Paslon-01">
			</label>
		</div>

		<div class="col-6 col-lg-5">
			<input id="wifi" name="in_wifi" type="checkbox">
			<label class="forLabel" for="wifi">
				<img src="images/assets-paslon/paslon-02.jpg" class="img-fluid" alt="Gambar Paslon-02">
			</label>
		</div>

		<div class="col-12 col-lg-10 d-grid">
			<button type="button" class="my-4 btn btn-lg text-light rounded-0" style="background-color: #223668;" data-bs-toggle="modal" data-bs-target="#kirimSuara">MASUKKAN KE KOTAK SUARA <i class="fa fa-envelope"></i></button>
		</div>

		<div class="modal fade" id="kirimSuara" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
		    <div class="modal-dialog">
		      	<div class="modal-content">
		      
		        	<!-- Modal Header -->
		        	<div class="modal-header">
		          		<h5 class="modal-title text-primary"><i class="fa fa-exclamation-triangle"></i> Apakah anda yakin dengan pilihan anda?</h5>
		          		<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
		        	</div>
		        
		        	<!-- Modal footer -->
		        	<div class="modal-footer">
		        		<button type="submit" class="btn btn-primary">Yakin <i class="fa fa-check"></i></button>
		          		<button type="button" class="btn btn-danger" data-bs-dismiss="modal">Tidak <i class="fa fa-times-circle"></i></button>
		        	</div>
		        
		      	</div>
		    </div>
		</div>

	</form>
	<!-- Form untuk pooting -->
</div>
<?php endif ?>
<!-- Jika Belum Melakukan Voting -->

<!-- Jika Sudah Melakukan Voting -->
<?php if (!empty($hasilVote)): ?>
<div class="container px-4">
	<div class="row justify-content-center shadow my-4 pb-4">
		<div class="alert alert-warning btn-block alert-dismissible fade show rounded-0" role="alert">
			  	<strong><i class="fa fa-info-circle"></i> Hallo <?= $rportal1['nama']; ?>!</strong> Terimakasih sudah berpartisipasi dalam Pemilihan Ketua & Wakil Ketua HMTIF.
				<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
		</div>
		<div class="col-12 col-md-8 col-lg-6 col-xl-6 text-center">
			<?php
				if ($hasilVote==3) {
					echo "<h3 class='my-4 text-light btn-get-started btn-get-started-blue'>TERIMAKASIH SUDAH MENGGUNAKAN HAK SUARA ANDA</h3>";
					echo "<img src='images/assets-paslon/golput.jpeg' class='img-fluid' alt='Gambar Golput'>";
				}elseif ($hasilVote==1) {
					echo "<h3 class='my-4 text-light btn-get-started btn-get-started-blue'>TERIMAKASIH SUDAH MEMILIH KAMI</h3>";
					echo "<img src='images/assets-paslon/paslon-01-done.jpg' class='img-fluid' alt='Gambar Paslon-01'>";
				}elseif ($hasilVote==2) {
					echo "<h3 class='my-4 text-light btn-get-started btn-get-started-blue'>TERIMAKASIH SUDAH MEMILIH KAMI</h3>";
					echo "<img src='images/assets-paslon/paslon-02-done.jpg' class='img-fluid' alt='Gambar Paslon-02'>";
				}
			?>
		</div>
	</div>
</div>
<?php endif ?>
<!-- Jika Sudah Melakukan Voting -->