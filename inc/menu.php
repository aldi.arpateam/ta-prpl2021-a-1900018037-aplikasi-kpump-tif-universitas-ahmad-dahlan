<section style="height:100%; width: 100%; box-sizing: border-box; background-color: #FFFFFF">

    <div class="header-2-2" style="font-family: 'Poppins', sans-serif;">

        <nav class="navbar navbar-expand-lg navbar-light p-4 px-md-4 mb-3 bg-body border-bottom" style="font-family: Poppins, sans-serif;">

            <div class="container">

                <a class="navbar-brand" href="home">
                    <img src="images/icon-kpum.jpg" alt="Icon KPUM">
                </a>

                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                    <ul class="navbar-nav mx-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link px-md-4 <?php if($_GET['module']=='home'){echo "active";} ?>" aria-current="page" href="home"><i class="fa fa-home"></i> <span class="d-inline d-lg-none">Home</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link px-md-4 <?php if($_GET['module']=='cek-dpt'){echo "active";} ?>" aria-current="page" href="cek-dpt"><i class="fa fa-book"></i> Cek DPT</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link px-md-4 <?php if($_GET['module']=='pot'){echo "active";} ?>" href="inc/google.php"><i class="fa fa-edit"></i> Vote</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link px-md-4 <?php if($_GET['module']=='hasil-vote'){echo "active";} ?>" href="hasil-vote"><i class="fa fa-file-text-o"></i> Hasil Vote</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link px-md-4 <?php if($_GET['module']=='kritik-saran'){echo "active";} ?>" href="inc/login-kritik-saran.php"><i class="fa fa-commenting-o"></i> Kritik & Saran</a>
                        </li>
                    </ul>
                    <div class="d-flex">
                        <button class="btn btn-get-started btn-get-started-blue">
                            <a href="#"><i class="fa fa-instagram"></i></a>
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-youtube"></i></a>
                        </button>
                    </div>
                </div>
            </div>
        </nav>

    </div>

</section>

<?php if (strtotime($sekarang)>strtotime($waktu_mulai) && strtotime($sekarang)<strtotime($waktu_berakhir)): ?>

<div class="container px-4">
    <div class="alert alert-primary alert-dismissible fade show" role="alert">

        <?php

            $banyakData = $pdo->query("SELECT * FROM vote WHERE hasil_vote!=NULL OR hasil_vote!='0'");
            $tampilBanyakData = $banyakData->rowCount();
        ?>

        <strong><i class="fa fa-file-text-o"></i></strong> Sudah ada <strong><?= $tampilBanyakData; ?> Surat Suara</strong> masuk.
        <marquee scrollamount="5">
            <?php

                $pemilih = $pdo->query("SELECT nama FROM vote ORDER BY waktu DESC LIMIT 5");
                while ($rpemilih = $pemilih->fetch(PDO::FETCH_ASSOC)) {
                    echo "<strong>".$rpemilih['nama']."</strong> baru saja memilih | ";
                }

            ?>
        </marquee>

        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>

    </div>
</div>
    
<?php endif ?>