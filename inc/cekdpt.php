<?php require "inc/logout.php"; ?>

<section style="height:100%; width: 100%; box-sizing: border-box; background-color: #FFFFFF">

    <div class="header-2-2" style="font-family: 'Poppins', sans-serif;">

        <div class="mx-auto d-flex flex-lg-row flex-column hero-header-2-2">
            
            <!-- Left Column -->
            <div class="left-column-header-2-2 d-flex flex-lg-grow-1 flex-column align-items-lg-start text-lg-start align-items-center text-center">

                <h1 class="title-text-big-header-2-2">YUK,<br /> CEK DAFTAR PEMILIH TETAP ANDA</h1>

                <div class="d-block d-md-none right-column-header-2-2 text-center justify-content-center pe-0 mb-4 text-center">
                    <img class="img-fluid" src="images/asset-images/icon-kpum.png" alt="Icon KPUMP-TIF">
                </div>

                <?php

                    if (isset($_POST['submit'])) {

                        $jumlahNIM  = strlen($_POST['nim']);

                        if ($jumlahNIM!=10) {
                            echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>";
                            echo "<strong><i class='fa fa-ban'></i> Error!</strong> Mohon masukkan NIM 10 digit.";
                            echo "<button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'></button>";
                            echo "</div>";
                        }elseif ($jumlahNIM==10) {

                            $cariDPT = $pdo->query("SELECT nama_lengkap FROM data_mahasiswa_aktif WHERE nim='$_POST[nim]'");
                            $ketemuDPT = $cariDPT->rowCount();
                            $resultDPT = $cariDPT->fetch(PDO::FETCH_ASSOC);

                            if ($ketemuDPT > 0){
                                echo "<div class='alert alert-primary alert-dismissible fade show col-10' role='alert'>";
                                echo "<strong><i class='fa fa-check-square-o'></i> Hay ".$resultDPT['nama_lengkap']."!</strong> Anda terdaftar sebagai DPT.";
                                echo "<br />";
                                echo "<strong><i class='fa fa-check-square-o'></i> Hari Jum'at, 02 April 2021</strong> jangan lupa untuk memilih ya.";
                                echo "<button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'></button>";
                                echo "</div>";
                            }else{
                                echo "<div class='alert alert-warning alert-dismissible fade show col-10' role='alert'>";
                                echo "<strong><i class='fa fa-exclamation-triangle'></i> Maaf!</strong> Anda bukan termasuk sebagai Daftar Pemilih Tetap.";
                                echo "<button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'></button>";
                                echo "</div>";
                            }

                        }

                        
                    }

                ?>

                <form method="POST" action="">
                    <div class="input-group mb-3 shadow">
                        <input type="number" name="nim" class="form-control border border-primary" placeholder="NIM (10 digit)" aria-label="Masukkan NIM (10 digit)" aria-describedby="button-addon2" required>
                        <button class="btn btn-lg btn-outline-primary" type="submit" name="submit" id="button-addon2"><i class="fa fa-search"></i></button>
                    </div>
                </form>

            </div>
      
            <!-- Right Column -->
            <div class="d-none d-md-block right-column-header-2-2 text-center d-flex justify-content-center pe-0">
                <img id="img-fluid" style="display: block;max-width: 100%;height: auto;" src="images/asset-images/icon-kpum.png" alt="Icon KPUMP-TIF">
            </div>

        </div>
    </div>

</section>