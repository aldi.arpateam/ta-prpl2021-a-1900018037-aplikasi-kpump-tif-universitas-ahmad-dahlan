<?php
	
	session_start();
	$_SESSION = [];
	session_destroy();
	session_unset();

	echo "<script>window.history.go(-2);</script>";