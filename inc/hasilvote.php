<?php

    require "inc/logout.php";
    
    if (strtotime($sekarang)<strtotime($waktu_mulai) OR strtotime($sekarang)>strtotime($waktu_mulai) && strtotime($sekarang)<strtotime($waktu_berakhir)) {

?>

<div class="p-4" style="font-family: 'Poppins', sans-serif;">    
    <div class="mx-auto d-flex align-items-center justify-content-center flex-column">
        <img class="img-empty-3-3" src="http://api.elements.buildwithangga.com/storage/files/2/assets/Empty%20State/EmptyState3/Empty-3-1.png" alt="">                       
        <div class="text-center w-100">
            <h1 class="title-text-empty-3-3">Tunggu Sebentar Ya</h1>
            <p class="caption-text-empty-3-3">Ntar kalo pemilihannya sudah selesai, <br />kamu juga bakal bisa lihat kok :-)</p>
        </div>
    </div>
</div>

<?php
    }else{
?>

<div class="container">

    <div class="row">

        <div class="col-12">
            <?php
                $suratSuaraSAH = $pdo->query("SELECT nim FROM vote WHERE hasil_vote!=NULL OR hasil_vote!='0'");
                $tampilsuratSuaraSAH = $suratSuaraSAH->rowCount();

                $banyakData = $pdo->query("SELECT nim FROM vote");
                $tampilBanyakData = $banyakData->rowCount();
            ?>

            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                <h4><i class="fa fa-info-circle"></i> Terdapat <span class="badge rounded-pill bg-primary"><?= $tampilsuratSuaraSAH; ?></span> Surat Suara yang SAH!</h4>
                Dari <span class="badge rounded-pill bg-primary"><?= $tampilBanyakData; ?></span> Partisipasi Pencoblos.
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>

            <div class="table-responsive mt-4">
                <table class="table table-bordered table-striped">

                    <thead>
                        <tr class="text-center">
                            <th width="5%">No</th>
                            <th width="25%">Angkatan</th>
                            <th width="25%" class="bg-success text-light">PASLON 01</th>
                            <th width="25%" class="bg-primary text-light">PASLON 02</th>
                            <th width="20%" class="bg-danger text-light">GOLPUT</th>
                        </tr>
                    </thead>

                    <tbody>

                        <?php

                            $VoteAngkatan20_01 = $pdo->query("SELECT nim FROM vote WHERE hasil_vote='1' AND nim LIKE '20%'");
                            $tampilVoteAngkatan20_01 = $VoteAngkatan20_01->rowCount();

                            $VoteAngkatan20_02 = $pdo->query("SELECT nim FROM vote WHERE hasil_vote='2' AND nim LIKE '20%'");
                            $tampilVoteAngkatan20_02 = $VoteAngkatan20_02->rowCount();

                            $VoteAngkatan20_03 = $pdo->query("SELECT nim FROM vote WHERE hasil_vote='3' AND nim LIKE '20%'");
                            $tampilVoteAngkatan20_03 = $VoteAngkatan20_03->rowCount();

                            $VoteAngkatan19_01 = $pdo->query("SELECT nim FROM vote WHERE hasil_vote='1' AND nim LIKE '19%'");
                            $tampilVoteAngkatan19_01 = $VoteAngkatan19_01->rowCount();

                            $VoteAngkatan19_02 = $pdo->query("SELECT nim FROM vote WHERE hasil_vote='2' AND nim LIKE '19%'");
                            $tampilVoteAngkatan19_02 = $VoteAngkatan19_02->rowCount();

                            $VoteAngkatan19_03 = $pdo->query("SELECT nim FROM vote WHERE hasil_vote='3' AND nim LIKE '19%'");
                            $tampilVoteAngkatan19_03 = $VoteAngkatan19_03->rowCount();

                            $VoteAngkatan18_01 = $pdo->query("SELECT nim FROM vote WHERE hasil_vote='1' AND nim LIKE '18%'");
                            $tampilVoteAngkatan18_01 = $VoteAngkatan18_01->rowCount();

                            $VoteAngkatan18_02 = $pdo->query("SELECT nim FROM vote WHERE hasil_vote='2' AND nim LIKE '18%'");
                            $tampilVoteAngkatan18_02 = $VoteAngkatan18_02->rowCount();

                            $VoteAngkatan18_03 = $pdo->query("SELECT nim FROM vote WHERE hasil_vote='3' AND nim LIKE '18%'");
                            $tampilVoteAngkatan18_03 = $VoteAngkatan18_03->rowCount();

                            $VoteAngkatan17_01 = $pdo->query("SELECT nim FROM vote WHERE hasil_vote='1' AND nim LIKE '17%'");
                            $tampilVoteAngkatan17_01 = $VoteAngkatan17_01->rowCount();

                            $VoteAngkatan17_02 = $pdo->query("SELECT nim FROM vote WHERE hasil_vote='2' AND nim LIKE '17%'");
                            $tampilVoteAngkatan17_02 = $VoteAngkatan17_02->rowCount();

                            $VoteAngkatan17_03 = $pdo->query("SELECT nim FROM vote WHERE hasil_vote='3' AND nim LIKE '17%'");
                            $tampilVoteAngkatan17_03 = $VoteAngkatan17_03->rowCount();

                            $VoteAngkatan16_01 = $pdo->query("SELECT nim FROM vote WHERE hasil_vote='1' AND nim LIKE '16%'");
                            $tampilVoteAngkatan16_01 = $VoteAngkatan16_01->rowCount();

                            $VoteAngkatan16_02 = $pdo->query("SELECT nim FROM vote WHERE hasil_vote='2' AND nim LIKE '16%'");
                            $tampilVoteAngkatan16_02 = $VoteAngkatan16_02->rowCount();

                            $VoteAngkatan16_03 = $pdo->query("SELECT nim FROM vote WHERE hasil_vote='3' AND nim LIKE '16%'");
                            $tampilVoteAngkatan16_03 = $VoteAngkatan16_03->rowCount();

                            $VoteAngkatan15_01 = $pdo->query("SELECT nim FROM vote WHERE hasil_vote='1' AND nim LIKE '15%'");
                            $tampilVoteAngkatan15_01 = $VoteAngkatan15_01->rowCount();

                            $VoteAngkatan15_02 = $pdo->query("SELECT nim FROM vote WHERE hasil_vote='2' AND nim LIKE '15%'");
                            $tampilVoteAngkatan15_02 = $VoteAngkatan15_02->rowCount();

                            $VoteAngkatan15_03 = $pdo->query("SELECT nim FROM vote WHERE hasil_vote='3' AND nim LIKE '15%'");
                            $tampilVoteAngkatan15_03 = $VoteAngkatan15_03->rowCount();

                            $VoteAngkatan14_01 = $pdo->query("SELECT nim FROM vote WHERE hasil_vote='1' AND nim LIKE '14%'");
                            $tampilVoteAngkatan14_01 = $VoteAngkatan14_01->rowCount();

                            $VoteAngkatan14_02 = $pdo->query("SELECT nim FROM vote WHERE hasil_vote='2' AND nim LIKE '14%'");
                            $tampilVoteAngkatan14_02 = $VoteAngkatan14_02->rowCount();

                            $VoteAngkatan14_03 = $pdo->query("SELECT nim FROM vote WHERE hasil_vote='3' AND nim LIKE '14%'");
                            $tampilVoteAngkatan14_03 = $VoteAngkatan14_03->rowCount();

                            $VoteAngkatan13_01 = $pdo->query("SELECT nim FROM vote WHERE hasil_vote='1' AND nim LIKE '13%'");
                            $tampilVoteAngkatan13_01 = $VoteAngkatan13_01->rowCount();

                            $VoteAngkatan13_02 = $pdo->query("SELECT nim FROM vote WHERE hasil_vote='2' AND nim LIKE '13%'");
                            $tampilVoteAngkatan13_02 = $VoteAngkatan13_02->rowCount();

                            $VoteAngkatan13_03 = $pdo->query("SELECT nim FROM vote WHERE hasil_vote='3' AND nim LIKE '13%'");
                            $tampilVoteAngkatan13_03 = $VoteAngkatan13_03->rowCount();

                        ?>

                        <tr class="text-center">
                            <td>1.</td>
                            <td class="<?php if($tampilVoteAngkatan20_01>$tampilVoteAngkatan20_02 AND $tampilVoteAngkatan20_01>$tampilVoteAngkatan20_03){ echo "bg-success text-light"; }elseif($tampilVoteAngkatan20_02>$tampilVoteAngkatan20_01 AND $tampilVoteAngkatan20_02>$tampilVoteAngkatan20_03){ echo "bg-primary text-light"; }elseif($tampilVoteAngkatan20_03>$tampilVoteAngkatan20_01 AND $tampilVoteAngkatan20_03>$tampilVoteAngkatan20_02){ echo "bg-danger text-light"; } ?>">Angkatan 2020</td>
                            <td class="<?php if($tampilVoteAngkatan20_01>$tampilVoteAngkatan20_02 AND $tampilVoteAngkatan20_01>$tampilVoteAngkatan20_03){ echo "bg-success text-light"; } ?>"><?= $tampilVoteAngkatan20_01; ?></td>
                            <td class="<?php if($tampilVoteAngkatan20_02>$tampilVoteAngkatan20_01 AND $tampilVoteAngkatan20_02>$tampilVoteAngkatan20_03){ echo "bg-primary text-light"; } ?>"><?= $tampilVoteAngkatan20_02; ?></td>
                            <td class="<?php if($tampilVoteAngkatan20_03>$tampilVoteAngkatan20_01 AND $tampilVoteAngkatan20_03>$tampilVoteAngkatan20_02){ echo "bg-danger text-light"; } ?>"><?= $tampilVoteAngkatan20_03; ?></td>
                        </tr>

                        <tr style="text-align: center;">
                            <td>2.</td>
                            <td class="<?php if($tampilVoteAngkatan19_01>$tampilVoteAngkatan19_02 AND $tampilVoteAngkatan19_01>$tampilVoteAngkatan19_03){ echo "bg-success text-light"; }elseif($tampilVoteAngkatan19_02>$tampilVoteAngkatan19_01 AND $tampilVoteAngkatan19_02>$tampilVoteAngkatan19_03){ echo "bg-primary text-light"; }elseif($tampilVoteAngkatan19_03>$tampilVoteAngkatan19_01 AND $tampilVoteAngkatan19_03>$tampilVoteAngkatan19_02){ echo "bg-danger text-light"; } ?>">Angkatan 2019</td>
                            <td class="<?php if($tampilVoteAngkatan19_01>$tampilVoteAngkatan19_02 AND $tampilVoteAngkatan19_01>$tampilVoteAngkatan19_03){ echo "bg-success text-light"; } ?>"><?= $tampilVoteAngkatan19_01; ?></td>
                            <td class="<?php if($tampilVoteAngkatan19_02>$tampilVoteAngkatan19_01 AND $tampilVoteAngkatan19_02>$tampilVoteAngkatan19_03){ echo "bg-primary text-light"; } ?>"><?= $tampilVoteAngkatan19_02; ?></td>
                            <td class="<?php if($tampilVoteAngkatan19_03>$tampilVoteAngkatan19_01 AND $tampilVoteAngkatan19_03>$tampilVoteAngkatan19_02){ echo "bg-danger text-light"; } ?>"><?= $tampilVoteAngkatan19_03; ?></td>
                        </tr>

                        <tr style="text-align: center;">
                            <td>3.</td>
                            <td class="<?php if($tampilVoteAngkatan18_01>$tampilVoteAngkatan18_02 AND $tampilVoteAngkatan18_01>$tampilVoteAngkatan18_03){ echo "bg-success text-light"; }elseif($tampilVoteAngkatan18_02>$tampilVoteAngkatan18_01 AND $tampilVoteAngkatan18_02>$tampilVoteAngkatan18_03){ echo "bg-primary text-light"; }elseif($tampilVoteAngkatan18_03>$tampilVoteAngkatan18_01 AND $tampilVoteAngkatan18_03>$tampilVoteAngkatan18_02){ echo "bg-danger text-light"; } ?>">Angkatan 2018</td>
                            <td class="<?php if($tampilVoteAngkatan18_01>$tampilVoteAngkatan18_02 AND $tampilVoteAngkatan18_01>$tampilVoteAngkatan18_03){ echo "bg-success text-light"; } ?>"><?= $tampilVoteAngkatan18_01; ?></td>
                            <td class="<?php if($tampilVoteAngkatan18_02>$tampilVoteAngkatan18_01 AND $tampilVoteAngkatan18_02>$tampilVoteAngkatan18_03){ echo "bg-primary text-light"; } ?>"><?= $tampilVoteAngkatan18_02; ?></td>
                            <td class="<?php if($tampilVoteAngkatan18_03>$tampilVoteAngkatan18_01 AND $tampilVoteAngkatan18_03>$tampilVoteAngkatan18_02){ echo "bg-danger text-light"; } ?>"><?= $tampilVoteAngkatan18_03; ?></td>
                        </tr>

                        <tr style="text-align: center;">
                            <td>4.</td>
                            <td class="<?php if($tampilVoteAngkatan17_01>$tampilVoteAngkatan17_02 AND $tampilVoteAngkatan17_01>$tampilVoteAngkatan17_03){ echo "bg-success text-light"; }elseif($tampilVoteAngkatan17_02>$tampilVoteAngkatan17_01 AND $tampilVoteAngkatan17_02>$tampilVoteAngkatan17_03){ echo "bg-primary text-light"; }elseif($tampilVoteAngkatan17_03>$tampilVoteAngkatan17_01 AND $tampilVoteAngkatan17_03>$tampilVoteAngkatan17_02){ echo "bg-danger text-light"; } ?>">Angkatan 2017</td>
                            <td class="<?php if($tampilVoteAngkatan17_01>$tampilVoteAngkatan17_02 AND $tampilVoteAngkatan17_01>$tampilVoteAngkatan17_03){ echo "bg-success text-light"; } ?>"><?= $tampilVoteAngkatan17_01; ?></td>
                            <td class="<?php if($tampilVoteAngkatan17_02>$tampilVoteAngkatan17_01 AND $tampilVoteAngkatan17_02>$tampilVoteAngkatan17_03){ echo "bg-primary text-light"; } ?>"><?= $tampilVoteAngkatan17_02; ?></td>
                            <td class="<?php if($tampilVoteAngkatan17_03>$tampilVoteAngkatan17_01 AND $tampilVoteAngkatan17_03>$tampilVoteAngkatan17_02){ echo "bg-danger text-light"; } ?>"><?= $tampilVoteAngkatan17_03; ?></td>
                        </tr>

                        <tr style="text-align: center;">
                            <td>5.</td>
                            <td class="<?php if($tampilVoteAngkatan16_01>$tampilVoteAngkatan16_02 AND $tampilVoteAngkatan16_01>$tampilVoteAngkatan16_03){ echo "bg-success text-light"; }elseif($tampilVoteAngkatan16_02>$tampilVoteAngkatan16_01 AND $tampilVoteAngkatan16_02>$tampilVoteAngkatan16_03){ echo "bg-primary text-light"; }elseif($tampilVoteAngkatan16_03>$tampilVoteAngkatan16_01 AND $tampilVoteAngkatan16_03>$tampilVoteAngkatan16_02){ echo "bg-danger text-light"; } ?>">Angkatan 2016</td>
                            <td class="<?php if($tampilVoteAngkatan16_01>$tampilVoteAngkatan16_02 AND $tampilVoteAngkatan16_01>$tampilVoteAngkatan16_03){ echo "bg-success text-light"; } ?>"><?= $tampilVoteAngkatan16_01; ?></td>
                            <td class="<?php if($tampilVoteAngkatan16_02>$tampilVoteAngkatan16_01 AND $tampilVoteAngkatan16_02>$tampilVoteAngkatan16_03){ echo "bg-primary text-light"; } ?>"><?= $tampilVoteAngkatan16_02; ?></td>
                            <td class="<?php if($tampilVoteAngkatan16_03>$tampilVoteAngkatan16_01 AND $tampilVoteAngkatan16_03>$tampilVoteAngkatan16_02){ echo "bg-danger text-light"; } ?>"><?= $tampilVoteAngkatan16_03; ?></td>
                        </tr>

                        <tr style="text-align: center;">
                            <td>6.</td>
                            <td class="<?php if($tampilVoteAngkatan15_01>$tampilVoteAngkatan15_02 AND $tampilVoteAngkatan15_01>$tampilVoteAngkatan15_03){ echo "bg-success text-light"; }elseif($tampilVoteAngkatan15_02>$tampilVoteAngkatan15_01 AND $tampilVoteAngkatan15_02>$tampilVoteAngkatan15_03){ echo "bg-primary text-light"; }elseif($tampilVoteAngkatan15_03>$tampilVoteAngkatan15_01 AND $tampilVoteAngkatan15_03>$tampilVoteAngkatan15_02){ echo "bg-danger text-light"; } ?>">Angkatan 2015</td>
                            <td class="<?php if($tampilVoteAngkatan15_01>$tampilVoteAngkatan15_02 AND $tampilVoteAngkatan15_01>$tampilVoteAngkatan15_03){ echo "bg-success text-light"; } ?>"><?= $tampilVoteAngkatan15_01; ?></td>
                            <td class="<?php if($tampilVoteAngkatan15_02>$tampilVoteAngkatan15_01 AND $tampilVoteAngkatan15_02>$tampilVoteAngkatan15_03){ echo "bg-primary text-light"; } ?>"><?= $tampilVoteAngkatan15_02; ?></td>
                            <td class="<?php if($tampilVoteAngkatan15_03>$tampilVoteAngkatan15_01 AND $tampilVoteAngkatan15_03>$tampilVoteAngkatan15_02){ echo "bg-danger text-light"; } ?>"><?= $tampilVoteAngkatan15_03; ?></td>
                        </tr>

                        <tr style="text-align: center;">
                            <td>7.</td>
                            <td class="<?php if($tampilVoteAngkatan14_01>$tampilVoteAngkatan14_02 AND $tampilVoteAngkatan14_01>$tampilVoteAngkatan14_03){ echo "bg-success text-light"; }elseif($tampilVoteAngkatan14_02>$tampilVoteAngkatan14_01 AND $tampilVoteAngkatan14_02>$tampilVoteAngkatan14_03){ echo "bg-primary text-light"; }elseif($tampilVoteAngkatan14_03>$tampilVoteAngkatan14_01 AND $tampilVoteAngkatan14_03>$tampilVoteAngkatan14_02){ echo "bg-danger text-light"; } ?>">Angkatan 2014</td>
                            <td class="<?php if($tampilVoteAngkatan14_01>$tampilVoteAngkatan14_02 AND $tampilVoteAngkatan14_01>$tampilVoteAngkatan14_03){ echo "bg-success text-light"; } ?>"><?= $tampilVoteAngkatan14_01; ?></td>
                            <td class="<?php if($tampilVoteAngkatan14_02>$tampilVoteAngkatan14_01 AND $tampilVoteAngkatan14_02>$tampilVoteAngkatan14_03){ echo "bg-primary text-light"; } ?>"><?= $tampilVoteAngkatan14_02; ?></td>
                            <td class="<?php if($tampilVoteAngkatan14_03>$tampilVoteAngkatan14_01 AND $tampilVoteAngkatan14_03>$tampilVoteAngkatan14_02){ echo "bg-danger text-light"; } ?>"><?= $tampilVoteAngkatan14_03; ?></td>
                        </tr>

                        <tr style="text-align: center;">
                            <td>8.</td>
                            <td class="<?php if($tampilVoteAngkatan13_01>$tampilVoteAngkatan13_02 AND $tampilVoteAngkatan13_01>$tampilVoteAngkatan13_03){ echo "bg-success text-light"; }elseif($tampilVoteAngkatan13_02>$tampilVoteAngkatan13_01 AND $tampilVoteAngkatan13_02>$tampilVoteAngkatan13_03){ echo "bg-primary text-light"; }elseif($tampilVoteAngkatan13_03>$tampilVoteAngkatan13_01 AND $tampilVoteAngkatan13_03>$tampilVoteAngkatan13_02){ echo "bg-danger text-light"; } ?>">Angkatan 2013</td>
                            <td class="<?php if($tampilVoteAngkatan13_01>$tampilVoteAngkatan13_02 AND $tampilVoteAngkatan13_01>$tampilVoteAngkatan13_03){ echo "bg-success text-light"; } ?>"><?= $tampilVoteAngkatan13_01; ?></td>
                            <td class="<?php if($tampilVoteAngkatan13_02>$tampilVoteAngkatan13_01 AND $tampilVoteAngkatan13_02>$tampilVoteAngkatan13_03){ echo "bg-primary text-light"; } ?>"><?= $tampilVoteAngkatan13_02; ?></td>
                            <td class="<?php if($tampilVoteAngkatan13_03>$tampilVoteAngkatan13_01 AND $tampilVoteAngkatan13_03>$tampilVoteAngkatan13_02){ echo "bg-danger text-light"; } ?>"><?= $tampilVoteAngkatan13_03; ?></td>
                        </tr>

                    </tbody>
                    <?php

                        $suratSuaraMasuk = $pdo->query("SELECT nim FROM vote WHERE hasil_vote!=NULL OR hasil_vote!='0'");
                        $tampilsuratSuaraMasuk = $suratSuaraMasuk->rowCount();

                        // Jumlah vote paslon

                        $rincianVotePaslon1 = $pdo->query("SELECT vote.nim FROM vote WHERE hasil_vote='1'");
                        $rrincianVotePaslon1 = $rincianVotePaslon1->rowCount();

                        $rincianVotePaslon2 = $pdo->query("SELECT vote.nim FROM vote WHERE hasil_vote='2'");
                        $rrincianVotePaslon2 = $rincianVotePaslon2->rowCount();

                        $rincianVotePaslonGolput = $pdo->query("SELECT vote.nim FROM vote WHERE hasil_vote='3'");
                        $rrincianVotePaslonGolput = $rincianVotePaslonGolput->rowCount();

                        // Presentase

                        $presentase01    = (100 * ($rrincianVotePaslon1/$tampilsuratSuaraMasuk));
                        $presentase02     = (100 * ($rrincianVotePaslon2/$tampilsuratSuaraMasuk));
                        $presentaseGOLPUT   = (100 * ($rrincianVotePaslonGolput/$tampilsuratSuaraMasuk));

                        if ($presentase01>$presentase02 AND $presentase01>$presentaseGOLPUT) {
                            $pemenangnya    = "WIDA GHAIB NUGROHO - SUPRAYOGI BUDI .P";
                            $rincianVotePemenang    = $rrincianVotePaslon1;
                            $presentasKemenangan    = number_format($presentase01,2);
                        }elseif ($presentase02>$presentase01 AND $presentase02>$presentaseGOLPUT) {
                            $pemenangnya    = "WISNU ANDRIAN - AMIRATUR RAFIFAH";
                            $rincianVotePemenang    = $rrincianVotePaslon2;
                            $presentasKemenangan    = number_format($presentase02,2);
                        }elseif ($presentaseGOLPUT>$presentase01 AND $presentaseGOLPUT>$presentase02) {
                            $pemenangnya    = "SEMUA PADA MILIH GOLPUT";
                            $rincianVotePemenang    = $rrincianVotePaslonGolput;
                            $presentasKemenangan    = number_format($presentaseGOLPUT,2);
                        }elseif ($presentase02==$presentase01) {
                            $pemenangnya    = "SERI";
                            $rincianVotePemenang    = $rrincianVotePaslon1;
                            $presentasKemenangan    = "SERI";
                        }

                    ?>

                    <thead style="font-size: 20px;" class="text-center">
                        <tr>
                            <th width="30%" colspan="2">JUMLAH</th>
                            <th width="25%" class="<?php if($rrincianVotePaslon1>$rrincianVotePaslon2 AND $rrincianVotePaslon1>$rrincianVotePaslonGolput){ echo "bg-success text-light"; } ?>"><?= $rrincianVotePaslon1; ?></th>
                            <th width="25%" class="<?php if($rrincianVotePaslon2>$rrincianVotePaslon1 AND $rrincianVotePaslon2>$rrincianVotePaslonGolput){ echo "bg-primary text-light"; } ?>"><?= $rrincianVotePaslon2; ?></th>
                            <th width="20%" class="<?php if($rrincianVotePaslonGolput>$rrincianVotePaslon1 AND $rrincianVotePaslonGolput>$rrincianVotePaslon2){ echo "bg-danger text-light"; } ?>"><?= $rrincianVotePaslonGolput; ?></th>
                        </tr>
                        <tr>
                            <th width="30%" colspan="2">PRESENTASE</th>
                            <th width="25%" class="<?php if($rrincianVotePaslon1>$rrincianVotePaslon2 AND $rrincianVotePaslon1>$rrincianVotePaslonGolput){ echo "bg-success text-light"; } ?>"><?= number_format($presentase01,2); ?>%</th>
                            <th width="25%" class="<?php if($rrincianVotePaslon2>$rrincianVotePaslon1 AND $rrincianVotePaslon2>$rrincianVotePaslonGolput){ echo "bg-primary text-light"; } ?>"><?= number_format($presentase02,2); ?>%</th>
                            <th width="20%" class="<?php if($rrincianVotePaslonGolput>$rrincianVotePaslon1 AND $rrincianVotePaslonGolput>$rrincianVotePaslon2){ echo "bg-danger text-light"; } ?>"><?= number_format($presentaseGOLPUT,2); ?>%</th>
                        </tr>
                    </thead>

                </table>
            </div><!-- /.box-body -->
        </div>

    </div>

</div>

<?php } ?>