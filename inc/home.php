<?php require "inc/logout.php"; ?>

<section style="height:100%; width: 100%; box-sizing: border-box; background-color: #FFFFFF">

    <div class="header-2-2" style="font-family: 'Poppins', sans-serif;">
        <div class="mx-auto d-flex flex-lg-row flex-column hero-header-2-2">
            
            <!-- Left Column -->
            <div class="left-column-header-2-2 d-flex flex-lg-grow-1 flex-column align-items-lg-start text-lg-start align-items-center text-center">

                <h1 class="title-text-big-header-2-2 d-lg-inline d-none">WEBSITE<br />PEMILIHAN KETUA & WAKIL KETUA<br> HMTIF 2021</h1>
                <h1 class="title-text-small-header-2-2 d-lg-none d-inline">WEBSITE<br />PEMILIHAN KETUA & WAKIL KETUA<br> HMTIF 2021</h1>

                <div class="d-block d-md-none right-column-header-2-2 text-center justify-content-center pe-0 mb-4">
                    <img class="img-fluid" src="images/asset-images/icon-kpum.png" alt="Gambar Pemilu">
                </div>

                <div class="div-button-header-2-2 d-inline d-lg-flex align-items-center mx-lg-0 mx-auto justify-content-center">
                    <a href="inc/google.php" role="button" class="btn mb-md-0 btn-try-header-2-2">Vote Sekarang <i class="fa fa-edit"></i></a>
                    <a href="https://www.instagram.com/p/CIr_ugasU1Y/?utm_source=ig_web_copy_link" role="button" class="btn btn-outline-header-2-2 align-items-center"><i class="fa fa-play-circle-o"></i> Lihat tutorial</a>
                </div>
            </div>
      
            <!-- Right Column -->
            <div class="d-none d-md-block right-column-header-2-2 text-center d-flex justify-content-center pe-0">
                <img id="img-fluid" style="display: block;max-width: 100%;height: auto;" src="images/asset-images/icon-kpum.png" alt="Gambar Pemilu">
            </div>

        </div>
    </div>

</section>

<section style="height:100%; width: 100%; box-sizing: border-box; background-color: #FFFFFF">

    <div style="font-family: 'Poppins', sans-serif;">

        <div class="card-block-content-2-2">
            <div class="card-content-2-2">
                <div class="d-flex flex-lg-row flex-column align-items-center">
                    <div class="me-lg-3">
                        <img src="images/asset-images/pemilu-me-v1.png" alt="Assets Gambar KPUM UAD">          
                    </div>

                    <div class="flex-grow-1 text-lg-start text-center card-text-content-2-2">

                        <?php

                            if (strtotime($sekarang)<strtotime($waktu_mulai)) {
                                echo "<h3 class='card-content-2-2-title'><strong><i class='fa fa-clock-o'></i></strong> <strong id='sebelumMulai'></strong> lagi, <br /> E-Vote Pemilwa HMTIF UAD 2021 <strong>akan di <u>buka</u></strong></h3>";
                                echo "<p class='card-content-2-2-caption'><strong><i class='fa fa-exclamation-triangle'></i> Pemilihan</strong> hanya di buka pada <strong>Hari Sabtu, 03 April 2021</strong> | <strong>JAM 07.00 - 17.00 WIB</strong></p>";
                            }

                            elseif (strtotime($sekarang)>strtotime($waktu_mulai) && strtotime($sekarang)<strtotime($waktu_berakhir)) {
                                echo "<h3 class='card-content-2-2-title'><strong><i class='fa fa-clock-o'></i></strong> <strong id='sesudahMulai'></strong> lagi, <br /> E-Vote Pemilwa HMTIF UAD 2021 <strong>akan di <u>tutup</u>.</strong></h3>";
                                echo "<p class='card-content-2-2-caption'><strong><i class='fa fa-exclamation-triangle'></i> Pemilihan</strong> sedang berlangsung sampai <strong>JAM 17.00 WIB</strong>.</p>";
                            }

                            else{
                                echo "<h3 class='card-content-2-2-title'><strong><i class='fa fa-exclamation-triangle'></i></strong> Pemilihan sudah dilaksanakan pada <strong>Hari Sabtu, 03 April 2021</strong> | <strong>JAM 07.00 - 17.00 WIB</strong>.</h3>";
                                echo "<p class='card-content-2-2-caption'><strong><i class='fa fa-exclamation-triangle'></i></strong> Sekarang anda sudah bisa melihat siapa pemenangnya. <br />Yuk lihat <strong><a href='hasil-vote'>Hasil Votenya</a></strong>.</p>";
                            }

                        ?>

                    </div>
                    <div class="me-lg-3 d-none d-lg-block">
                        <img src="images/asset-images/pemilu-me-v2.png" alt="Assets Gambar KPUM UAD">          
                    </div>

                </div>
            </div>
        </div>

        <div class="text-center title-text-content-2-2">
            <h1 class="text-title-content-2-2">YUK KENALAN DULU DENGAN</h1>
            <h5 class="text-caption-content-2-2" style="margin-left: 3rem; margin-right: 3rem;">PASLON KETUA & WAKIL KETUA HMTIF</h5>
        </div>

        <div class="container mb-4 pb-4">
            <div class="row justify-content-center">

                <div class="col-12 col-md-9 col-lg-6">
                    <img src="images/assets-paslon/wasup.jpg" class="img-fluid shadow" alt="Gambar Paslon-WASUP">
                </div>
                <div class="col-12 col-md-9 col-lg-6 mt-4 mt-lg-0">
                    <img src="images/assets-paslon/wifi.jpg" class="img-fluid shadow" alt="Gambar Paslon-WIFI">
                </div>

            </div>
        </div>

    </div>

</section>

<script>
    // Set the date we're counting down to
    var countDownDate = new Date("<?= $waktu_mulai; ?>").getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get today's date and time
        var now = new Date().getTime();
        
        // Find the distance between now and the count down date
        var distance = countDownDate - now;
        
        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        
        // Output the result in an element with id="sebelumMulai"
        document.getElementById("sebelumMulai").innerHTML = days + "d " + hours + "h " + minutes + "m " + seconds + "s ";
        
        // If the count down is over, write some text 
        if (distance < 0 && distance > -1000) {
            clearInterval(x);
            window.location.replace("https://kpump-tif.arpateam.com/");
        }
    }, 1000);

    // Set the date we're counting down to
    var countDownDate2 = new Date("<?= $waktu_berakhir; ?>").getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get today's date and time
        var now = new Date().getTime();
        
        // Find the distance between now and the count down date
        var distance = countDownDate2 - now;
        
        // Time calculations for days, hours, minutes and seconds
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        
        // Output the result in an element with id="sesudahMulai"
        document.getElementById("sesudahMulai").innerHTML = hours + "h " + minutes + "m " + seconds + "s ";
        
        // If the count down is over, write some text 
        if (distance > -1000 && distance < 0) {
            clearInterval(x);
            window.location.replace("https://www.arpateam.com");
        }
    }, 1000);
</script>