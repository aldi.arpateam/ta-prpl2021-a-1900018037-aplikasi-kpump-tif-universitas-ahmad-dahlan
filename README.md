# TA-PRPL2021-A-1900018037-Aplikasi KPUMP-TIF Universitas Ahmad Dahlan

**Membuat Aplikasi KPUMP-TIF Universitas Ahmad Dahlan**


Di masa pandemi ini, memaksa semua kegiatan perkuliahan harus dilakukan secara Online.
Salah satunya adalah Kegiatan Himpunan Mahasiswa Teknik Informatika yang harus di lakukan secara online juga.

Oleh karena itu saya akan membuat aplikasi Pemilihan Ketua & Wakil Ketua HMTIF berbasis website.

*Cara Kerja Aplikasi KPUMP-TIF 
    - Pemilihan hanya bisa di akses jika sudah masuk periode pemilihan
	- Pastikan jika akan memilih harus login menggunakan webmail uad
	- Yang berhak memilih adalah mahasiswa aktif UAD
	- Pemilih harus berasal dari Prodi Teknik Informatika
	- Jika pengkondisian di atas TIDAK SESUAI, maka TIDAK BISA melakukan pemilihan
	- Jika pengkondisian di atas SESUAI, maka kita BISA melakukan pemilihan
	- Pemilihan ini memiliki 3 opsi yang bisa dilakukan, yaitu: memilih paslon, memilih paslon (Termasuk GOLPUT) & Tidak memilih salah satu paslon (Termasuk GOLPUT)
	- Pemilihan hanya bisa di lakukan 1x (tidak bisa merubah paslon yang sudah di pilih)
	- Setelah memilih di perbolehkan mengisi form untuk Kritik & Saran terhadap Aplikasi Ini ataupun untuk paslon

*Fitur
	- Memiliki 2 halaman website: Halaman Utama & Halaman Portal Admin
	- Halaman Utama berfungsi untuk melakukan pemilihan & kampanya berbentuk banner/flyer
	- Di Halaman Utama juga terdapat form untuk Kritik & Saran terhadap Aplikasi Ini ataupun untuk paslon
	- Di Halaman Portal Admin, terdapat fitur sebagai berikut:
		- Fitur Rekap Mahasiswa Aktif (Mengetahui Daftar Mahasiswa yang memiliki hak memilih). Data Mahasiswa Aktif bisa di inport dari file .xps ataupun input manual (CRUD). Jumlah semua mahasiswa aktif & mahasiswa aktif per angkatan juga terekap di sini
		- Fitur Rekap Partisipasi Pencoblos (Mengetahui Mahasiswa yang sudah login, tapi belum melakukan pemilihan). Jumlah semua Partisipasi Pencoblos & Partisipasi Pencoblos per angkatan juga terekap di sini. Jumlah Partisipan yang sudah mencoblos maupun belum mencoblos juga terekap di sini.
		- Fitur Rekap Hasil Vote (Mengetahui jumlah surat suara yang masuk). Disini bisa diketahui paslon yang dipilih (surat suara sah) maupun surat suara tidak sah (GOLPUT)
		- Fitur Rincian Hasil Vote (Merekap jumlah surat suara sah maupun tidak sah), kemudian diketahui pemenang sementara dari surat suara yang masuk.
		- Fitur Rekap Hasil Vote Per-Angkatan (mengetahui pemenang paslon di setiap angkatannya)
		- Semua rekapan sudah terdapat diagram Bar Chart & Donuts Chart
		- Semua rekapan bisa di export ke .PDF
		- Fitur Rekap Kritik & Saran (Merekap Kritik & Saran dari partisipan pemilih)
		- Terdapat beberapa admin untuk membantu perekapan data (setiap admin memiliki hak akses sendiri dengan menggunakan username & password)
