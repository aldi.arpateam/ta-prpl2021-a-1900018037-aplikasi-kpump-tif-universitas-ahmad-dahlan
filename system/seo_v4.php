<?php

$author		= "kpump-tif.arpateam.com";

$default 	= "PEMILIHAN KETUA & WAKIL KETUA HMTIF";
$default2 	= "PEMILIHAN KETUA & WAKIL KETUA HMTIF";
$default3 	= "kpump-tif.arpateam.com";
$default4 	= "https://www.kpump-tif.arpateam.com";

if(($_GET['module']=='home')){
	$tseo = $pdo->query("SELECT title, keyword, description FROM page WHERE id_page='$_GET[id]'");
	$seo = $tseo->fetch(PDO::FETCH_ASSOC);
	
	$title = "$seo[title]";
	$keyword = "$seo[keyword]";
	$description = "$seo[description]";
	
	$imageshare = "images/default-share.jpg";
	$urlshare = $default4;

}else{
	$tseo = $pdo->query("SELECT title, keyword, description FROM page WHERE id_page='0'");
	$seo = $tseo->fetch(PDO::FETCH_ASSOC);

	$title = "$seo[title]";
	$keyword = "$seo[keyword]";
	$description = "$seo[description]";
	
	$imageshare = "images/default-share.jpg";
	$urlshare = $default4;
}
?>
	<meta charset="utf-8">

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="HandheldFriendly" content="true">

	<meta name="googlebot" content="index,follow">
	<meta name="googlebot-news" content="index,follow">
	<meta name="robots" content="index,follow">
	<meta name="Slurp" content="all">

	<!-- Tempat verivikasi search console -->

	<!-- index ke google, bing & yahoo saja -->

	<!-- Tempat verivikasi search console -->

	<title><?php echo $title; ?></title>

	<meta name="keywords" content="<?php echo $keyword; ?>">
	<meta name="description" content="<?php echo $description; ?>">

	<!-- Untuk Facebook -->

	<meta property="fb:app_id" content="184663602948248">

	<!-- Untuk Facebook -->

	<!-- Untuk Twitter -->

	<meta name="twitter:card" content="summary" />
	<meta name="twitter:site" content="<?php echo $default; ?>" />
	<meta name="twitter:creator" content="<?php echo $author; ?>" />

	<!-- Untuk Twitter -->

	<meta property="og:url" content="<?php echo $urlshare; ?>">
	<meta property="og:type" content="article">
	<meta property="og:title" content="<?php echo $title; ?>">
	<meta property="og:description" content="<?php echo $description; ?>">
	<meta property="og:site_name" content="<?php echo $default; ?>">
	  
	<meta name="image_src" content="<?php echo $default4; ?>/<?php echo $imageshare; ?>">
	<meta property="og:image" content="<?php echo $default4; ?>/<?php echo $imageshare; ?>">
	<meta property="og:image:alt" content="Gambar <?php echo $title; ?>">
	<meta property="og:image:type" content="image/jpeg" />
	<meta property="og:image:width" content="480" />
	<meta property="og:image:height" content="269" />
		
	<link rel="canonical" href="<?php echo $urlshare; ?>">
	<link rel="shortlink" href="<?php echo $default3; ?>">