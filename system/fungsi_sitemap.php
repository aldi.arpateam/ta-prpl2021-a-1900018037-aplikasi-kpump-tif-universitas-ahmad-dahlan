<?php

	function tambahSitemap_1($database_1, $id_sub_sitemap, $loc_1, $priority_1){

		include 'koneksi.php';

		try{

			$stmt = $pdo->prepare("INSERT INTO $database_1
							(id_sub_sitemap,loc_1,lastmod_1,priority_1)
							VALUES(:id_sub_sitemap,:loc_1,now(),:priority_1)" );
					
			$stmt->bindParam(":id_sub_sitemap", $id_sub_sitemap, PDO::PARAM_STR);
			$stmt->bindParam(":loc_1", $loc_1, PDO::PARAM_STR);
			$stmt->bindParam(":priority_1", $priority_1, PDO::PARAM_STR);
				
			$count = $stmt->execute();
					
			$insertId = $pdo->lastInsertId();
					
		}catch(PDOException $e){
			echo "<script>window.alert('Gagal tambah sitemap!'); window.location(history.back(-1))</script>";
			exit();
		}

	}

	function tambahSitemap_2($database_2, $id_sub_sitemap, $loc_2, $priority_2){

		include 'koneksi.php';

		try{

			$stmt = $pdo->prepare("INSERT INTO $database_2
							(id_sub_sitemap,loc_2,lastmod_2,priority_2)
							VALUES(:id_sub_sitemap,:loc_2,now(),:priority_2)" );
					
			$stmt->bindParam(":id_sub_sitemap", $id_sub_sitemap, PDO::PARAM_STR);
			$stmt->bindParam(":loc_2", $loc_2, PDO::PARAM_STR);
			$stmt->bindParam(":priority_2", $priority_2, PDO::PARAM_STR);
				
			$count = $stmt->execute();
					
			$insertId = $pdo->lastInsertId();
					
		}catch(PDOException $e){
			echo "<script>window.alert('Gagal tambah sitemap!'); window.location(history.back(-1))</script>";
			exit();
		}

	}

	function editSitemap_1($database, $id_sitemap, $id_sub_sitemap, $loc, $priority){

		include 'koneksi.php';

		try {
			$sql = "UPDATE $database
					SET id_sub_sitemap 			= :id_sub_sitemap,
						loc_1 					= :loc,
						lastmod_1 				= now(),
						priority_1 				= :priority
					WHERE id_$database 			= :id_sitemap
				";
						  
			$statement = $pdo->prepare($sql);

			$statement->bindParam(":id_sitemap", $id_sitemap, PDO::PARAM_INT);
			$statement->bindParam(":id_sub_sitemap", $id_sub_sitemap, PDO::PARAM_STR);
			$statement->bindParam(":loc", $loc, PDO::PARAM_STR);
			$statement->bindParam(":priority", $priority, PDO::PARAM_STR);

			$count = $statement->execute();
					
		}catch(PDOException $e){
			echo "<script>window.alert('SiteMap Gagal di edit!'); window.location(history.back(-1))</script>";
			exit();
		}

	}

	function editSitemap_2($database, $id_sitemap, $id_sub_sitemap, $loc, $priority){

		include 'koneksi.php';

		try {
			$sql = "UPDATE $database
					SET id_sub_sitemap 			= :id_sub_sitemap,
						loc_2 					= :loc,
						lastmod_2 				= now(),
						priority_2 				= :priority
					WHERE id_$database 			= :id_sitemap
				";
						  
			$statement = $pdo->prepare($sql);

			$statement->bindParam(":id_sitemap", $id_sitemap, PDO::PARAM_INT);
			$statement->bindParam(":id_sub_sitemap", $id_sub_sitemap, PDO::PARAM_STR);
			$statement->bindParam(":loc", $loc, PDO::PARAM_STR);
			$statement->bindParam(":priority", $priority, PDO::PARAM_STR);

			$count = $statement->execute();
					
		}catch(PDOException $e){
			echo "<script>window.alert('SiteMap Gagal di edit!'); window.location(history.back(-1))</script>";
			exit();
		}

	}

	function hapusSitemap($database, $id_sitemap){

		include 'koneksi.php';

		$del = $pdo->query("DELETE FROM $database WHERE id_$database='$id_sitemap'");
		$del->execute();

	}