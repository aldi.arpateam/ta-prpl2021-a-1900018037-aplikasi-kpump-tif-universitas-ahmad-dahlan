<?php
session_start();
error_reporting(0);

if (empty($_SESSION['namaadmin']) AND empty($_SESSION['leveladmin'])){
	echo "<link href='style.css' rel='stylesheet' type='text/css'>
	<center>Untuk mengakses Halaman ini, Anda harus login <br>";
	echo "<a href=../../index.php><b>LOGIN</b></a></center>";

}else{

	require "../../../system/koneksi.php";
	require "../../../system/fungsi_upload_gambar.php";
	require "../../../system/fungsi_form.php";
	require "../../../system/fungsi_seo.php";
	require "../../../system/fungsi_seo2.php";

	// Data file

	$hal 				= "Konten Halaman";
	$link 				= "konten-halaman";

	$penyimpananGambar	= "../../../images/asset-konten-halaman";

	$database			= "konten_halaman";
	$module 			= $_GET["module"];
	$module2 			= "konten-halaman";
	$act 				= $_GET["act"];

	// Data file

	if($module==$module2 AND $act=='add'){

		$jenis_konten_halaman	= $_POST['in_jenis_konten_halaman'];
		$judul					= $_POST['in_judul'];

		// Cek nilai yang belum di isi
		formWajib($jenis_konten_halaman);
		// Cek nilai yang belum di isi

		try {

			$stmt = $pdo->prepare("INSERT INTO konten_halaman
							(jenis_konten_halaman,judul,tgl_update)
							VALUES(:jenis_konten_halaman,:judul, now() )" );
					
			$stmt->bindParam(":jenis_konten_halaman", $jenis_konten_halaman, PDO::PARAM_STR);
			$stmt->bindParam(":judul", $judul, PDO::PARAM_STR);

			$count = $stmt->execute();

			$insertId = $pdo->lastInsertId();
			
			echo "<script>alert('$hal berhasil di Tambah'); window.location = '../../$link';</script>";

		}catch(PDOException $e){
			echo "<script>alert('$hal gagal di Tambah'); window.history.back();</script>";
		}
	}

	elseif($module==$module2 AND $act=='edit'){

		$id_konten_halaman		= $_POST['in_id_konten_halaman'];
		$jenis_konten_halaman	= $_POST['in_jenis_konten_halaman'];
		$judul					= $_POST['in_judul'];

		$deskripsi				= $_POST['in_deskripsi'];

		$acak					= rand(00,99);

		$judul_gambar_1			= $_POST['in_judul_gambar_1'];

		$lokasi_file_1 			= $_FILES['in_gambar_1']['tmp_name'];
		$lokasi_upload_1		= "$penyimpananGambar/";
		$nama_file_1   			= $_FILES['in_gambar_1']['name'];
		$tipe_file_1   			= strtolower($_FILES['in_gambar_1']['type']);
		$tipe_file2_1   		= seo2($tipe_file_1); // ngedapetin png / jpg / jpeg
		$ukuran_1   			= $_FILES['in_gambar_1']['size'];
		$nama_file_unik_1 		= seo($judul_gambar_1)."-".$acak.".".$tipe_file2_1;

		$judul_gambar_2			= $_POST['in_judul_gambar_2'];

		$lokasi_file_2 			= $_FILES['in_gambar_2']['tmp_name'];
		$lokasi_upload_2		= "$penyimpananGambar/";
		$nama_file_2   			= $_FILES['in_gambar_2']['name'];
		$tipe_file_2   			= strtolower($_FILES['in_gambar_2']['type']);
		$tipe_file2_2   		= seo2($tipe_file_2); // ngedapetin png / jpg / jpeg
		$ukuran_2   			= $_FILES['in_gambar_2']['size'];
		$nama_file_unik_2 		= seo($judul_gambar_2)."-".$acak.".".$tipe_file2_2;

		// Cek nilai yang belum di isi
		formWajib($jenis_konten_halaman);
		// Cek nilai yang belum di isi

		if ($jenis_konten_halaman == "Deskripsi") {

			$deskripsinya			= $deskripsi;

			// Eksekusi gambar_1
			$CekGambar_1 			= $pdo->query("SELECT gambar_1 FROM $database WHERE id_$database='$id_konten_halaman'");
			$tCekGambar_1 			= $CekGambar_1->fetch(PDO::FETCH_ASSOC);

			$cariExtensiGambar_1	= explode(".", $tCekGambar_1["gambar_1"]);
			$extensiGambarnya_1 	= $cariExtensiGambar_1[1];
			// Eksekusi gambar_1

			// Eksekusi gambar_2
			$CekGambar_2 			= $pdo->query("SELECT gambar_2 FROM $database WHERE id_$database='$id_konten_halaman'");
			$tCekGambar_2 			= $CekGambar_2->fetch(PDO::FETCH_ASSOC);

			$cariExtensiGambar_2	= explode(".", $tCekGambar_2["gambar_2"]);
			$extensiGambarnya_2 	= $cariExtensiGambar_2[1];
			// Eksekusi gambar_2

			echo "<br />".$penyimpananGambar."/".$tCekGambar_1["gambar_1"];
			echo "<br />".$penyimpananGambar."/".$tCekGambar_2["gambar_2"];

			// Hapus gambar 1
			unlink("$penyimpananGambar/$tCekGambar_1[gambar_1]");
			$judul_gambar_edit_1	= NULL;
			$nama_file_edit_1		= NULL;
			// Hapus gambar 1

			// Hapus gambar 2
			unlink("$penyimpananGambar/$tCekGambar_2[gambar_2]");
			$judul_gambar_edit_2	= NULL;
			$nama_file_edit_2		= NULL;
			// Hapus gambar 2

		}

		if ($jenis_konten_halaman == "Gambar 1") {

			$deskripsinya			= NULL;
			$judul_gambar_edit_1	= $judul_gambar_1;
			$judul_gambar_edit_2	= NULL;

			// Eksekusi gambar_1
			$CekGambar_1 			= $pdo->query("SELECT gambar_1 FROM $database WHERE id_$database='$id_konten_halaman'");
			$tCekGambar_1 			= $CekGambar_1->fetch(PDO::FETCH_ASSOC);

			$cariExtensiGambar_1	= explode(".", $tCekGambar_1["gambar_1"]);
			$extensiGambarnya_1 	= $cariExtensiGambar_1[1];

			if (!empty($nama_file_1)) {

				// Cek ukuran yang di upload
				cekUkuranFile2mb($ukuran_1);
				// Cek ukuran yang di upload

				// Cek jenis file yang di upload
				cekFileGambar($tipe_file_1);
				// Cek jenis file yang di upload

				// Hapus gambar
				unlink("$penyimpananGambar/$tCekGambar_1[gambar_1]");
				// Hapus gambar

				uploadGambarAsli($nama_file_unik_1, $tipe_file_1, $lokasi_file_1, $lokasi_upload_1);

				$nama_file_edit_1	= $nama_file_unik_1;

			}elseif (empty($nama_file_1)) {

				rename("$penyimpananGambar/$tCekGambar_1[gambar_1]", "$penyimpananGambar/$nama_file_unik_1".$extensiGambarnya_1);

				$nama_file_edit_1	= $nama_file_unik_1.$extensiGambarnya_1;

			}
			// Eksekusi gambar_1

		}
		

		if ($jenis_konten_halaman == "Gambar 2") {

			$deskripsinya			= NULL;
			$judul_gambar_edit_1	= $judul_gambar_1;
			$judul_gambar_edit_2	= $judul_gambar_2;

			// Eksekusi gambar_1
			$CekGambar_1 			= $pdo->query("SELECT gambar_1 FROM $database WHERE id_$database='$id_konten_halaman'");
			$tCekGambar_1 			= $CekGambar_1->fetch(PDO::FETCH_ASSOC);

			$cariExtensiGambar_1	= explode(".", $tCekGambar_1["gambar_1"]);
			$extensiGambarnya_1 	= $cariExtensiGambar_1[1];

			if (!empty($nama_file_1)) {

				// Cek ukuran yang di upload
				cekUkuranFile2mb($ukuran_1);
				// Cek ukuran yang di upload

				// Cek jenis file yang di upload
				cekFileGambar($tipe_file_1);
				// Cek jenis file yang di upload

				// Hapus gambar
				unlink("$penyimpananGambar/$tCekGambar_1[gambar_1]");
				// Hapus gambar

				uploadGambarAsli($nama_file_unik_1, $tipe_file_1, $lokasi_file_1, $lokasi_upload_1);

				$nama_file_edit_1	= $nama_file_unik_1;

			}elseif (empty($nama_file_1)) {

				rename("$penyimpananGambar/$tCekGambar_1[gambar_1]", "$penyimpananGambar/$nama_file_unik_1".$extensiGambarnya_1);

				$nama_file_edit_1	= $nama_file_unik_1.$extensiGambarnya_1;

			}
			// Eksekusi gambar_1

			// Eksekusi gambar_2
			$CekGambar_2 			= $pdo->query("SELECT gambar_2 FROM $database WHERE id_$database='$id_konten_halaman'");
			$tCekGambar_2 			= $CekGambar_2->fetch(PDO::FETCH_ASSOC);

			$cariExtensiGambar_2	= explode(".", $tCekGambar_2["gambar_2"]);
			$extensiGambarnya_2 	= $cariExtensiGambar_2[1];

			if (!empty($nama_file_2)) {

				// Cek ukuran yang di upload
				cekUkuranFile2mb($ukuran_2);
				// Cek ukuran yang di upload

				// Cek jenis file yang di upload
				cekFileGambar($tipe_file_2);
				// Cek jenis file yang di upload

				// Hapus gambar
				unlink("$penyimpananGambar/$tCekGambar_2[gambar_2]");
				// Hapus gambar

				uploadGambarAsli($nama_file_unik_2, $tipe_file_2, $lokasi_file_2, $lokasi_upload_2);

				$nama_file_edit_2	= $nama_file_unik_2;

			}elseif (empty($nama_file_2)) {

				rename("$penyimpananGambar/$tCekGambar_2[gambar_2]", "$penyimpananGambar/$nama_file_unik_2".$extensiGambarnya_2);

				$nama_file_edit_2	= $nama_file_unik_2.$extensiGambarnya_2;

			}
			// Eksekusi gambar_2
		}

		try {

			$sql = "UPDATE konten_halaman SET 
						judul 					= :judul,
						jenis_konten_halaman 	= :jenis_konten_halaman,
						deskripsi 				= :deskripsi,
						judul_gambar_1 			= :judul_gambar_1,
						gambar_1 				= :gambar_1,
						judul_gambar_2 			= :judul_gambar_2,
						gambar_2 				= :gambar_2,
						tgl_update 				= now()
					WHERE id_konten_halaman = :id_konten_halaman
				  ";
				  
			$statement = $pdo->prepare($sql);
			$statement->bindParam(":id_konten_halaman", $id_konten_halaman, PDO::PARAM_INT);
			$statement->bindParam(":judul", $judul, PDO::PARAM_STR);
			$statement->bindParam(":jenis_konten_halaman", $jenis_konten_halaman, PDO::PARAM_STR);
			$statement->bindParam(":deskripsi", $deskripsinya, PDO::PARAM_STR);
			$statement->bindParam(":judul_gambar_1", $judul_gambar_edit_1, PDO::PARAM_STR);
			$statement->bindParam(":gambar_1", $nama_file_edit_1, PDO::PARAM_STR);
			$statement->bindParam(":judul_gambar_2", $judul_gambar_edit_2, PDO::PARAM_STR);
			$statement->bindParam(":gambar_2", $nama_file_edit_2, PDO::PARAM_STR);

			$count = $statement->execute();
			
			echo "<script>alert('$hal berhasil di Edit'); window.location = '../../media.php?module=$module&act=edit&id=$id_konten_halaman'</script>";

		}catch(PDOException $e){

			echo "<script>alert('$hal gagal di Edit'); window.history.back();</script>";
		}
	}
}
?>

<center style="margin-top: 250px;"><img src="../../load.gif"></center>