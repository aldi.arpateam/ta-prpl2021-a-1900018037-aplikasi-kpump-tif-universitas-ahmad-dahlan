<?php
if (empty($_SESSION['namaadmin']) AND empty($_SESSION['leveladmin'])){
	echo "<link href='style.css' rel='stylesheet' type='text/css'>
	<center>Untuk mengakses Halaman ini, Anda harus login <br>";
	echo "<a href=../../index.php><b>LOGIN</b></a></center>";

}else{

	$aksi 				= "modul/konten-halaman/aksi.php";
	$hal 				= "Konten Halaman";
	$penyimpananGambar	= "../images/asset-konten-halaman";
	$module 			= "konten-halaman";
	$database 			= "konten_halaman";
	$link 				= "konten-halaman";

	switch($_GET['act']){
		// Tampil Modul
	  	default:
	?>

		<section class="content">
			<div class="row">
				<div class="col-xs-12">

					<div class="box">

						<div class="box-header">
							<h1 style="text-transform: capitalize;"><?= $hal; ?></h1>
							<ol class="breadcrumb">
								<li><a href="media.php?module=home"><i class="fa fa-dashboard"></i> Home</a></li>
								<li class="active"><?= $hal; ?></li>
							</ol>
						</div>

						<div class="box-body">
							<a href="<?= $link; ?>-add" class="btn btn-block btn-success"><i class="fa fa-plus"></i> Tambah <?= $hal; ?></a>
						</div>

						<div class="box-body" style="overflow-x:auto;">
							<table id="example1" class="table table-bordered table-striped">

								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="5%">Id</th>
										<th width="25%">Judul</th>
										<th width="30%">Jenis Konten Halaman</th>
										<th width="20%">Tanggal Update</th>
										<th width="15%">Aksi</th>
									</tr>
								</thead>

								<tbody class="text-center">

									<?php
										$no = 1;
										$tampil = $pdo->query("SELECT * FROM $database ORDER BY id_$database ASC");
											while($r = $tampil->fetch(PDO::FETCH_ASSOC)){
									?>

									<tr>
										<td><?= $no; ?></td>
										<td><?= $r['id_konten_halaman']; ?></td>
										<td><?= $r['judul']; ?></td>
										<td><span class="badge btn-success"><?= $r['jenis_konten_halaman']; ?></span></td>
										<td><?= tgl2($r['tgl_update']); ?></td>
										<td>
											<a href="<?= $link; ?>-edit-<?= $r['id_konten_halaman']; ?>" class="btn btn-primary"><i class="fa fa-edit"></i></a>
										</td>
									</tr>

									<?php
										$no++;
										}
									?>

								</tbody>

							</table>

						</div><!-- /.box-body -->

					</div><!-- /.box -->
			   
				</div><!-- /.col -->
			</div><!-- /.row -->
		</section>
			
	<?php
		break;
		case "add":

		$action	= "add";
	?>

		<section class="content">
			<div class="row">
			  
				<!-- left column -->
				<div class="col-md-12">

				  	<!-- general form elements -->
				  	<div class="box box-primary">
						<div class="box-header">
							<h1 style="text-transform: capitalize;"><?= $hal; ?></h1>
							<ol class="breadcrumb">
								<li><a href="media.php?module=home"><i class="fa fa-dashboard"></i> Home</a></li>
								<li><a href="<?= $link; ?>"><?= $hal; ?></a></li>
								<li class="active">Tambah <?= $hal; ?></li>
							</ol>
						</div><!-- /.box-header -->

						<div class="box-body">
							<div class="alert alert-warning alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				            	<h4><i class="icon fa fa-warning"></i> Perhatian!</h4>
				                Mohon isi data dengan benar & lengkap
				            </div>
				        </div>

						<!-- form start -->
						<form role="form" action="modul/<?= $link; ?>/aksi.php?module=<?= $module; ?>&act=<?= $action; ?>" method="POST">
						
							<div class="box-body">

								<div class="col-md-12">
									<div class="form-group">
										<label for="jenis_konten_halaman">Jenis Konten Halaman</label>
										<select id="jenis_konten_halaman" name="in_jenis_konten_halaman" class="form-control">

											<option value="0">- Pilih Jenis Konten Halaman -</option>

											<option value="Deskripsi">Deskripsi</option>
											<option value="Gambar 1">Gambar 1</option>
											<option value="Gambar 2">Gambar 2</option>

										</select>
									</div>
								</div>
						
								<div class="col-md-12">
									<div class="form-group">
										<label for="judul">Judul <?= $hal; ?></label>
										<input id="judul" name="in_judul" type="text" class="form-control" placeholder="Cth: Konten Halaman Home" required>
									</div>
								</div>
								
							</div><!-- /.box-body -->

							<div class="box-footer">
								<button type="submit" class="btn btn-success">Selesai <i class="fa fa-check-square-o"></i></button>
								<a href="<?= $link; ?>"  class="btn btn-warning">Batal <i class="fa fa-times"></i></a>
							</div>
						</form>
				  	</div><!-- /.box -->

				</div>

			</div>
		</section>
			
	<?php

		break;
		case "edit":
		$action	= "edit";

		$edit = $pdo->query("SELECT * FROM konten_halaman WHERE id_konten_halaman='$_GET[id]'");
		$tedit = $edit->fetch(PDO::FETCH_ASSOC);

	?>
		
		<section class="content">
			<div class="row">

				<div class="col-md-12">

					<!-- general form elements -->
					<div class="box box-primary">

						<div class="box-header with-border">
							<h1 style="text-transform: capitalize;">Edit <?= $hal; ?></h1>
							<ol class="breadcrumb">
								<li><a href="media.php?module=home"><i class="fa fa-dashboard"></i> Home</a></li>
								<li><a href="<?= "$link"; ?>"><?= $hal; ?></a></li>
								<li class="active">Edit <?= $tedit['judul']; ?></li>
							</ol>
						</div><!-- /.box-header -->

						<div class="box-body">
							<div class="alert alert-warning alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				            	<h4><i class="icon fa fa-warning"></i> Perhatian!</h4>
				                Mohon isi data dengan benar & lengkap
				            </div>
				        </div>

						<!-- form start -->
						<form role="form" action="modul/<?= $module; ?>/aksi.php?module=<?= $module; ?>&act=<?= $action; ?>" method="POST" enctype="multipart/form-data">

							<input id="id_konten_halaman" name="in_id_konten_halaman" type="hidden" class="form-control" value="<?= $tedit['id_konten_halaman']; ?>" required>
						
							<div class="box-body">

								<div class="col-md-12">
									<div class="form-group">
										<label for="jenis_konten_halaman">Jenis Konten Halaman</label>
										<select id="jenis_konten_halaman" name="in_jenis_konten_halaman" class="form-control">

											<option value="Deskripsi" <?php if ($tedit['jenis_konten_halaman'] == "Deskripsi") { echo "selected"; } ?>>Deskripsi</option>
											<option value="Gambar 1" <?php if ($tedit['jenis_konten_halaman'] == "Gambar 1") { echo "selected"; } ?>>Gambar 1</option>
											<option value="Gambar 2" <?php if ($tedit['jenis_konten_halaman'] == "Gambar 2") { echo "selected"; } ?>>Gambar 2</option>

										</select>
									</div>
								</div>
						
								<div class="col-md-12">
									<div class="form-group">
										<label for="judul">Judul <?= $hal; ?></label>
										<input id="judul" name="in_judul" type="text" class="form-control" value="<?= $tedit['judul']; ?>" required>
									</div>
								</div>

								<?php

									if ($tedit['jenis_konten_halaman'] == "Deskripsi") {

								?>

								<div class="col-md-12">

									<div class="form-group">
										<label for="deskripsi">Deskripsi Konten</label>
										<textarea id="deskripsi" class="ckeditor" name="in_deskripsi"><?= $tedit['deskripsi']; ?></textarea>
									</div>

								</div>

								<?php 
									}
								?>

								<?php

									if ($tedit['jenis_konten_halaman'] == "Gambar 1") {

								?>

								<div class="col-md-12">
									<div class="form-group">
										<label for="judul_gambar_1">Judul Gambar 1</label>
										<input id="judul_gambar_1" name="in_judul_gambar_1" type="text" class="form-control" value="<?= $tedit['judul_gambar_1']; ?>" required>
									</div>
								</div>
								
								<div class="col-md-12">
									<div class="form-group">

										<label for="gambar_1">Gambar 1</label>
										<br /><br />

										<div class="photo">
											<img src="<?= $penyimpananGambar; ?>/<?= $tedit['gambar_1']; ?>" alt="<?= $tedit['judul_gambar_1']; ?>" width="250">
										</div>
										<br />
										
										<input id="gambar_1" name="in_gambar_1" type="file">

										<br />

							            <div class="alert alert-warning alert-dismissible">
							            	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							                <h4><i class="icon fa fa-warning"></i> Perhatian!</h4>
											<p>*) Maksimal Ukuran Gambar 2MB</p>
							            </div>
									
									</div><!-- /.box-body -->
								</div>

								<?php 
									}
								?>

								<?php

									if ($tedit['jenis_konten_halaman'] == "Gambar 2") {

								?>

								<div class="col-md-12">
									<div class="form-group">
										<label for="judul_gambar_1">Judul Gambar 1</label>
										<input id="judul_gambar_1" name="in_judul_gambar_1" type="text" class="form-control" value="<?= $tedit['judul_gambar_1']; ?>" required>
									</div>
								</div>
								
								<div class="col-md-12">
									<div class="form-group">

										<label for="gambar_1">Gambar 1</label>
										<br /><br />

										<div class="photo">
											<img src="<?= $penyimpananGambar; ?>/<?= $tedit['gambar_1']; ?>" alt="<?= $tedit['judul_gambar_1']; ?>" width="250">
										</div>
										<br />
										
										<input id="gambar_1" name="in_gambar_1" type="file">

										<br />

							            <div class="alert alert-warning alert-dismissible">
							            	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							                <h4><i class="icon fa fa-warning"></i> Perhatian!</h4>
											<p>*) Maksimal Ukuran Gambar 2MB</p>
							            </div>
									
									</div><!-- /.box-body -->
								</div>

								<div class="col-md-12">
									<div class="form-group">
										<label for="judul_gambar_2">Judul Gambar 2</label>
										<input id="judul_gambar_2" name="in_judul_gambar_2" type="text" class="form-control" value="<?= $tedit['judul_gambar_2']; ?>" required>
									</div>
								</div>
								
								<div class="col-md-12">
									<div class="form-group">

										<label for="gambar_2">Gambar 2</label>
										<br /><br />

										<div class="photo">
											<img src="<?= $penyimpananGambar; ?>/<?= $tedit['gambar_2']; ?>" alt="<?= $tedit['judul_gambar_2']; ?>" width="250">
										</div>
										<br />
										
										<input id="gambar_2" name="in_gambar_2" type="file">

										<br />

							            <div class="alert alert-warning alert-dismissible">
							            	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							                <h4><i class="icon fa fa-warning"></i> Perhatian!</h4>
											<p>*) Maksimal Ukuran Gambar 2MB</p>
							            </div>
									
									</div><!-- /.box-body -->
								</div>

								<?php 
									}
								?>
								
							</div><!-- /.box-body -->
		
							<div class="box-footer">
								<button type="submit" class="btn btn-success">Selesai <i class="fa fa-check-square-o"></i></button>
								<a href="<?= $link; ?>"  class="btn btn-warning">Batal <i class="fa fa-times"></i></a>
							</div>

						</form>

					</div><!-- /.box -->

				</div>

			</div>
		</section>
			
	<?php
		break;  
	}
}
?>
