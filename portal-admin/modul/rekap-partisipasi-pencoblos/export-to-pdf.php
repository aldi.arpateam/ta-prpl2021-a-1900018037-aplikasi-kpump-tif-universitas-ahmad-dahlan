<!DOCTYPE html>
<html>
<head>
	<title>REKAP DATA PARTISIPASI PENCOBLOS - www.kpump-tif.arpateam.com</title>

	<style type="text/css">

		.ket{
			text-align: center;
			margin-bottom: 25px;
			padding: 10px;
			box-sizing: border-box;
			background-color: #223668;
			color: #F1692B;
		}

		.informasi{
			margin-top: 25px;
			margin-bottom: 25px;
			padding: 10px;
			background-color: #F39C12;
			border-radius: 10px;
			color: #fff;
		}

		.bg-green{
			background-color: #00A65A;
			border-radius: 10px;
			padding: 3px;
		}

		table{
			width: 100%;
			border-collapse: collapse;
		}

		table th,
		table td{
			border: 1px solid #3c3c3c;
		}

		table th{
			font-size: 18px;
			height: 35px;
	 		background-color: rgb(91, 155, 213);
	 	}

		table td{
			font-size: 16px;
			height: 25px;
	 	}

	 	tr:nth-child(even) {
		  	background-color: #f2f2f2;
		}

	</style>

</head>
<body>

	<?php $database = "vote"; ?>

	<header>
		<?php
			date_default_timezone_set("Asia/Jakarta");

			echo "<p>Date: ".date("Y-m-d H:i:s")." WIB</p>";
		?>
		<p></p>
	</header>

	<div class="ket">
		<h1>REKAP DATA PARTISIPASI PENCOBLOS</h1>
		<h2>www.kpump-tif.arpateam.com</h2>
	</div>
 
	<table border="1">

		<tr>
			<th width="10%">No</th>
			<th width="40%">NIM</th>
			<th width="60%">Nama Lengkap</th>
		</tr>

		<?php
			require '../../../system/koneksi.php';
			$no = 1;
			$tampil = $pdo->query("SELECT $database.nim, $database.nama, $database.hasil_vote FROM $database ORDER BY nim ASC");

			while($r = $tampil->fetch(PDO::FETCH_ASSOC)){
		?>

		<tr style="text-align: center;<?php if(empty($r['hasil_vote']) OR $r['hasil_vote']==0){ echo 'background-color: #DD4B39;color: #fff;'; } ?>">
			<td><?= $no++; ?></td>
			<td><?= $r['nim']; ?></td>
			<td><?= $r['nama']; ?></td>
		</tr>

		<?php
			}
		?>

	</table>

	<?php
		$totalPartisipasi = $pdo->query("SELECT nim FROM $database");
		$tampiltotalPartisipasi = $totalPartisipasi->rowCount();

		$PartisipasiAngkatan20 = $pdo->query("SELECT nim FROM $database WHERE nim LIKE '20%' ORDER BY nim ASC ");
		$tampilPartisipasiAngkatan20 = $PartisipasiAngkatan20->rowCount();

		$PartisipasiAngkatan19 = $pdo->query("SELECT nim FROM $database WHERE nim LIKE '19%' ORDER BY nim ASC ");
		$tampilPartisipasiAngkatan19 = $PartisipasiAngkatan19->rowCount();

		$PartisipasiAngkatan18 = $pdo->query("SELECT nim FROM $database WHERE nim LIKE '18%' ORDER BY nim ASC ");
		$tampilPartisipasiAngkatan18 = $PartisipasiAngkatan18->rowCount();

		$PartisipasiAngkatan17 = $pdo->query("SELECT nim FROM $database WHERE nim LIKE '17%' ORDER BY nim ASC ");
		$tampilPartisipasiAngkatan17 = $PartisipasiAngkatan17->rowCount();

		$PartisipasiAngkatan16 = $pdo->query("SELECT nim FROM $database WHERE nim LIKE '16%' ORDER BY nim ASC ");
		$tampilPartisipasiAngkatan16 = $PartisipasiAngkatan16->rowCount();

		$PartisipasiAngkatan15 = $pdo->query("SELECT nim FROM $database WHERE nim LIKE '15%' ORDER BY nim ASC ");
		$tampilPartisipasiAngkatan15 = $PartisipasiAngkatan15->rowCount();

		$PartisipasiAngkatan14 = $pdo->query("SELECT nim FROM $database WHERE nim LIKE '14%' ORDER BY nim ASC ");
		$tampilPartisipasiAngkatan14 = $PartisipasiAngkatan14->rowCount();

		$PartisipasiAngkatan13 = $pdo->query("SELECT nim FROM $database WHERE nim LIKE '13%' ORDER BY nim ASC ");
		$tampilPartisipasiAngkatan13 = $PartisipasiAngkatan13->rowCount();

			$partisipasiSudahNyoblos = $pdo->query("SELECT nim FROM $database WHERE hasil_vote!=NULL OR hasil_vote!='0'");
			$tampilpartisipasiSudahNyoblos = $partisipasiSudahNyoblos->rowCount();

			$partisipasiBelumNyoblos = $pdo->query("SELECT nim FROM $database");
			$tampilpartisipasiBelumNyoblos = $partisipasiBelumNyoblos->rowCount();

	?>

	<div class="informasi">
		<h4>TERDAPAT <span class="bg-green"><?= $tampiltotalPartisipasi; ?></span> Partisipasi Pencoblos, Terdiri dari:</h4>
        <p><span class="bg-green"><?= $tampilPartisipasiAngkatan20; ?></span> Dari Angkatan 2020</p>
        <p><span class="bg-green"><?= $tampilPartisipasiAngkatan19; ?></span> Dari Angkatan 2019</p>
        <p><span class="bg-green"><?= $tampilPartisipasiAngkatan18; ?></span> Dari Angkatan 2018</p>
        <p><span class="bg-green"><?= $tampilPartisipasiAngkatan17; ?></span> Dari Angkatan 2017</p>
        <p><span class="bg-green"><?= $tampilPartisipasiAngkatan16; ?></span> Dari Angkatan 2016</p>
        <p><span class="bg-green"><?= $tampilPartisipasiAngkatan15; ?></span> Dari Angkatan 2015</p>
        <p><span class="bg-green"><?= $tampilPartisipasiAngkatan14; ?></span> Dari Angkatan 2014</p>
        <p><span class="bg-green"><?= $tampilPartisipasiAngkatan13; ?></span> Dari Angkatan 2013</p>

    	<h4><i class="icon fa fa-warning"></i> Dengan rincian Partisipasi Pencoblos sebagai berikut:</h4>
        <p>Sebanyak <span class="label bg-green"><?= $tampilpartisipasiSudahNyoblos; ?></span> <strong>Partisipan</strong> <u>sudah</u> mencoblos</p>
        <p>Sebanyak <span class="label bg-green"><?= $tampilpartisipasiBelumNyoblos-$tampilpartisipasiSudahNyoblos; ?></span> <strong>Partisipan</strong> <u>belum</u> mencoblos</p>
		
	</div>

	<script>
		window.print();
	</script>

</body>
</html>