<?php
if (empty($_SESSION['namaadmin']) AND empty($_SESSION['leveladmin'])){
	echo "<script>window.alert('Untuk mengakses halaman ini ada harus Login!'); window.location = 'login';</script>";
}else{

	$hal 				= "Rekap Partisipasi Pencoblos";
	$database 			= "vote";

	if (isset($_POST['submit'])) {
		$del = $pdo->query("DELETE FROM $database");
		$del->execute();
		echo "<script>alert('Data Login Portal Admin berhasil di reset');</script>";
	}
	
?>

	<section class="content">

			<div class="row">

				<div class="col-md-12">

					<div class="box">
						<div class="box-header">
							<h1 style="text-transform: capitalize;"><?= $hal; ?></h1>
							<ol class="breadcrumb">
								<li><a href="media.php?module=home"><i class="fa fa-dashboard"></i> Dasboard</a></li>
								<li class="active"><?= $hal; ?></li>
							</ol>
						</div><!-- /.box-header -->

						<?php
							$totalPartisipasi = $pdo->query("SELECT nim FROM $database");
							$tampiltotalPartisipasi = $totalPartisipasi->rowCount();

							$PartisipasiAngkatan20 = $pdo->query("SELECT nim FROM $database WHERE nim LIKE '20%' ORDER BY nim ASC ");
							$tampilPartisipasiAngkatan20 = $PartisipasiAngkatan20->rowCount();

							$PartisipasiAngkatan19 = $pdo->query("SELECT nim FROM $database WHERE nim LIKE '19%' ORDER BY nim ASC ");
							$tampilPartisipasiAngkatan19 = $PartisipasiAngkatan19->rowCount();

							$PartisipasiAngkatan18 = $pdo->query("SELECT nim FROM $database WHERE nim LIKE '18%' ORDER BY nim ASC ");
							$tampilPartisipasiAngkatan18 = $PartisipasiAngkatan18->rowCount();

							$PartisipasiAngkatan17 = $pdo->query("SELECT nim FROM $database WHERE nim LIKE '17%' ORDER BY nim ASC ");
							$tampilPartisipasiAngkatan17 = $PartisipasiAngkatan17->rowCount();

							$PartisipasiAngkatan16 = $pdo->query("SELECT nim FROM $database WHERE nim LIKE '16%' ORDER BY nim ASC ");
							$tampilPartisipasiAngkatan16 = $PartisipasiAngkatan16->rowCount();

							$PartisipasiAngkatan15 = $pdo->query("SELECT nim FROM $database WHERE nim LIKE '15%' ORDER BY nim ASC ");
							$tampilPartisipasiAngkatan15 = $PartisipasiAngkatan15->rowCount();

							$PartisipasiAngkatan14 = $pdo->query("SELECT nim FROM $database WHERE nim LIKE '14%' ORDER BY nim ASC ");
							$tampilPartisipasiAngkatan14 = $PartisipasiAngkatan14->rowCount();

							$PartisipasiAngkatan13 = $pdo->query("SELECT nim FROM $database WHERE nim LIKE '13%' ORDER BY nim ASC ");
							$tampilPartisipasiAngkatan13 = $PartisipasiAngkatan13->rowCount();

								$partisipasiSudahNyoblos = $pdo->query("SELECT nim FROM $database WHERE hasil_vote!=NULL OR hasil_vote!='0'");
								$tampilpartisipasiSudahNyoblos = $partisipasiSudahNyoblos->rowCount();

								$partisipasiBelumNyoblos = $pdo->query("SELECT nim FROM $database");
								$tampilpartisipasiBelumNyoblos = $partisipasiBelumNyoblos->rowCount();

						?>

						<div class="box-body">
							<div class="alert alert-warning alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				            	<h4><i class="icon fa fa-warning"></i> TERDAPAT <span class="label bg-green"><?= $tampiltotalPartisipasi; ?></span> Partisipasi Pencoblos, Terdiri dari:</h4>
				                <p><span class="label bg-green"><?= $tampilPartisipasiAngkatan20; ?></span> Dari Angkatan 2020</p>
				                <p><span class="label bg-green"><?= $tampilPartisipasiAngkatan19; ?></span> Dari Angkatan 2019</p>
				                <p><span class="label bg-green"><?= $tampilPartisipasiAngkatan18; ?></span> Dari Angkatan 2018</p>
				                <p><span class="label bg-green"><?= $tampilPartisipasiAngkatan17; ?></span> Dari Angkatan 2017</p>
				                <p><span class="label bg-green"><?= $tampilPartisipasiAngkatan16; ?></span> Dari Angkatan 2016</p>
				                <p><span class="label bg-green"><?= $tampilPartisipasiAngkatan15; ?></span> Dari Angkatan 2015</p>
				                <p><span class="label bg-green"><?= $tampilPartisipasiAngkatan14; ?></span> Dari Angkatan 2014</p>
				                <p><span class="label bg-green"><?= $tampilPartisipasiAngkatan13; ?></span> Dari Angkatan 2013</p>

				                <br />

				            	<h4><i class="icon fa fa-warning"></i> Dengan rincian Partisipasi Pencoblos sebagai berikut:</h4>
				                <p>Sebanyak <span class="label bg-green"><?= $tampilpartisipasiSudahNyoblos; ?></span> <strong>Partisipan</strong> <u>sudah</u> mencoblos</p>
				                <p>Sebanyak <span class="label bg-green"><?= $tampilpartisipasiBelumNyoblos-$tampilpartisipasiSudahNyoblos; ?></span> <strong>Partisipan</strong> <u>belum</u> mencoblos</p>
				            </div>

				            <a target="_blank" href="https://kpump-tif.arpateam.com/aldiyuni150315/modul/rekap-partisipasi-pencoblos/export-to-pdf.php" role="button" class="btn btn-block btn-danger">Export to PDF <i class="fa fa-file-pdf-o"></i></a>

				        </div>
				
						<div class="box-body table-responsive" style="overflow-x: auto;">
							<table id="example1" class="table table-bordered table-striped">

								<thead>
						  			<tr>
										<th width="20%" class="center">No</th>
										<th width="40%" class="center">NIM</th>
										<th width="40%" class="center">Nama</th>
						  			</tr>
								</thead>

								<tbody>

									<!-- Query menampilkan rekap partisipasi pencoblos -->

									<?php
										$no = 1;
										$tampil = $pdo->query("SELECT $database.nim, $database.nama, $database.hasil_vote FROM $database ORDER BY nim ASC");

										while($r = $tampil->fetch(PDO::FETCH_ASSOC)){
									?>

									<tr style="text-align: center;" class="<?php if(empty($r['hasil_vote']) OR $r['hasil_vote']==0){ echo 'bg-red'; } ?>">
										<td><?= $no++; ?></td>
										<td><?= $r['nim']; ?></td>
										<td><?= $r['nama']; ?></td>
									</tr>

									<?php
										}
									?>

								</tbody>

					 		</table>
						</div><!-- /.box-body -->

				  	</div><!-- /.box -->
			   
				</div><!-- /.col -->

			</div>
		
		</section><!-- /.col -->
		
<?php
	}
?>