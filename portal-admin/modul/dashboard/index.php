<?php
if (empty($_SESSION['namaadmin']) AND empty($_SESSION['leveladmin'])){
	echo "<script>window.alert('Untuk mengakses halaman ini ada harus Login!'); window.location = 'login';</script>";
}else{

	$hal 				= "Dashboard";
	$database 			= "data_login_portal_admin";

	if (isset($_POST['submit'])) {
		$del = $pdo->query("DELETE FROM $database");
		$del->execute();
		echo "<script>alert('Data Login Portal Admin berhasil di reset');</script>";
	}
	
?>
		
	<section class="content">

		<div class="row">

			<div class="col-md-12">

				<div class="box box-primary">
					<div class="box-header">
						<h3>
							Aktivitas  
							<small>Login Portal Admin</small>
							<button onclick="return confirm('Apakah anda ingin mereset Aktivitas Login?')" name="submit" class="btn btn-success">Reset Aktivitas <i class="fa fa-refresh fa-spin"></i></button>
						</h3>
					</div><!-- /.box-header -->

					<div class="box-body">

						<table class="table table-condensed">

							<tr>
								<th width="5%">No</th>
								<th width="15%">Nama</th>
								<th width="10%">Level Admin</th>
								<th width="10%">Status Login</th>
								<th width="15%">Waktu Login</th>
								<th width="15%">Waktu Logout</th>
							</tr>

							<?php

								$no = 1;
								$Aktivitas = $pdo->query("SELECT * FROM $database WHERE level!='Administrator' ORDER BY id_$database DESC");
									while($resultAktivitas = $Aktivitas->fetch(PDO::FETCH_ASSOC)){

							?>

								<tr>
									<td><?= $no++; ?></td>
									<td><?= $resultAktivitas['nama']; ?></td>
									<td class="text-center">
									<?php
										if ($resultAktivitas['level'] == "Admin Utama") {
											echo "<span class='badge btn-success'>Admin Utama</span>";
										}elseif ($resultAktivitas['level'] == "Penulis") {
											echo "<span class='badge btn-warning'>Penulis</span>";
										}elseif ($resultAktivitas['level'] == "Marketing") {
											echo "<span class='badge btn-info'>Marketing</span>";}
									?>
												
									</td>
									<td class="text-center">

										<?php

											if ($resultAktivitas['status_login'] == "Berhasil") {
												echo "<span class='badge btn-success'>Berhasil</span>";
											}elseif ($resultAktivitas['status_login'] == "Gagal") {
												echo "<span class='badge bg-red'>Gagal</span>";
											}

										?>
											
									</td>
									<td><?= tgl2($resultAktivitas['waktu_login']); ?></td>
									<td><?= tgl2($resultAktivitas['waktu_logout']); ?></td>
								</tr>

							<?php
								}
							?>

						</table>

					</div>

				</div>

			</div>

		</div>

	</section>
		
<?php
	}
?>