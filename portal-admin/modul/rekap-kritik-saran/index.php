<?php
if (empty($_SESSION['namaadmin']) AND empty($_SESSION['leveladmin'])){
	echo "<script>window.alert('Untuk mengakses halaman ini ada harus Login!'); window.location = 'login';</script>";
}else{

	$hal 				= "Rekap Kritik & Saran";
	$database 			= "kritik_saran";

	if (isset($_POST['submit'])) {
		$del = $pdo->query("DELETE FROM $database");
		$del->execute();
		echo "<script>alert('Data Login Portal Admin berhasil di reset');</script>";
	}
	
?>

	<section class="content">

			<div class="row">

				<div class="col-md-12">

					<div class="box box-primary">
						<div class="box-header">
							<h1 style="text-transform: capitalize;"><?= $hal; ?></h1>
							<ol class="breadcrumb">
								<li><a href="media.php?module=home"><i class="fa fa-dashboard"></i> Dasboard</a></li>
								<li class="active"><?= $hal; ?></li>
							</ol>
						</div><!-- /.box-header -->
				
						<div class="box-body table-responsive" style="overflow-x: auto;">
							<table id="example1" class="table table-bordered table-striped">

								<thead>
						  			<tr>
										<th width="5%" class="center">No</th>
										<th width="15%" class="center">NIM</th>
										<th width="20%" class="center">NAMA</th>
										<th width="45%" class="center">Kritik & Saran</th>
										<th width="15%" class="center">Waktu</th>
						  			</tr>
								</thead>

								<tbody>

									<!-- Query menampilkan rekap kritik & saran -->
									<?php
										$no = 1;
										$tampil = $pdo->query("SELECT nim,nama,kritik_saran,waktu FROM $database  ORDER BY id_kritik_saran DESC");

										while($r = $tampil->fetch(PDO::FETCH_ASSOC)){
									?>

									<tr style="text-align: center;">
										<td><?= $no++; ?></td>
										<td><?= $r['nim']; ?></td>
										<td><?= $r['nama']; ?></td>
										<td><?= $r['kritik_saran']; ?></td>
										<td><?= $r['waktu']; ?></td>
									</tr>

									<?php
										}
									?>

								</tbody>

					 		</table>
						</div><!-- /.box-body -->

				  	</div><!-- /.box -->
			   
				</div><!-- /.col -->

			</div>
		
		</section><!-- /.col -->
		
<?php
	}
?>