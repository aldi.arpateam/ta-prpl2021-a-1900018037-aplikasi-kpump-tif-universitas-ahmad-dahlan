<?php
session_start();
// error_reporting(0);

if (empty($_SESSION['namaadmin']) AND empty($_SESSION['leveladmin'])){
	echo "<link href='style.css' rel='stylesheet' type='text/css'>
	<center>Untuk mengakses modul, Anda harus login <br>";
	echo "<a href=../../index.php><b>LOGIN</b></a></center>";

}else{
	require "../../../system/koneksi.php";
	require "../../../system/fungsi_upload_gambar.php";
	require "../../../system/fungsi_form.php";
	require "../../../system/fungsi_seo.php";
	require "../../../system/fungsi_seo2.php";
    require "../../../system/z_setting.php";
	require "../../../system/fungsi_sitemap.php";

	// Data file

	$hal 		= "Mahasiswa Aktif";
	$link 		= "mahasiswa-aktif";

	$database	= "data_mahasiswa_aktif";
	$module 	= $_GET["module"];
	$module2 	= "mahasiswa-aktif";
	$act 		= $_GET["act"];
	// Data file

	// Nilai yang akan di input

		$nim 	= $_POST['in_nim'];
		$nama_lengkap 	= $_POST['in_nama_lengkap'];

		$nim_2 	= $_POST['in_nim_2'];
		$nama_lengkap_2 = $_POST['in_nama_lengkap_2'];

		$nim_3 	= $_POST['in_nim_3'];
		$nama_lengkap_3 = $_POST['in_nama_lengkap_3'];

		$nim_4 	= $_POST['in_nim_4'];
		$nama_lengkap_4 = $_POST['in_nama_lengkap_4'];

		$nim_5 	= $_POST['in_nim_5'];
		$nama_lengkap_5 = $_POST['in_nama_lengkap_5'];

		$nim_6 	= $_POST['in_nim_6'];
		$nama_lengkap_6 = $_POST['in_nama_lengkap_6'];

		$nim_7 	= $_POST['in_nim_7'];
		$nama_lengkap_7 = $_POST['in_nama_lengkap_7'];

		$nim_8 	= $_POST['in_nim_8'];
		$nama_lengkap_8 = $_POST['in_nama_lengkap_8'];

		$nim_9 	= $_POST['in_nim_9'];
		$nama_lengkap_9 = $_POST['in_nama_lengkap_9'];

		$nim_10 = $_POST['in_nim_10'];
		$nama_lengkap_10 = $_POST['in_nama_lengkap_10'];

	// Nilai yang akan di input

	// Add
	if ($module==$module2 AND $act=='add'){

		try{

			$stmt = $pdo->prepare("INSERT INTO $database (nim,nama_lengkap) VALUES 
								(:nim,:nama_lengkap), 
								(:nim_2,:nama_lengkap_2),
								(:nim_3,:nama_lengkap_3),
								(:nim_4,:nama_lengkap_4),
								(:nim_5,:nama_lengkap_5),
								(:nim_6,:nama_lengkap_6),
								(:nim_7,:nama_lengkap_7),
								(:nim_8,:nama_lengkap_8),
								(:nim_9,:nama_lengkap_9),
								(:nim_10,:nama_lengkap_10)
								");
					
			$stmt->bindParam(":nim", $nim, PDO::PARAM_STR);
			$stmt->bindParam(":nama_lengkap", $nama_lengkap, PDO::PARAM_STR);
					
			$stmt->bindParam(":nim_2", $nim_2, PDO::PARAM_STR);
			$stmt->bindParam(":nama_lengkap_2", $nama_lengkap_2, PDO::PARAM_STR);
					
			$stmt->bindParam(":nim_3", $nim_3, PDO::PARAM_STR);
			$stmt->bindParam(":nama_lengkap_3", $nama_lengkap_3, PDO::PARAM_STR);
					
			$stmt->bindParam(":nim_4", $nim_4, PDO::PARAM_STR);
			$stmt->bindParam(":nama_lengkap_4", $nama_lengkap_4, PDO::PARAM_STR);
					
			$stmt->bindParam(":nim_5", $nim_5, PDO::PARAM_STR);
			$stmt->bindParam(":nama_lengkap_5", $nama_lengkap_5, PDO::PARAM_STR);
					
			$stmt->bindParam(":nim_6", $nim_6, PDO::PARAM_STR);
			$stmt->bindParam(":nama_lengkap_6", $nama_lengkap_6, PDO::PARAM_STR);
					
			$stmt->bindParam(":nim_7", $nim_7, PDO::PARAM_STR);
			$stmt->bindParam(":nama_lengkap_7", $nama_lengkap_7, PDO::PARAM_STR);
					
			$stmt->bindParam(":nim_8", $nim_8, PDO::PARAM_STR);
			$stmt->bindParam(":nama_lengkap_8", $nama_lengkap_8, PDO::PARAM_STR);
					
			$stmt->bindParam(":nim_9", $nim_9, PDO::PARAM_STR);
			$stmt->bindParam(":nama_lengkap_9", $nama_lengkap_9, PDO::PARAM_STR);
					
			$stmt->bindParam(":nim_10", $nim_10, PDO::PARAM_STR);
			$stmt->bindParam(":nama_lengkap_10", $nama_lengkap_10, PDO::PARAM_STR);
				
			$count = $stmt->execute();
				
			echo "<script>alert('$hal berhasil di tambah'); window.location = '../../$link';</script>";
					
		}catch(PDOException $e){
			echo "<script>window.alert('$hal Gagal di tambah!'); window.location(history.back(-1))</script>";
		}

	}
	
	// Edit
	elseif ($module==$module2 AND $act=='edit'){

		$nim_lama	= $_POST['in_nim_lama'];

		try {
			$sql = "UPDATE $database
					SET nim 			= :nim,
						nama_lengkap 	= :nama_lengkap
					WHERE nim 		= :nim_lama
				";
						  
			$statement = $pdo->prepare($sql);

			$statement->bindParam(":nim_lama", $nim_lama, PDO::PARAM_INT);
			$statement->bindParam(":nim", $nim, PDO::PARAM_INT);
			$statement->bindParam(":nama_lengkap", $nama_lengkap, PDO::PARAM_INT);

			$count = $statement->execute();

			echo "<script>alert('$hal berhasil di edit'); window.location = '../../media.php?module=$module&act=edit&id=$_POST[in_nim]'</script>";
					
		}catch(PDOException $e){
			echo "<script>window.alert('$hal Gagal di edit!'); window.location(history.back(-1))</script>";
		}

	}
	  	
	// Delete
	elseif ($module==$module2 AND $act=='delete'){
		
		$del = $pdo->query("DELETE FROM $database WHERE nim='$_GET[id]'");
		$del->execute();
		
		echo "<script>alert('$hal berhasil di hapus'); window.location = '../../$link'</script>";
	}
	
	
}
?>
<center style="margin-top: 250px;"><img src="../../load.gif"></center>