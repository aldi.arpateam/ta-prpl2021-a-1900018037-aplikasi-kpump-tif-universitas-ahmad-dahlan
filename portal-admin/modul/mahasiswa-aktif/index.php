<?php
if (empty($_SESSION['namaadmin']) AND empty($_SESSION['leveladmin'])){
	echo "<link href='style.css' rel='stylesheet' type='text/css'>
	<center>Untuk mengakses modul, Anda harus login <br>";
	echo "<a href=../../index.php><b>LOGIN</b></a></center>";
}else{
	
	$aksi 				= "modul/mahasiswa-aktif/aksi.php";
	$hal 				= "Mahasiswa Aktif";
	$module 			= "mahasiswa-aktif";
	$database 			= "data_mahasiswa_aktif";
	$link 				= "mahasiswa-aktif";

	switch($_GET['act']){
		case "list":
	?>
		
		<section class="content">

			<div class="row">

				<div class="col-md-12">

					<div class="box">
						<div class="box-header">
							<h1 style="text-transform: capitalize;"><?= $hal; ?></h1>
							<ol class="breadcrumb">
								<li><a href="media.php?module=home"><i class="fa fa-dashboard"></i> Home</a></li>
								<li class="active"><?= $hal; ?></li>
							</ol>
						</div><!-- /.box-header -->

						<?php
							$totalPartisipasi = $pdo->query("SELECT nim FROM data_mahasiswa_aktif");
							$tampiltotalPartisipasi = $totalPartisipasi->rowCount();

							$PartisipasiAngkatan20 = $pdo->query("SELECT nim FROM data_mahasiswa_aktif WHERE nim LIKE '20%' ORDER BY nim ASC ");
							$tampilPartisipasiAngkatan20 = $PartisipasiAngkatan20->rowCount();

							$PartisipasiAngkatan19 = $pdo->query("SELECT nim FROM data_mahasiswa_aktif WHERE nim LIKE '19%' ORDER BY nim ASC ");
							$tampilPartisipasiAngkatan19 = $PartisipasiAngkatan19->rowCount();

							$PartisipasiAngkatan18 = $pdo->query("SELECT nim FROM data_mahasiswa_aktif WHERE nim LIKE '18%' ORDER BY nim ASC ");
							$tampilPartisipasiAngkatan18 = $PartisipasiAngkatan18->rowCount();

							$PartisipasiAngkatan17 = $pdo->query("SELECT nim FROM data_mahasiswa_aktif WHERE nim LIKE '17%' ORDER BY nim ASC ");
							$tampilPartisipasiAngkatan17 = $PartisipasiAngkatan17->rowCount();

							$PartisipasiAngkatan16 = $pdo->query("SELECT nim FROM data_mahasiswa_aktif WHERE nim LIKE '16%' ORDER BY nim ASC ");
							$tampilPartisipasiAngkatan16 = $PartisipasiAngkatan16->rowCount();

							$PartisipasiAngkatan15 = $pdo->query("SELECT nim FROM data_mahasiswa_aktif WHERE nim LIKE '15%' ORDER BY nim ASC ");
							$tampilPartisipasiAngkatan15 = $PartisipasiAngkatan15->rowCount();

							$PartisipasiAngkatan14 = $pdo->query("SELECT nim FROM data_mahasiswa_aktif WHERE nim LIKE '14%' ORDER BY nim ASC ");
							$tampilPartisipasiAngkatan14 = $PartisipasiAngkatan14->rowCount();

							$PartisipasiAngkatan13 = $pdo->query("SELECT nim FROM data_mahasiswa_aktif WHERE nim LIKE '13%' ORDER BY nim ASC ");
							$tampilPartisipasiAngkatan13 = $PartisipasiAngkatan13->rowCount();

						?>

						<div class="box-body">
							<div class="alert alert-warning alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				            	<h4><i class="icon fa fa-warning"></i> TERDAPAT <span class="label bg-green"><?= rp($tampiltotalPartisipasi); ?></span> Mahasiswa <u>Prodi Teknik Informatika</u> yang aktif, Terdiri dari:</h4>
				                <p><span class="label bg-green"><?= $tampilPartisipasiAngkatan20; ?></span> Dari Angkatan 2020</p>
				                <p><span class="label bg-green"><?= $tampilPartisipasiAngkatan19; ?></span> Dari Angkatan 2019</p>
				                <p><span class="label bg-green"><?= $tampilPartisipasiAngkatan18; ?></span> Dari Angkatan 2018</p>
				                <p><span class="label bg-green"><?= $tampilPartisipasiAngkatan17; ?></span> Dari Angkatan 2017</p>
				                <p><span class="label bg-green"><?= $tampilPartisipasiAngkatan16; ?></span> Dari Angkatan 2016</p>
				                <p><span class="label bg-green"><?= $tampilPartisipasiAngkatan15; ?></span> Dari Angkatan 2015</p>
				                <p><span class="label bg-green"><?= $tampilPartisipasiAngkatan14; ?></span> Dari Angkatan 2014</p>
				                <p><span class="label bg-green"><?= $tampilPartisipasiAngkatan13; ?></span> Dari Angkatan 2013</p>
				            </div>

				            <a target="_blank" href="https://kpump-tif.arpateam.com/aldiyuni150315/modul/mahasiswa-aktif/export-to-pdf.php" role="button" class="btn btn-block btn-danger">Export to PDF <i class="fa fa-file-pdf-o"></i></a>

				        </div>

						<div class="box-body">
							<a href="<?= $link; ?>-add" class="btn btn-lg btn-block btn-success"><i class="fa fa-plus"></i> Tambah <?= $hal; ?></a>
						</div>
				
						<div class="box-body table-responsive" style="overflow-x: auto;">
							<table id="example1" class="table table-bordered table-striped">

								<thead>
						  			<tr>
										<th width="40%" class="center">NIM</th>
										<th width="40%" class="center">Nama Lengkap</th>
										<th width="20%"  class="center">Aksi</th>
						  			</tr>
								</thead>

								<tbody>

									<!-- Query menampilkan rekap Mahasiswa Aktif -->
									<?php
										$tampil = $pdo->query("SELECT * FROM $database");
										while($r = $tampil->fetch(PDO::FETCH_ASSOC)){
									?>

									<tr style="text-align: center;">
										<td><?= $r['nim']; ?></td>
										<td><?= $r['nama_lengkap']; ?></td>
										
										<td>
											<a href="<?= $link; ?>-edit-<?= $r['nim']; ?>" class="btn btn-primary"><i class="fa fa-edit"></i></a>
											
											<a onclick="return confirm('Apakah anda ingin menghapus <?= $hal; ?> dengan NIM <?= $r['nim']; ?>?')" href="modul/<?= $link; ?>/aksi.php?module=<?= $module; ?>&act=delete&id=<?= $r['nim']; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
										</td>
								
									</tr>

									<?php
										$no++;
										}
									?>

								</tbody>

					 		</table>
						</div><!-- /.box-body -->
				  	</div><!-- /.box -->
			   
				</div><!-- /.col -->

			</div>
		
		</section><!-- /.col -->
			
	<?php
		break;
		case "add":

		$action	= "add";
	?>

		<section class="content">
			<div class="row">
			  
				<!-- left column -->
				<div class="col-md-12">

				  	<!-- general form elements -->
				  	<div class="box box-primary">
						<div class="box-header">
							<h1 style="text-transform: capitalize;"><?= $hal; ?></h1>
							<ol class="breadcrumb">
								<li><a href="media.php?module=home"><i class="fa fa-dashboard"></i> Home</a></li>
								<li><a href="<?= $link; ?>"><?= $hal; ?></a></li>
								<li class="active">Tambah <?= $hal; ?></li>
							</ol>
						</div><!-- /.box-header -->

						<div class="box-body">
							<div class="alert alert-warning alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				            	<h4><i class="icon fa fa-warning"></i> Perhatian!</h4>
				                Mohon isi data dengan benar & lengkap
				            </div>
				        </div>

						<!-- form start -->
						<form role="form" action="modul/<?= $link; ?>/aksi.php?module=<?= $module; ?>&act=<?= $action; ?>" method="POST">
						
							<div class="box-body">
						
								<div class="col-md-6">
									<div class="form-group">
										<label for="nim">NIM <?= $hal; ?></label>
										<input id="nim" name="in_nim" type="text" class="form-control" placeholder="Cth: 1900018037" min="10" required>
									</div>
								</div>
						
								<div class="col-md-6">
									<div class="form-group">
										<label for="nama_lengkap">Nama Lengkap <?= $hal; ?></label>
										<input id="nama_lengkap" name="in_nama_lengkap" type="text" class="form-control" placeholder="Cth: Aldi Febriyanto" required>
									</div>
								</div>
						
								<div class="col-md-6">
									<div class="form-group">
										<label for="nim_2">NIM <?= $hal; ?></label>
										<input id="nim_2" name="in_nim_2" type="text" class="form-control" placeholder="Cth: 1900018037" min="10" required>
									</div>
								</div>
						
								<div class="col-md-6">
									<div class="form-group">
										<label for="nama_lengkap_2">Nama Lengkap <?= $hal; ?></label>
										<input id="nama_lengkap_2" name="in_nama_lengkap_2" type="text" class="form-control" placeholder="Cth: Aldi Febriyanto" required>
									</div>
								</div>
						
								<div class="col-md-6">
									<div class="form-group">
										<label for="nim_3">NIM <?= $hal; ?></label>
										<input id="nim_3" name="in_nim_3" type="text" class="form-control" placeholder="Cth: 1900018037" min="10" required>
									</div>
								</div>
						
								<div class="col-md-6">
									<div class="form-group">
										<label for="nama_lengkap_3">Nama Lengkap <?= $hal; ?></label>
										<input id="nama_lengkap_3" name="in_nama_lengkap_3" type="text" class="form-control" placeholder="Cth: Aldi Febriyanto" required>
									</div>
								</div>
						
								<div class="col-md-6">
									<div class="form-group">
										<label for="nim_4">NIM <?= $hal; ?></label>
										<input id="nim_4" name="in_nim_4" type="text" class="form-control" placeholder="Cth: 1900018037" min="10" required>
									</div>
								</div>
						
								<div class="col-md-6">
									<div class="form-group">
										<label for="nama_lengkap_4">Nama Lengkap <?= $hal; ?></label>
										<input id="nama_lengkap_4" name="in_nama_lengkap_4" type="text" class="form-control" placeholder="Cth: Aldi Febriyanto" required>
									</div>
								</div>
						
								<div class="col-md-6">
									<div class="form-group">
										<label for="nim_5">NIM <?= $hal; ?></label>
										<input id="nim_5" name="in_nim_5" type="text" class="form-control" placeholder="Cth: 1900018037" min="10" required>
									</div>
								</div>
						
								<div class="col-md-6">
									<div class="form-group">
										<label for="nama_lengkap_5">Nama Lengkap <?= $hal; ?></label>
										<input id="nama_lengkap_5" name="in_nama_lengkap_5" type="text" class="form-control" placeholder="Cth: Aldi Febriyanto" required>
									</div>
								</div>
						
								<div class="col-md-6">
									<div class="form-group">
										<label for="nim_6">NIM <?= $hal; ?></label>
										<input id="nim_6" name="in_nim_6" type="text" class="form-control" placeholder="Cth: 1900018037" min="10" required>
									</div>
								</div>
						
								<div class="col-md-6">
									<div class="form-group">
										<label for="nama_lengkap_6">Nama Lengkap <?= $hal; ?></label>
										<input id="nama_lengkap_6" name="in_nama_lengkap_6" type="text" class="form-control" placeholder="Cth: Aldi Febriyanto" required>
									</div>
								</div>
						
								<div class="col-md-6">
									<div class="form-group">
										<label for="nim_7">NIM <?= $hal; ?></label>
										<input id="nim_7" name="in_nim_7" type="text" class="form-control" placeholder="Cth: 1900018037" min="10" required>
									</div>
								</div>
						
								<div class="col-md-6">
									<div class="form-group">
										<label for="nama_lengkap_7">Nama Lengkap <?= $hal; ?></label>
										<input id="nama_lengkap_7" name="in_nama_lengkap_7" type="text" class="form-control" placeholder="Cth: Aldi Febriyanto" required>
									</div>
								</div>
						
								<div class="col-md-6">
									<div class="form-group">
										<label for="nim_8">NIM <?= $hal; ?></label>
										<input id="nim_8" name="in_nim_8" type="text" class="form-control" placeholder="Cth: 1900018037" min="10" required>
									</div>
								</div>
						
								<div class="col-md-6">
									<div class="form-group">
										<label for="nama_lengkap_8">Nama Lengkap <?= $hal; ?></label>
										<input id="nama_lengkap_8" name="in_nama_lengkap_8" type="text" class="form-control" placeholder="Cth: Aldi Febriyanto" required>
									</div>
								</div>
						
								<div class="col-md-6">
									<div class="form-group">
										<label for="nim_9">NIM <?= $hal; ?></label>
										<input id="nim_9" name="in_nim_9" type="text" class="form-control" placeholder="Cth: 1900018037" min="10" required>
									</div>
								</div>
						
								<div class="col-md-6">
									<div class="form-group">
										<label for="nama_lengkap_9">Nama Lengkap <?= $hal; ?></label>
										<input id="nama_lengkap_9" name="in_nama_lengkap_9" type="text" class="form-control" placeholder="Cth: Aldi Febriyanto" required>
									</div>
								</div>
						
								<div class="col-md-6">
									<div class="form-group">
										<label for="nim_10">NIM <?= $hal; ?></label>
										<input id="nim_10" name="in_nim_10" type="text" class="form-control" placeholder="Cth: 1900018037" min="10" required>
									</div>
								</div>
						
								<div class="col-md-6">
									<div class="form-group">
										<label for="nama_lengkap_10">Nama Lengkap <?= $hal; ?></label>
										<input id="nama_lengkap_10" name="in_nama_lengkap_10" type="text" class="form-control" placeholder="Cth: Aldi Febriyanto" required>
									</div>
								</div>
								
							</div><!-- /.box-body -->

							<div class="box-footer">
								<button type="submit" class="btn btn-success">Selesai <i class="fa fa-check-square-o"></i></button>
								<a href="<?= $module; ?>"  class="btn btn-warning">Batal <i class="fa fa-times"></i></a>
							</div>
						</form>
				  	</div><!-- /.box -->

				</div>

			</div>
		</section>
		
	<?php
		break;
		case "edit":
		$action	= "edit";

		$edit = $pdo->query("SELECT * FROM $database WHERE nim='$_GET[id]'");
		$tedit = $edit->fetch(PDO::FETCH_ASSOC);
		
	?>

		<section class="content">
			<div class="row">
			  
				<!-- left column -->
				<div class="col-md-12">

				  	<!-- general form elements -->
				  	<div class="box box-primary">
						<div class="box-header">
							<h1 style="text-transform: capitalize;"><?= $hal; ?></h1>
							<ol class="breadcrumb">
								<li><a href="media.php?module=home"><i class="fa fa-dashboard"></i> Home</a></li>
								<li><a href="<?= $link; ?>"><?= $hal; ?></a></li>
								<li class="active">Edit <?= $tedit['nim']; ?></li>
							</ol>
						</div><!-- /.box-header -->

						<div class="box-body">
							<div class="alert alert-warning alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				            	<h4><i class="icon fa fa-warning"></i> Perhatian!</h4>
				                Mohon isi data dengan benar & lengkap
				            </div>
				        </div>

						<!-- form start -->
						<form role="form" action="modul/<?= $link; ?>/aksi.php?module=<?= $module; ?>&act=<?= $action; ?>" method="POST">
						
							<div class="box-body">

								<input type="text" name="in_nim_lama" value="<?= $_GET['id']; ?>">
						
								<div class="col-md-6">
									<div class="form-group">
										<label for="nim">NIM <?= $hal; ?></label>
										<input id="nim" name="in_nim" type="text" class="form-control" value="<?= $tedit['nim']; ?>" min="10" required>
									</div>
								</div>
						
								<div class="col-md-6">
									<div class="form-group">
										<label for="nama_lengkap">Nama Lengkap <?= $hal; ?></label>
										<input id="nama_lengkap" name="in_nama_lengkap" type="text" class="form-control" value="<?= $tedit['nama_lengkap']; ?>" required>
									</div>
								</div>

							</div>

							<div class="box-footer">
								<button type="submit" class="btn btn-success">Selesai <i class="fa fa-check-square-o"></i></button>
								<a href="<?= $module; ?>"  class="btn btn-warning">Batal <i class="fa fa-times"></i></a>
							</div>
						</form>
				  	</div><!-- /.box -->

				</div>

			</div>
		</section>
			
			
			
	<?php
		break;  
	}
}
?>
