<?php
if (empty($_SESSION['namaadmin']) AND empty($_SESSION['leveladmin'])){
	echo "<script>window.alert('Untuk mengakses halaman ini ada harus Login!'); window.location = 'login';</script>";
}else{

	$hal 				= "Rekap Hasil Vote";
	$database 			= "vote";

	if (isset($_POST['submit'])) {
		$del = $pdo->query("DELETE FROM $database");
		$del->execute();
		echo "<script>alert('Data Login Portal Admin berhasil di reset');</script>";
	}
	
?>

	<section class="content">

			<div class="row">

				<div class="col-md-12">

				    <a target="_blank" href="https://kpump-tif.arpateam.com/aldiyuni150315/modul/rekap-hasil-vote/export-to-pdf.php" role="button" class="btn btn-block btn-danger">Export to PDF <i class="fa fa-file-pdf-o"></i></a>

				    <br />

					<div class="box box-primary">
						<div class="box-header">
							<h1 style="text-transform: capitalize;"><?= $hal; ?></h1>
							<ol class="breadcrumb">
								<li><a href="media.php?module=home"><i class="fa fa-dashboard"></i> Dasboard</a></li>
								<li class="active"><?= $hal; ?></li>
							</ol>
						</div><!-- /.box-header -->

						<?php
							$suratSuaraMasuk = $pdo->query("SELECT nim FROM $database WHERE hasil_vote!=NULL OR hasil_vote!='0'");
							$tampilsuratSuaraMasuk = $suratSuaraMasuk->rowCount();
						?>

						<div class="box-body">
							<div class="alert alert-warning alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				            	<h4><i class="icon fa fa-warning"></i> Terdapat <span class="label bg-green"><?= $tampilsuratSuaraMasuk; ?></span> Surat Suara Yang Masuk!</h4>
				            </div>
				        </div>
				
						<div class="box-body table-responsive" style="overflow-x: auto;">
							<table id="example1" class="table table-bordered table-striped">

								<thead>
						  			<tr>
										<th width="20%" class="center">Surat Suara ke-</th>
										<th width="25%" class="center">PASLON 01</th>
										<th width="25%" class="center">PASLON 02</th>
										<th width="25%" class="center">GOLPUT</th>
						  			</tr>
								</thead>

								<tbody>

									<?php
										$no = 1;
										$tampil = $pdo->query("SELECT $database.hasil_vote FROM $database WHERE hasil_vote!=NULL OR hasil_vote!='0' ORDER BY waktu ASC");

										while($r = $tampil->fetch(PDO::FETCH_ASSOC)){
									?>

									<tr style="text-align: center;">
										<td><?= $no++; ?></td>
										<td><?php if($r['hasil_vote']==1) { echo "<i class='fa fa-check'></i>"; }; ?></td>
							      		<td><?php if($r['hasil_vote']==2) { echo "<i class='fa fa-check'></i>"; }; ?></td>
							      		<td><?php if($r['hasil_vote']==3) { echo "<i class='fa fa-check'></i>"; }; ?></td>
									</tr>

									<?php
										}
									?>

								</tbody>

					 		</table>
						</div><!-- /.box-body -->

				  	</div><!-- /.box -->

					<div class="box box-primary">
			            <div class="box-header">
			              	<h1>Rincian Hasil Vote</h1>
			            </div>
			            <!-- /.box-header -->

			            <div class="box-body no-padding">
			              	<table class="table table-striped">

			                	<tr>
				                  	<th style="width: 50%">Data</th>
				                  	<th style="width: 25%">Total Suara</th>
				                  	<th style="width: 25%">Presentase</th>
			                	</tr>

			                	<!-- Query menampilkan rekap hasil vote -->

			                	<?php 

			                		$rincianVotePaslon1 = $pdo->query("SELECT $database.nim FROM $database WHERE hasil_vote='1'");
									$rrincianVotePaslon1 = $rincianVotePaslon1->rowCount();

			                		$rincianVotePaslon2 = $pdo->query("SELECT $database.nim FROM $database WHERE hasil_vote='2'");
									$rrincianVotePaslon2 = $rincianVotePaslon2->rowCount();

			                		$rincianVotePaslonGolput = $pdo->query("SELECT $database.nim FROM $database WHERE hasil_vote='3'");
									$rrincianVotePaslonGolput = $rincianVotePaslonGolput->rowCount();

									$presentaseWASUP	= (100 * ($rrincianVotePaslon1/$tampilsuratSuaraMasuk));
									$presentaseWIFI		= (100 * ($rrincianVotePaslon2/$tampilsuratSuaraMasuk));
									$presentaseGOLPUT	= (100 * ($rrincianVotePaslonGolput/$tampilsuratSuaraMasuk));

									if ($presentaseWASUP>$presentaseWIFI AND $presentaseWASUP>$presentaseGOLPUT) {
										$pemenangnya 	= "WIDA GHAIB NUGROHO - SUPRAYOGI BUDI .P";
										$rincianVotePemenang	= $rrincianVotePaslon1;
										$presentasKemenangan	= number_format($presentaseWASUP,2);
									}elseif ($presentaseWIFI>$presentaseWASUP AND $presentaseWIFI>$presentaseGOLPUT) {
										$pemenangnya 	= "WISNU ANDRIAN - AMIRATUR RAFIFAH";
										$rincianVotePemenang	= $rrincianVotePaslon2;
										$presentasKemenangan	= number_format($presentaseWIFI,2);
									}elseif ($presentaseGOLPUT>$presentaseWASUP AND $presentaseGOLPUT>$presentaseWIFI) {
										$pemenangnya 	= "SEMUA PADA MILIH GOLPUT";
										$rincianVotePemenang	= $rrincianVotePaslonGolput;
										$presentasKemenangan	= number_format($presentaseGOLPUT,2);
									}elseif ($presentaseWIFI==$presentaseWASUP) {
										$pemenangnya 	= "SERI";
										$rincianVotePemenang	= $rrincianVotePaslon1;
										$presentasKemenangan	= "SERI";
									}

			                	?>

			                	<tr>
			                  		<td>
			                  			<h5>WIDA GHAIB NUGROHO - SUPRAYOGI BUDI .P</h5>
			                  		</td>
			                  		<td>
			                    		<h5><?= $rrincianVotePaslon1; ?></h5>
			                  		</td>
			                  		<td><span class="badge bg-red"><?= number_format($presentaseWASUP,2); ?>%</span></td>
			                  	</tr>

			                  	<tr>
			                  		<td>
			                  			<h5>WISNU ANDRIAN - AMIRATUR RAFIFAH</h5>
			                  		</td>
			                  		<td>
			                    		<h5><?= $rrincianVotePaslon2; ?></h5>
			                  		</td>
			                  		<td><span class="badge bg-red"><?= number_format($presentaseWIFI,2); ?>%</span></td>
			                  	</tr>

			                  	<tr>
			                  		<td>
			                  			<h5>GOLPUT</h5>
			                  		</td>
			                  		<td>
			                    		<h5><?= $rrincianVotePaslonGolput; ?></h5>
			                  		</td>
			                  		<td><span class="badge bg-red"><?= number_format($presentaseGOLPUT,2); ?>%</span></td>
			                	</tr>

			              	</table>
			            </div>



						<div class="box-body">
							<div class="alert alert-warning alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				            	<h4><i class="icon fa fa-warning"></i> Pemenangnya adalah <span class="label bg-green"><?= $pemenangnya; ?></span></h4>
				                <p>Mendapatkan total <span class="label bg-green"><?= $rincianVotePemenang; ?></span> Suara</p>
				                <p>Dengan Presentase kemenangan <span class="label bg-red"><?= $presentasKemenangan; ?>%</span></p>
				            </div>
				        </div>
			            <!-- /.box-body -->
			        </div>
			   
				</div><!-- /.col -->

			</div>
		
		</section><!-- /.col -->
		
<?php
	}
?>