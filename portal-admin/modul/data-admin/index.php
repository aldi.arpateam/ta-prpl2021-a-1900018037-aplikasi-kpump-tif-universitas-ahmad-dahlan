<?php
if (empty($_SESSION['namaadmin']) AND empty($_SESSION['leveladmin'])){
	echo "<link href='style.css' rel='stylesheet' type='text/css'>
	<center>Untuk mengakses halaman ini, Anda harus login <br>";
	echo "<a href=../../index.php><b>LOGIN</b></a></center>";
}else{
	
	$aksi 				= "modul/data-admin/aksi.php";

	$hal 				= "Data Admin";
	$link 				= "data-admin";

	$penyimpananGambar	= "images/data-admin";
	$module 			= "data-admin";
	$database 			= "data_admin";

	switch($_GET['act']){
		case "list":
	?>
		
		<section class="content">

			<div class="row">

				<div class="col-md-12">

					<div class="box">
						<div class="box-header">
							<h1 style="text-transform: capitalize;"><?= $hal; ?></h1>
							<ol class="breadcrumb">
								<li><a href="media.php?module=home"><i class="fa fa-dashboard"></i> Home</a></li>
								<li class="active"><?= $hal; ?></li>
							</ol>
						</div><!-- /.box-header -->

						<div class="box-body">
							<a href="<?= $link; ?>-add" class="btn btn-block btn-success"><i class="fa fa-plus"></i> Tambah <?= $hal; ?></a>
						</div>
				
						<div class="box-body table-responsive" style="overflow-x: auto;">
							<table id="example1" class="table table-bordered table-striped">

								<thead>
						  			<tr>
										<th width="2%" class="center">No</th>
										<th width="10%" class="center">Username</th>
										<th width="10%" class="center">Password</th>
										<th width="13%" class="center">Nama</th>
										<th width="18%" class="center">Jenis Kelamin</th>
										<th width="15%" class="center">Nomor WA</th>
										<th width="5%" class="center">Level</th>
										<th width="5%" class="center">Status</th>
										<th width="10%" class="center">Gambar</th>
										<th width="12%"  class="center">Aksi</th>
						  			</tr>
								</thead>

								<tbody>

									<?php
										$no = 1;
										$tampil = $pdo->query("SELECT * FROM $database WHERE level!='Administrator' ORDER BY id_$database DESC");

										while($r = $tampil->fetch(PDO::FETCH_ASSOC)){
											
											if($r["gambar"]==""){$gbr = "../images/no-images.jpg";}else{$gbr = "$penyimpananGambar/$r[gambar]";}
									?>

									<tr style="text-align: center;">
										<td><?=  $no; ?></td>
										<td><?= $r['username']; ?></td>
										<td><span class="badge btn-warning"><?= $r['password']; ?></span></td>
										<td><?= $r['nama']; ?></td>
										<td><?= $r['jenis_kelamin']; ?></td>
										<td><?= $r['nomor_whatsapp']; ?></td>
										<td>
											<?php

												if ($r['level'] == 'Admin Utama') {
													echo "<span class='badge btn-success'>Admin Utama</span>";
												}elseif ($r['level'] == 'Penulis') {
													echo "<span class='badge btn-warning'>Penulis</span>";
												}elseif ($r['level'] == 'Marketing') {
													echo "<span class='badge btn-info'>Marketing</span>";
												}

											?>
										</td>
										<td>
											<?php

												if ($r['status'] == 'Aktif') {
													echo "<span class='badge btn-success'>Aktif</span>";
												}elseif ($r['status'] == 'Blokir') {
													echo "<span class='badge btn-danger'>Blokir</span>";
												}

											?>
										</td>
										<td><img src="<?= $gbr; ?>" width="80px"></td>
										
										<td>
											<a href="<?= $link; ?>-edit-<?= $r['id_data_admin']; ?>" class="btn btn-primary"><i class="fa fa-edit"></i></a>
											
											<a onclick="return confirm('Apakah anda ingin menghapus <?= $hal; ?> dengan nama <?= $r['nama']; ?>?')" href="modul/<?= $module; ?>/aksi.php?module=<?= $module; ?>&act=delete&id=<?= $r['id_data_admin']; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
										</td>
								
									</tr>

									<?php
										$no++;
										}
									?>

								</tbody>

					 		</table>
						</div><!-- /.box-body -->
				  	</div><!-- /.box -->
			   
				</div><!-- /.col -->

			</div>
		
		</section><!-- /.col -->
			
	<?php
		break;
		case "add":

		$action	= "add";
	?>

		<section class="content">
			<div class="row">
			  
				<!-- left column -->
				<div class="col-md-12">

				  	<!-- general form elements -->
				  	<div class="box box-primary">
						<div class="box-header">
							<h1 style="text-transform: capitalize;"><?= $hal; ?></h1>
							<ol class="breadcrumb">
								<li><a href="media.php?module=home"><i class="fa fa-dashboard"></i> Home</a></li>
								<li><a href="<?= $link; ?>"><?= $hal; ?></a></li>
								<li class="active">Tambah <?= $hal; ?></li>
							</ol>
						</div><!-- /.box-header -->

						<div class="box-body">
							<div class="alert alert-warning alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				            	<h4><i class="icon fa fa-warning"></i> Perhatian!</h4>
				                Mohon isi data dengan benar & lengkap
				            </div>
				        </div>

						<!-- form start -->
						<form role="form" action="modul/<?= $module; ?>/aksi.php?module=<?= $module; ?>&act=<?= $action; ?>" method="POST" enctype="multipart/form-data" >
						
							<div class="box-body">
						
								<div class="col-md-12">
									<div class="form-group">
										<label for="username">Username <?= $hal; ?></label>
										<input id="username" name="in_username" type="text" class="form-control" placeholder="Cth: AdminBaru" autocomplete="off" required>
									</div>
								</div>
						
								<div class="col-md-12">
									<div class="form-group">
										<label for="password">password <?= $hal; ?></label>
										<input id="password" name="in_password" type="password" class="form-control" autocomplete="off" required>
									</div>
								</div>
						
								<div class="col-md-12">
									<div class="form-group">
										<label for="nama">Nama Lengkap</label>
										<input id="nama" name="in_nama" type="text" class="form-control" placeholder="Cth: AdminBaru" required>
									</div>
								</div>

								<div class="col-md-12">
									<div class="form-group">
										<label for="jenis_kelamin">Jenis Kelamin</label>
										<select id="jenis_kelamin" name="in_jenis_kelamin" class="form-control">

											<option value="0">- Pilih Jenis Kelamin -</option>

											<option value="Laki-Laki">Laki-Laki</option>
											<option value="Perempuan">Perempuan</option>

										</select>
									</div>
								</div>
						
								<div class="col-md-12">
									<div class="form-group">
										<label for="nomor_whatsapp">Nomor WhatsApp</label>
										<input id="nomor_whatsapp" name="in_nomor_whatsapp" type="text" class="form-control" placeholder="Cth: 0816 9070 19">
									</div>
								</div>

								<div class="col-md-12">
									<div class="form-group">
										<label for="level">Level Admin</label>
										<select id="level" name="in_level" class="form-control">

											<option value="0">- Pilih Level Admin -</option>

											<option value="Admin Utama">Admin Utama</option>
											<option value="Penulis">Penulis</option>
											<option value="Marketing">Marketing</option>

										</select>
									</div>
								</div>

								<div class="col-md-12">
									<div class="form-group">
										<label for="status">Status</label>
										<select id="status" name="in_status" class="form-control">

											<option value="0">- Pilih Status -</option>

											<option value="Aktif">Aktif</option>
											<option value="Blokir">Blokir</option>

										</select>
									</div>
								</div>
								
								<div class="col-md-12">
									<div class="form-group">

										<label for="gambar">Gambar</label>

										<div class="photo">
											<input id="gambar" name="in_gambar" type="file">
										</div>

										<br />

							            <div class="alert alert-warning alert-dismissible">
							            	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							                <h4><i class="icon fa fa-warning"></i> Perhatian!</h4>
											<p>*) Maksimal Ukuran Gambar 400 KB</p>
							            </div>
									
									</div><!-- /.box-body -->
								</div>
								
							</div><!-- /.box-body -->

							<div class="box-footer">
								<button type="submit" class="btn btn-success">Selesai <i class="fa fa-check-square-o"></i></button>
								<a href="<?= $link; ?>"  class="btn btn-warning">Batal <i class="fa fa-times"></i></a>
							</div>
						</form>
				  	</div><!-- /.box -->

				</div>

			</div>
		</section>
		
	<?php
		break;
		case "edit":
		$action	= "edit";

		$edit = $pdo->query("SELECT * FROM $database WHERE id_$database='$_GET[id]'");
		$tedit = $edit->fetch(PDO::FETCH_ASSOC);
		
	?>

		<section class="content">
			<div class="row">
			  
				<!-- left column -->
				<div class="col-md-12">

				  	<!-- general form elements -->
				  	<div class="box box-primary">
						<div class="box-header">
							<h1 style="text-transform: capitalize;"><?= $hal; ?></h1>
							<ol class="breadcrumb">
								<li><a href="media.php?module=home"><i class="fa fa-dashboard"></i> Home</a></li>
								<li><a href="<?= $link; ?>"><?= $hal; ?></a></li>
								<li class="active">Edit <?= $tedit['nama']; ?></li>
							</ol>
						</div><!-- /.box-header -->

						<div class="box-body">
							<div class="alert alert-warning alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				            	<h4><i class="icon fa fa-warning"></i> Perhatian!</h4>
				                Mohon isi data dengan benar & lengkap
				            </div>
				        </div>

						<!-- form start -->
						<form role="form" action="modul/<?= $module; ?>/aksi.php?module=<?= $module; ?>&act=<?= $action; ?>" method="POST" enctype="multipart/form-data" >

							<input name="in_id_data_admin" type="hidden" class="form-control" value="<?= $tedit['id_data_admin']; ?>">
						
							<div class="box-body">
						
								<div class="col-md-12">
									<div class="form-group">
										<label for="username">Username <?= $hal; ?></label>
										<input id="username" name="in_username" type="text" class="form-control" value="<?= $tedit['username']; ?>" autocomplete="off" required>
									</div>
								</div>
						
								<div class="col-md-12">
									<div class="form-group">
										<label for="password">password <?= $hal; ?></label>
										<input id="password" name="in_password" type="password" class="form-control" autocomplete="off" value="<?= $tedit['password']; ?>" required>
									</div>
								</div>
						
								<div class="col-md-12">
									<div class="form-group">
										<label for="nama">Nama Lengkap</label>
										<input id="nama" name="in_nama" type="text" class="form-control" value="<?= $tedit['nama']; ?>" required>
									</div>
								</div>

								<div class="col-md-12">
									<div class="form-group">
										<label for="jenis_kelamin">Jenis Kelamin</label>
										<select id="jenis_kelamin" name="in_jenis_kelamin" class="form-control">

											<option value="Laki-Laki" <?php if ($tedit['jenis_kelamin'] == "Laki-Laki") { echo "selected"; } ?>>Laki-Laki</option>
											<option value="Perempuan" <?php if ($tedit['jenis_kelamin'] == "Perempuan") { echo "selected"; } ?>>Perempuan</option>

										</select>
									</div>
								</div>
						
								<div class="col-md-12">
									<div class="form-group">
										<label for="nomor_whatsapp">Nomor WhatsApp</label>
										<input id="nomor_whatsapp" name="in_nomor_whatsapp" type="text" class="form-control" value="<?= $tedit['nomor_whatsapp']; ?>">
									</div>
								</div>

								<div class="col-md-12">
									<div class="form-group">
										<label for="level">Level Admin</label>
										<select id="level" name="in_level" class="form-control">

											<option value="Admin Utama" <?php if ($tedit['level'] == "Admin Utama") { echo "selected"; } ?>>Admin Utama</option>
											<option value="Penulis" <?php if ($tedit['level'] == "Penulis") { echo "selected"; } ?>>Penulis</option>
											<option value="Marketing" <?php if ($tedit['level'] == "Marketing") { echo "selected"; } ?>>Marketing</option>

										</select>
									</div>
								</div>

								<div class="col-md-12">
									<div class="form-group">
										<label for="status">Status</label>
										<select id="status" name="in_status" class="form-control">

											<option value="Aktif" <?php if ($tedit['status'] == "Aktif") { echo "selected"; } ?>>Aktif</option>
											<option value="Blokir" <?php if ($tedit['status'] == "Blokir") { echo "selected"; } ?>>Blokir</option>

										</select>
									</div>
								</div>
								
								<div class="col-md-12">
									<div class="form-group">

										<label for="gambar">Gambar</label>
										<br /><br />

										<div class="photo">
											<img src="<?= $penyimpananGambar ?>/<?= $tedit['gambar']; ?>" alt="Gambar <?= $tedit['nama']; ?>" width="250">
										</div>
										<br />
										
										<input id="gambar" name="in_gambar" type="file" value="xxx">

										<br />

							            <div class="alert alert-warning alert-dismissible">
							            	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							                <h4><i class="icon fa fa-warning"></i> Alert!</h4>
											<p>*) Maksimal Ukuran Gambar 400 KB</p>
							            </div>
									
									</div><!-- /.box-body -->
								</div>
								
							</div><!-- /.box-body -->

							<div class="box-footer">
								<button type="submit" class="btn btn-success">Selesai <i class="fa fa-check-square-o"></i></button>
								<a href="<?= $link; ?>"  class="btn btn-warning">Batal <i class="fa fa-times"></i></a>
							</div>
						</form>
				  	</div><!-- /.box -->

				</div>

			</div>
		</section>
			
			
			
	<?php
		break;  
	}
}
?>
