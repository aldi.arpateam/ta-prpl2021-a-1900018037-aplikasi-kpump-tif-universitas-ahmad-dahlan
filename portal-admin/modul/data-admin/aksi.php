<?php
session_start();
error_reporting(0);

if (empty($_SESSION['namaadmin']) AND empty($_SESSION['leveladmin'])){
	echo "<link href='style.css' rel='stylesheet' type='text/css'>
	<center>Untuk mengakses halaman ini, Anda harus login <br>";
	echo "<a href=../../index.php><b>LOGIN</b></a></center>";

}else{
	require "../../../system/koneksi.php";
	require "../../../system/fungsi_upload_gambar.php";
	require "../../../system/fungsi_form.php";
	require "../../../system/fungsi_seo.php";
	require "../../../system/fungsi_seo2.php";

	// Data file
	$hal 				= "Data Admin";
	$link 				= "data-admin";

	$penyimpananGambar	= "../../images/data-admin";

	$module 			= $_GET["module"];
	$module2 			= "data-admin";
	$database			= "data_admin";

	$act 				= $_GET["act"];
	// Data file

	// Nilai yang akan di input

		$id_data_admin 	= $_POST['in_id_data_admin'];
		$username 		= $_POST['in_username'];
		$password 		= $_POST['in_password'];

		$nama 			= $_POST['in_nama'];
		$jenis_kelamin 	= $_POST['in_jenis_kelamin'];
		$nomor_whatsapp = $_POST['in_nomor_whatsapp'];
		$level 			= $_POST['in_level'];
		$status 		= $_POST['in_status'];

		$lokasi_file 	= $_FILES['in_gambar']['tmp_name'];
		$lokasi_upload	= "$penyimpananGambar/";
		$nama_file   	= $_FILES['in_gambar']['name'];
		$tipe_file   	= strtolower($_FILES['in_gambar']['type']);
		$tipe_file2   	= seo2($tipe_file); // ngedapetin png / jpg / jpeg
		$ukuran   		= $_FILES['in_gambar']['size'];
		$nama_file_unik	= seo($nama)."-".seo($level)."-arpa-team".".".$tipe_file2;

	// Nilai yang akan di input

	// Add Website
	if ($module==$module2 AND $act=='add'){

		// cek username & password

		$cekUsername = $pdo->query("SELECT * FROM $database WHERE username='$username'");
		$resultcekUsername = $cekUsername->fetch(PDO::FETCH_ASSOC);

		if ($resultcekUsername['username'] != NULL) {
			echo "<script>window.alert('Username anda sudah terdaftar, mohon gunakan username yang lain!'); window.location(history.back(-1))</script>";
			exit();
		}

		$cekPassword = $pdo->query("SELECT * FROM $database WHERE password='$password'");
		$resultcekPassword = $cekPassword->fetch(PDO::FETCH_ASSOC);

		if ($resultcekPassword['password'] != NULL) {
			echo "<script>window.alert('Password anda sudah terdaftar, mohon gunakan Password yang lain!'); window.location(history.back(-1))</script>";
			exit();
		}

		// cek username & password
		
		// Cek file tidak boleh kosong
		fileWajib($nama_file);
		// Cek file tidak boleh kosong

		// Cek ukuran yang di upload
		cekUkuranFile400kb($ukuran);
		// Cek ukuran yang di upload

		// Cek jenis file yang di upload
		cekFileGambar($tipe_file);
		// Cek jenis file yang di upload

		if (empty($nomor_whatsapp)) {
			$nomor_whatsapp == NULL;
		}

		// Cek nilai yang belum di isi
		formWajib($jenis_kelamin AND $level AND $status);
		// Cek nilai yang belum di isi

		try{

			$stmt = $pdo->prepare("INSERT INTO $database
							(username,password,nama,jenis_kelamin,nomor_whatsapp,level,status,gambar)
							VALUES(:username,:password,:nama,:jenis_kelamin,:nomor_whatsapp,:level,:status,:gambar)" );
					
			$stmt->bindParam(":username", $username, PDO::PARAM_STR);
			$stmt->bindParam(":password", $password, PDO::PARAM_STR);
			$stmt->bindParam(":nama", $nama, PDO::PARAM_STR);
			$stmt->bindParam(":jenis_kelamin", $jenis_kelamin, PDO::PARAM_STR);
			$stmt->bindParam(":nomor_whatsapp", $nomor_whatsapp, PDO::PARAM_STR);
			$stmt->bindParam(":level", $level, PDO::PARAM_STR);
			$stmt->bindParam(":status", $status, PDO::PARAM_STR);
			$stmt->bindParam(":gambar", $nama_file_unik, PDO::PARAM_STR);
				
			$count = $stmt->execute();
					
			uploadGambarWidth600($nama_file_unik, $tipe_file, $lokasi_file, $lokasi_upload);
					
			$insertId = $pdo->lastInsertId();
				
			echo "<script>alert('$hal berhasil di tambah'); window.location = '../../$link';</script>";
					
		}catch(PDOException $e){
			echo "<script>window.alert('$hal Gagal di tambah!'); window.location(history.back(-1))</script>";
		}

	}
	
	// Edit Website
	elseif ($module==$module2 AND $act=='edit'){

		// Cek gambar
		$CekGambar = $pdo->query("SELECT gambar FROM $database WHERE id_$database='$id_data_admin'");
		$tCekGambar = $CekGambar->fetch(PDO::FETCH_ASSOC);

		$cariExtensiGambar	= explode(".", $tCekGambar["gambar"]);
		$extensiGambarnya 	= $cariExtensiGambar[1];
		// Cek gambar

		if (!empty($nama_file)) {

			// Cek ukuran yang di upload
			cekUkuranFile400kb($ukuran);
			// Cek ukuran yang di upload

			// Cek jenis file yang di upload
			cekFileGambar($tipe_file);
			// Cek jenis file yang di upload

			// Hapus gambar
			unlink("$penyimpananGambar/$tCekGambar[gambar]");
			// Hapus gambar

			uploadGambarWidth600($nama_file_unik, $tipe_file, $lokasi_file, $lokasi_upload);

			$nama_file_edit	= $nama_file_unik;

		}elseif (empty($nama_file)) {

			rename("$penyimpananGambar/$tCekGambar[gambar]", "$penyimpananGambar/$nama_file_unik".$extensiGambarnya);

			$nama_file_edit	= $nama_file_unik.$extensiGambarnya;

		}

		try {
			$sql = "UPDATE $database
					SET username 		= :username,
						password 		= :password,
						nama 			= :nama,
						jenis_kelamin 	= :jenis_kelamin,
						nomor_whatsapp 	= :nomor_whatsapp,
						level 			= :level,
						status 			= :status,
						gambar 			= :gambar
					WHERE id_$database 	= :id_data_admin
				";
						  
			$statement = $pdo->prepare($sql);

			$statement->bindParam(":id_data_admin", $id_data_admin, PDO::PARAM_INT);
			$statement->bindParam(":username", $username, PDO::PARAM_STR);
			$statement->bindParam(":password", $password, PDO::PARAM_STR);
			$statement->bindParam(":nama", $nama, PDO::PARAM_STR);
			$statement->bindParam(":jenis_kelamin", $jenis_kelamin, PDO::PARAM_STR);
			$statement->bindParam(":nomor_whatsapp", $nomor_whatsapp, PDO::PARAM_STR);
			$statement->bindParam(":level", $level, PDO::PARAM_STR);
			$statement->bindParam(":status", $status, PDO::PARAM_STR);
			$statement->bindParam(":gambar", $nama_file_edit, PDO::PARAM_STR);

			$count = $statement->execute();

			echo "<script>alert('$hal berhasil di edit'); window.location = '../../media.php?module=$module&act=edit&id=$_POST[in_id_data_admin]'</script>";
					
		}catch(PDOException $e){
			echo "<script>window.alert('$hal Gagal di edit!'); window.location(history.back(-1))</script>";
		}

	}
	  	
	// Delete Website
	elseif ($module==$module2 AND $act=='delete'){

		$CekGambarHapus = $pdo->query("SELECT gambar FROM $database WHERE id_$database='$_GET[id]'");
		$tCekGambarHapus = $CekGambarHapus->fetch(PDO::FETCH_ASSOC);
		unlink("$penyimpananGambar/$tCekGambarHapus[gambar]");
		
		$del = $pdo->query("DELETE FROM $database WHERE id_$database='$_GET[id]'");
		$del->execute();
		
		echo "<script>alert('$hal berhasil di hapus'); window.location = '../../$link'</script>";
	}
	
	
}

?>
<center style="margin-top: 250px;"><img src="../../load.gif"></center>