<!DOCTYPE html>
<html>
<head>
	<title>REKAP DATA PARTISIPASI PENCOBLOS - www.kpump-tif.arpateam.com</title>
	
   	<link href="../../../assets/css/font-awesome.min.css" rel="stylesheet">

	<style type="text/css">

		.ket{
			text-align: center;
			margin-bottom: 25px;
			padding: 10px;
			box-sizing: border-box;
			background-color: #223668;
			color: #F1692B;
		}

		.ket2{
			margin-top: 100px;
		}

		.informasi{
			margin-top: 25px;
			margin-bottom: 25px;
			padding: 10px;
			background-color: #F39C12;
			border-radius: 10px;
			color: #fff;
		}

		.bg-green{
			background-color: #00A65A;
			border-radius: 10px;
			padding: 3px;
			font-weight: bolder;
		}

		.bg-red{
			background-color: #DD4B39;
			border-radius: 10px;
			padding: 3px;
			color: #fff;
			font-weight: bolder;
		}

		table{
			width: 100%;
			border-collapse: collapse;
		}

		table th,
		table td{
			border: 1px solid #3c3c3c;
		}

		table th{
			font-size: 18px;
			height: 35px;
	 		background-color: rgb(91, 155, 213);
	 	}

		table td{
			font-size: 16px;
			height: 25px;
	 	}

	 	tr:nth-child(even) {
		  	background-color: #f2f2f2;
		}

	</style>

</head>
<body>

	<?php $database = "vote"; ?>

	<header>
		<?php
			date_default_timezone_set("Asia/Jakarta");

			echo "<p>Date: ".date("Y-m-d H:i:s")." WIB</p>";
		?>
		<p></p>
	</header>

	<div class="ket">
		<h1>REKAP HASIL VOTE</h1>
		<h2>www.kpump-tif.arpateam.com</h2>
	</div>
 
	<table border="1">

		<tr>
			<th width="20%" class="center">Surat Suara ke-</th>
			<th width="25%" class="center">PASLON 01</th>
			<th width="25%" class="center">PASLON 02</th>
			<th width="25%" class="center">GOLPUT</th>
		</tr>

		<?php
			require '../../../system/koneksi.php';
			$no = 1;
			$pas01	= 0;
			$pas02	= 0;
			$pasGol	= 0;
			$tampil = $pdo->query("SELECT $database.hasil_vote FROM $database WHERE hasil_vote!=NULL OR hasil_vote!='0' ORDER BY waktu ASC");

			while($r = $tampil->fetch(PDO::FETCH_ASSOC)){
		?>

		<tr style="text-align: center;">
			<td><?= $no++; ?></td>
			<td><?php if($r['hasil_vote']==1) { echo "<i class='fa fa-check'></i>"; $pas01++; }; ?></td>
      		<td><?php if($r['hasil_vote']==2) { echo "<i class='fa fa-check'></i>"; $pas02++; }; ?></td>
      		<td><?php if($r['hasil_vote']==3) { echo "<i class='fa fa-check'></i>"; $pasGol++; }; ?></td>
		</tr>

		<?php
			}
		?>

		<tr>
			<th width="20%" class="center">JUMLAH</th>
			<th width="25%" class="center"><?= $pas01; ?></th>
			<th width="25%" class="center"><?= $pas02; ?></th>
			<th width="25%" class="center"><?= $pasGol; ?></th>
		</tr>

	</table>

	<div class="ket ket2">
		<h1>RINCIAN HASIL VOTE</h1>
		<h2>www.kpump-tif.arpateam.com</h2>
	</div>

	<?php
		$suratSuaraSAH = $pdo->query("SELECT nim FROM $database WHERE hasil_vote!=NULL OR hasil_vote!='0'");
		$tampilsuratSuaraSAH = $suratSuaraSAH->rowCount();

		$banyakData = $pdo->query("SELECT nim FROM $database");
		$tampilBanyakData = $banyakData->rowCount();
	?>

	<div class="informasi">
		<h4><i class="icon fa fa-warning"></i> Terdapat <span class="label bg-green"><?= $tampilsuratSuaraSAH; ?></span> Surat Suara yang SAH!</h4>
		Dari <span class="label bg-green"><?= $tampilBanyakData; ?></span> Partisipasi Pencoblos.
	</div>
 
	<table border="1" style="text-align: center;width: 75%">

		<tr>
	      	<th style="width: 50%">Data</th>
	      	<th style="width: 25%">Total Suara</th>
	      	<th style="width: 25%">Presentase</th>
		</tr>

		<?php 

			$rincianVotePaslon1 = $pdo->query("SELECT $database.nim FROM $database WHERE hasil_vote='1'");
			$rrincianVotePaslon1 = $rincianVotePaslon1->rowCount();

			$rincianVotePaslon2 = $pdo->query("SELECT $database.nim FROM $database WHERE hasil_vote='2'");
			$rrincianVotePaslon2 = $rincianVotePaslon2->rowCount();

			$rincianVotePaslonGolput = $pdo->query("SELECT $database.nim FROM $database WHERE hasil_vote='3'");
			$rrincianVotePaslonGolput = $rincianVotePaslonGolput->rowCount();

			$presentaseWASUP	= (100 * ($rrincianVotePaslon1/$tampilsuratSuaraSAH));
			$presentaseWIFI		= (100 * ($rrincianVotePaslon2/$tampilsuratSuaraSAH));
			$presentaseGOLPUT	= (100 * ($rrincianVotePaslonGolput/$tampilsuratSuaraSAH));

			if ($presentaseWASUP>$presentaseWIFI AND $presentaseWASUP>$presentaseGOLPUT) {
				$pemenangnya 	= "WIDA GHAIB NUGROHO - SUPRAYOGI BUDI .P";
				$presentasKemenangan	= number_format($presentaseWASUP,2);
			}elseif ($presentaseWIFI>$presentaseWASUP AND $presentaseWIFI>$presentaseGOLPUT) {
				$pemenangnya 	= "WISNU ANDRIAN - AMIRATUR RAFIFAH";
				$presentasKemenangan	= number_format($presentaseWIFI,2);
			}elseif ($presentaseWIFI==$presentaseWASUP) {
				$pemenangnya 	= "NULL";
				$presentasKemenangan	= "NULL";
			}

		?>

		<tr>
      		<td>
      			<h3>WIDA GHAIB NUGROHO - SUPRAYOGI BUDI .P</h3>
      		</td>
      		<td>
        		<h3><?= $rrincianVotePaslon1; ?></h3>
      		</td>
      		<td><span class="badge bg-red"><?= number_format($presentaseWASUP,2); ?>%</span></td>
      	</tr>

      	<tr>
      		<td>
      			<h3>WISNU ANDRIAN - AMIRATUR RAFIFAH</h3>
      		</td>
      		<td>
        		<h3><?= $rrincianVotePaslon2; ?></h3>
      		</td>
      		<td><span class="badge bg-red"><?= number_format($presentaseWIFI,2); ?>%</span></td>
      	</tr>

      	<tr>
      		<td>
      			<h3>GOLPUT</h3>
      		</td>
      		<td>
        		<h3><?= $rrincianVotePaslonGolput; ?></h3>
      		</td>
      		<td><span class="badge bg-red"><?= number_format($presentaseGOLPUT,2); ?>%</span></td>
    	</tr>

		<div class="informasi">
        	<h4><i class="icon fa fa-warning"></i> Pemenangnya adalah <span class="bg-green"><?= $pemenangnya; ?></span></h4>
            <p>Mendapatkan total <span class="bg-green"><?= $rrincianVotePaslon1; ?></span> Suara</p>
            <p>Dengan Presentase kemenangan <span class="bg-red"><?= $presentasKemenangan; ?>%</span></p>
        </div>

	</table>

	<script>
		window.print();
	</script>

</body>
</html>