<?php
if (empty($_SESSION['namaadmin']) AND empty($_SESSION['leveladmin'])){
	echo "<script>window.alert('Untuk mengakses halaman ini ada harus Login!'); window.location = 'login';</script>";
}else{

	$hal 				= "Rekap Hasil Vote (Per Angkatan)";
	$database 			= "vote";

	if (isset($_POST['submit'])) {
		$del = $pdo->query("DELETE FROM $database");
		$del->execute();
		echo "<script>alert('Data Login Portal Admin berhasil di reset');</script>";
	}
	
?>

	<section class="content">

			<div class="row">

				<div class="col-md-12">

					<div class="box box-primary">
						<div class="box-header">
							<h1 style="text-transform: capitalize;"><?= $hal; ?></h1>
							<ol class="breadcrumb">
								<li><a href="media.php?module=home"><i class="fa fa-dashboard"></i> Dasboard</a></li>
								<li class="active"><?= $hal; ?></li>
							</ol>
						</div><!-- /.box-header -->

						<?php
							$suratSuaraSAH = $pdo->query("SELECT nim FROM $database WHERE hasil_vote!=NULL OR hasil_vote!='0'");
							$tampilsuratSuaraSAH = $suratSuaraSAH->rowCount();

							$banyakData = $pdo->query("SELECT nim FROM $database");
							$tampilBanyakData = $banyakData->rowCount();
						?>

						<div class="box-body">
							<div class="alert alert-warning alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				            	<h4><i class="icon fa fa-warning"></i> Terdapat <span class="label bg-green"><?= $tampilsuratSuaraSAH; ?></span> Surat Suara yang SAH!</h4>
				                Dari <span class="label bg-green"><?= $tampilBanyakData; ?></span> Partisipasi Pencoblos.
				            </div>
				            <a href="#" role="button" class="btn btn-block btn-danger">Export to PDF <i class="fa fa-file-pdf-o"></i></a>
				        </div>
				
						<div class="box-body table-responsive" style="overflow-x: auto;">
							<table id="example1" class="table table-bordered table-striped">

								<thead>
						  			<tr>
										<th width="5%" class="center">No</th>
										<th width="25%" class="center">Angkatan</th>
										<th width="25%" class="center bg-green">PASLON 01</th>
										<th width="25%" class="center bg-blue">PASLON 02</th>
										<th width="20%" class="center bg-red">GOLPUT</th>
						  			</tr>
								</thead>

								<tbody>

									<!-- Query menampilkan rekap hasil vote (per angkatan) -->

									<?php

										$VoteAngkatan20_01 = $pdo->query("SELECT * FROM $database WHERE hasil_vote='1' AND nim LIKE '20%'");
										$tampilVoteAngkatan20_01 = $VoteAngkatan20_01->rowCount();

										$VoteAngkatan20_02 = $pdo->query("SELECT * FROM $database WHERE hasil_vote='2' AND nim LIKE '20%'");
										$tampilVoteAngkatan20_02 = $VoteAngkatan20_02->rowCount();

										$VoteAngkatan20_03 = $pdo->query("SELECT * FROM $database WHERE hasil_vote='3' AND nim LIKE '20%'");
										$tampilVoteAngkatan20_03 = $VoteAngkatan20_03->rowCount();

										$VoteAngkatan19_01 = $pdo->query("SELECT * FROM $database WHERE hasil_vote='1' AND nim LIKE '19%'");
										$tampilVoteAngkatan19_01 = $VoteAngkatan19_01->rowCount();

										$VoteAngkatan19_02 = $pdo->query("SELECT * FROM $database WHERE hasil_vote='2' AND nim LIKE '19%'");
										$tampilVoteAngkatan19_02 = $VoteAngkatan19_02->rowCount();

										$VoteAngkatan19_03 = $pdo->query("SELECT * FROM $database WHERE hasil_vote='3' AND nim LIKE '19%'");
										$tampilVoteAngkatan19_03 = $VoteAngkatan19_03->rowCount();

										$VoteAngkatan18_01 = $pdo->query("SELECT * FROM $database WHERE hasil_vote='1' AND nim LIKE '18%'");
										$tampilVoteAngkatan18_01 = $VoteAngkatan18_01->rowCount();

										$VoteAngkatan18_02 = $pdo->query("SELECT * FROM $database WHERE hasil_vote='2' AND nim LIKE '18%'");
										$tampilVoteAngkatan18_02 = $VoteAngkatan18_02->rowCount();

										$VoteAngkatan18_03 = $pdo->query("SELECT * FROM $database WHERE hasil_vote='3' AND nim LIKE '18%'");
										$tampilVoteAngkatan18_03 = $VoteAngkatan18_03->rowCount();

										$VoteAngkatan17_01 = $pdo->query("SELECT * FROM $database WHERE hasil_vote='1' AND nim LIKE '17%'");
										$tampilVoteAngkatan17_01 = $VoteAngkatan17_01->rowCount();

										$VoteAngkatan17_02 = $pdo->query("SELECT * FROM $database WHERE hasil_vote='2' AND nim LIKE '17%'");
										$tampilVoteAngkatan17_02 = $VoteAngkatan17_02->rowCount();

										$VoteAngkatan17_03 = $pdo->query("SELECT * FROM $database WHERE hasil_vote='3' AND nim LIKE '17%'");
										$tampilVoteAngkatan17_03 = $VoteAngkatan17_03->rowCount();

										$VoteAngkatan16_01 = $pdo->query("SELECT * FROM $database WHERE hasil_vote='1' AND nim LIKE '16%'");
										$tampilVoteAngkatan16_01 = $VoteAngkatan16_01->rowCount();

										$VoteAngkatan16_02 = $pdo->query("SELECT * FROM $database WHERE hasil_vote='2' AND nim LIKE '16%'");
										$tampilVoteAngkatan16_02 = $VoteAngkatan16_02->rowCount();

										$VoteAngkatan16_03 = $pdo->query("SELECT * FROM $database WHERE hasil_vote='3' AND nim LIKE '16%'");
										$tampilVoteAngkatan16_03 = $VoteAngkatan16_03->rowCount();

										$VoteAngkatan15_01 = $pdo->query("SELECT * FROM $database WHERE hasil_vote='1' AND nim LIKE '15%'");
										$tampilVoteAngkatan15_01 = $VoteAngkatan15_01->rowCount();

										$VoteAngkatan15_02 = $pdo->query("SELECT * FROM $database WHERE hasil_vote='2' AND nim LIKE '15%'");
										$tampilVoteAngkatan15_02 = $VoteAngkatan15_02->rowCount();

										$VoteAngkatan15_03 = $pdo->query("SELECT * FROM $database WHERE hasil_vote='3' AND nim LIKE '15%'");
										$tampilVoteAngkatan15_03 = $VoteAngkatan15_03->rowCount();

										$VoteAngkatan14_01 = $pdo->query("SELECT * FROM $database WHERE hasil_vote='1' AND nim LIKE '14%'");
										$tampilVoteAngkatan14_01 = $VoteAngkatan14_01->rowCount();

										$VoteAngkatan14_02 = $pdo->query("SELECT * FROM $database WHERE hasil_vote='2' AND nim LIKE '14%'");
										$tampilVoteAngkatan14_02 = $VoteAngkatan14_02->rowCount();

										$VoteAngkatan14_03 = $pdo->query("SELECT * FROM $database WHERE hasil_vote='3' AND nim LIKE '14%'");
										$tampilVoteAngkatan14_03 = $VoteAngkatan14_03->rowCount();

										$VoteAngkatan13_01 = $pdo->query("SELECT * FROM $database WHERE hasil_vote='1' AND nim LIKE '13%'");
										$tampilVoteAngkatan13_01 = $VoteAngkatan13_01->rowCount();

										$VoteAngkatan13_02 = $pdo->query("SELECT * FROM $database WHERE hasil_vote='2' AND nim LIKE '13%'");
										$tampilVoteAngkatan13_02 = $VoteAngkatan13_02->rowCount();

										$VoteAngkatan13_03 = $pdo->query("SELECT * FROM $database WHERE hasil_vote='3' AND nim LIKE '13%'");
										$tampilVoteAngkatan13_03 = $VoteAngkatan13_03->rowCount();

									?>

									<tr style="text-align: center;">
										<td>1.</td>
										<td class="<?php if($tampilVoteAngkatan20_01>$tampilVoteAngkatan20_02 AND $tampilVoteAngkatan20_01>$tampilVoteAngkatan20_03){ echo "bg-green"; }elseif($tampilVoteAngkatan20_02>$tampilVoteAngkatan20_01 AND $tampilVoteAngkatan20_02>$tampilVoteAngkatan20_03){ echo "bg-blue"; }elseif($tampilVoteAngkatan20_03>$tampilVoteAngkatan20_01 AND $tampilVoteAngkatan20_03>$tampilVoteAngkatan20_02){ echo "bg-red"; } ?>">Angkatan 2020</td>
							      		<td class="<?php if($tampilVoteAngkatan20_01>$tampilVoteAngkatan20_02 AND $tampilVoteAngkatan20_01>$tampilVoteAngkatan20_03){ echo "bg-green"; } ?>"><?= $tampilVoteAngkatan20_01; ?></td>
							      		<td class="<?php if($tampilVoteAngkatan20_02>$tampilVoteAngkatan20_01 AND $tampilVoteAngkatan20_02>$tampilVoteAngkatan20_03){ echo "bg-blue"; } ?>"><?= $tampilVoteAngkatan20_02; ?></td>
							      		<td class="<?php if($tampilVoteAngkatan20_03>$tampilVoteAngkatan20_01 AND $tampilVoteAngkatan20_03>$tampilVoteAngkatan20_02){ echo "bg-red"; } ?>"><?= $tampilVoteAngkatan20_03; ?></td>
									</tr>

									<tr style="text-align: center;">
										<td>2.</td>
										<td class="<?php if($tampilVoteAngkatan19_01>$tampilVoteAngkatan19_02 AND $tampilVoteAngkatan19_01>$tampilVoteAngkatan19_03){ echo "bg-green"; }elseif($tampilVoteAngkatan19_02>$tampilVoteAngkatan19_01 AND $tampilVoteAngkatan19_02>$tampilVoteAngkatan19_03){ echo "bg-blue"; }elseif($tampilVoteAngkatan19_03>$tampilVoteAngkatan19_01 AND $tampilVoteAngkatan19_03>$tampilVoteAngkatan19_02){ echo "bg-red"; } ?>">Angkatan 2019</td>
							      		<td class="<?php if($tampilVoteAngkatan19_01>$tampilVoteAngkatan19_02 AND $tampilVoteAngkatan19_01>$tampilVoteAngkatan19_03){ echo "bg-green"; } ?>"><?= $tampilVoteAngkatan19_01; ?></td>
							      		<td class="<?php if($tampilVoteAngkatan19_02>$tampilVoteAngkatan19_01 AND $tampilVoteAngkatan19_02>$tampilVoteAngkatan19_03){ echo "bg-blue"; } ?>"><?= $tampilVoteAngkatan19_02; ?></td>
							      		<td class="<?php if($tampilVoteAngkatan19_03>$tampilVoteAngkatan19_01 AND $tampilVoteAngkatan19_03>$tampilVoteAngkatan19_02){ echo "bg-red"; } ?>"><?= $tampilVoteAngkatan19_03; ?></td>
									</tr>

									<tr style="text-align: center;">
										<td>3.</td>
										<td class="<?php if($tampilVoteAngkatan18_01>$tampilVoteAngkatan18_02 AND $tampilVoteAngkatan18_01>$tampilVoteAngkatan18_03){ echo "bg-green"; }elseif($tampilVoteAngkatan18_02>$tampilVoteAngkatan18_01 AND $tampilVoteAngkatan18_02>$tampilVoteAngkatan18_03){ echo "bg-blue"; }elseif($tampilVoteAngkatan18_03>$tampilVoteAngkatan18_01 AND $tampilVoteAngkatan18_03>$tampilVoteAngkatan18_02){ echo "bg-red"; } ?>">Angkatan 2018</td>
							      		<td class="<?php if($tampilVoteAngkatan18_01>$tampilVoteAngkatan18_02 AND $tampilVoteAngkatan18_01>$tampilVoteAngkatan18_03){ echo "bg-green"; } ?>"><?= $tampilVoteAngkatan18_01; ?></td>
							      		<td class="<?php if($tampilVoteAngkatan18_02>$tampilVoteAngkatan18_01 AND $tampilVoteAngkatan18_02>$tampilVoteAngkatan18_03){ echo "bg-blue"; } ?>"><?= $tampilVoteAngkatan18_02; ?></td>
							      		<td class="<?php if($tampilVoteAngkatan18_03>$tampilVoteAngkatan18_01 AND $tampilVoteAngkatan18_03>$tampilVoteAngkatan18_02){ echo "bg-red"; } ?>"><?= $tampilVoteAngkatan18_03; ?></td>
									</tr>

									<tr style="text-align: center;">
										<td>4.</td>
										<td class="<?php if($tampilVoteAngkatan17_01>$tampilVoteAngkatan17_02 AND $tampilVoteAngkatan17_01>$tampilVoteAngkatan17_03){ echo "bg-green"; }elseif($tampilVoteAngkatan17_02>$tampilVoteAngkatan17_01 AND $tampilVoteAngkatan17_02>$tampilVoteAngkatan17_03){ echo "bg-blue"; }elseif($tampilVoteAngkatan17_03>$tampilVoteAngkatan17_01 AND $tampilVoteAngkatan17_03>$tampilVoteAngkatan17_02){ echo "bg-red"; } ?>">Angkatan 2017</td>
							      		<td class="<?php if($tampilVoteAngkatan17_01>$tampilVoteAngkatan17_02 AND $tampilVoteAngkatan17_01>$tampilVoteAngkatan17_03){ echo "bg-green"; } ?>"><?= $tampilVoteAngkatan17_01; ?></td>
							      		<td class="<?php if($tampilVoteAngkatan17_02>$tampilVoteAngkatan17_01 AND $tampilVoteAngkatan17_02>$tampilVoteAngkatan17_03){ echo "bg-blue"; } ?>"><?= $tampilVoteAngkatan17_02; ?></td>
							      		<td class="<?php if($tampilVoteAngkatan17_03>$tampilVoteAngkatan17_01 AND $tampilVoteAngkatan17_03>$tampilVoteAngkatan17_02){ echo "bg-red"; } ?>"><?= $tampilVoteAngkatan17_03; ?></td>
									</tr>

									<tr style="text-align: center;">
										<td>5.</td>
										<td class="<?php if($tampilVoteAngkatan16_01>$tampilVoteAngkatan16_02 AND $tampilVoteAngkatan16_01>$tampilVoteAngkatan16_03){ echo "bg-green"; }elseif($tampilVoteAngkatan16_02>$tampilVoteAngkatan16_01 AND $tampilVoteAngkatan16_02>$tampilVoteAngkatan16_03){ echo "bg-blue"; }elseif($tampilVoteAngkatan16_03>$tampilVoteAngkatan16_01 AND $tampilVoteAngkatan16_03>$tampilVoteAngkatan16_02){ echo "bg-red"; } ?>">Angkatan 2016</td>
							      		<td class="<?php if($tampilVoteAngkatan16_01>$tampilVoteAngkatan16_02 AND $tampilVoteAngkatan16_01>$tampilVoteAngkatan16_03){ echo "bg-green"; } ?>"><?= $tampilVoteAngkatan16_01; ?></td>
							      		<td class="<?php if($tampilVoteAngkatan16_02>$tampilVoteAngkatan16_01 AND $tampilVoteAngkatan16_02>$tampilVoteAngkatan16_03){ echo "bg-blue"; } ?>"><?= $tampilVoteAngkatan16_02; ?></td>
							      		<td class="<?php if($tampilVoteAngkatan16_03>$tampilVoteAngkatan16_01 AND $tampilVoteAngkatan16_03>$tampilVoteAngkatan16_02){ echo "bg-red"; } ?>"><?= $tampilVoteAngkatan16_03; ?></td>
									</tr>

									<tr style="text-align: center;">
										<td>6.</td>
										<td class="<?php if($tampilVoteAngkatan15_01>$tampilVoteAngkatan15_02 AND $tampilVoteAngkatan15_01>$tampilVoteAngkatan15_03){ echo "bg-green"; }elseif($tampilVoteAngkatan15_02>$tampilVoteAngkatan15_01 AND $tampilVoteAngkatan15_02>$tampilVoteAngkatan15_03){ echo "bg-blue"; }elseif($tampilVoteAngkatan15_03>$tampilVoteAngkatan15_01 AND $tampilVoteAngkatan15_03>$tampilVoteAngkatan15_02){ echo "bg-red"; } ?>">Angkatan 2015</td>
							      		<td class="<?php if($tampilVoteAngkatan15_01>$tampilVoteAngkatan15_02 AND $tampilVoteAngkatan15_01>$tampilVoteAngkatan15_03){ echo "bg-green"; } ?>"><?= $tampilVoteAngkatan15_01; ?></td>
							      		<td class="<?php if($tampilVoteAngkatan15_02>$tampilVoteAngkatan15_01 AND $tampilVoteAngkatan15_02>$tampilVoteAngkatan15_03){ echo "bg-blue"; } ?>"><?= $tampilVoteAngkatan15_02; ?></td>
							      		<td class="<?php if($tampilVoteAngkatan15_03>$tampilVoteAngkatan15_01 AND $tampilVoteAngkatan15_03>$tampilVoteAngkatan15_02){ echo "bg-red"; } ?>"><?= $tampilVoteAngkatan15_03; ?></td>
									</tr>

									<tr style="text-align: center;">
										<td>7.</td>
										<td class="<?php if($tampilVoteAngkatan14_01>$tampilVoteAngkatan14_02 AND $tampilVoteAngkatan14_01>$tampilVoteAngkatan14_03){ echo "bg-green"; }elseif($tampilVoteAngkatan14_02>$tampilVoteAngkatan14_01 AND $tampilVoteAngkatan14_02>$tampilVoteAngkatan14_03){ echo "bg-blue"; }elseif($tampilVoteAngkatan14_03>$tampilVoteAngkatan14_01 AND $tampilVoteAngkatan14_03>$tampilVoteAngkatan14_02){ echo "bg-red"; } ?>">Angkatan 2014</td>
							      		<td class="<?php if($tampilVoteAngkatan14_01>$tampilVoteAngkatan14_02 AND $tampilVoteAngkatan14_01>$tampilVoteAngkatan14_03){ echo "bg-green"; } ?>"><?= $tampilVoteAngkatan14_01; ?></td>
							      		<td class="<?php if($tampilVoteAngkatan14_02>$tampilVoteAngkatan14_01 AND $tampilVoteAngkatan14_02>$tampilVoteAngkatan14_03){ echo "bg-blue"; } ?>"><?= $tampilVoteAngkatan14_02; ?></td>
							      		<td class="<?php if($tampilVoteAngkatan14_03>$tampilVoteAngkatan14_01 AND $tampilVoteAngkatan14_03>$tampilVoteAngkatan14_02){ echo "bg-red"; } ?>"><?= $tampilVoteAngkatan14_03; ?></td>
									</tr>

									<tr style="text-align: center;">
										<td>8.</td>
										<td class="<?php if($tampilVoteAngkatan13_01>$tampilVoteAngkatan13_02 AND $tampilVoteAngkatan13_01>$tampilVoteAngkatan13_03){ echo "bg-green"; }elseif($tampilVoteAngkatan13_02>$tampilVoteAngkatan13_01 AND $tampilVoteAngkatan13_02>$tampilVoteAngkatan13_03){ echo "bg-blue"; }elseif($tampilVoteAngkatan13_03>$tampilVoteAngkatan13_01 AND $tampilVoteAngkatan13_03>$tampilVoteAngkatan13_02){ echo "bg-red"; } ?>">Angkatan 2013</td>
							      		<td class="<?php if($tampilVoteAngkatan13_01>$tampilVoteAngkatan13_02 AND $tampilVoteAngkatan13_01>$tampilVoteAngkatan13_03){ echo "bg-green"; } ?>"><?= $tampilVoteAngkatan13_01; ?></td>
							      		<td class="<?php if($tampilVoteAngkatan13_02>$tampilVoteAngkatan13_01 AND $tampilVoteAngkatan13_02>$tampilVoteAngkatan13_03){ echo "bg-blue"; } ?>"><?= $tampilVoteAngkatan13_02; ?></td>
							      		<td class="<?php if($tampilVoteAngkatan13_03>$tampilVoteAngkatan13_01 AND $tampilVoteAngkatan13_03>$tampilVoteAngkatan13_02){ echo "bg-red"; } ?>"><?= $tampilVoteAngkatan13_03; ?></td>
									</tr>

								</tbody>

								<?php
									$rincianVotePaslon1 = $pdo->query("SELECT $database.nim FROM $database WHERE hasil_vote='1'");
									$rrincianVotePaslon1 = $rincianVotePaslon1->rowCount();

			                		$rincianVotePaslon2 = $pdo->query("SELECT $database.nim FROM $database WHERE hasil_vote='2'");
									$rrincianVotePaslon2 = $rincianVotePaslon2->rowCount();

			                		$rincianVotePaslonGolput = $pdo->query("SELECT $database.nim FROM $database WHERE hasil_vote='3'");
									$rrincianVotePaslonGolput = $rincianVotePaslonGolput->rowCount();
								?>

								<thead style="font-size: 20px;">
						  			<tr>
										<th width="30%" class="center" colspan="2">JUMLAH</th>
										<th width="25%" class="center <?php if($rrincianVotePaslon1>$rrincianVotePaslon2 AND $rrincianVotePaslon1>$rrincianVotePaslonGolput){ echo "bg-green"; } ?>"><?= $rrincianVotePaslon1; ?></th>
										<th width="25%" class="center <?php if($rrincianVotePaslon2>$rrincianVotePaslon1 AND $rrincianVotePaslon2>$rrincianVotePaslonGolput){ echo "bg-blue"; } ?>"><?= $rrincianVotePaslon2; ?></th>
										<th width="20%" class="center <?php if($rrincianVotePaslonGolput>$rrincianVotePaslon1 AND $rrincianVotePaslonGolput>$rrincianVotePaslon2){ echo "bg-red"; } ?>"><?= $rrincianVotePaslonGolput; ?></th>
						  			</tr>
								</thead>

					 		</table>
						</div><!-- /.box-body -->

				  	</div><!-- /.box -->
			   
				</div><!-- /.col -->

			</div>
		
		</section><!-- /.col -->
		
<?php
	}
?>