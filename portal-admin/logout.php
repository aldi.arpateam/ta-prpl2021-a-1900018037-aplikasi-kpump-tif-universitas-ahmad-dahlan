<?php

	require "../system/koneksi.php";

	session_start();

	$id_data_login_portal_admin	= $_SESSION['id_data_login_portal_admin'];

	try {

		$sql = "UPDATE data_login_portal_admin
				SET waktu_logout = now()
				WHERE id_data_login_portal_admin 	= :id_data_login_portal_admin
			";
						  
		$statement = $pdo->prepare($sql);

		$statement->bindParam(":id_data_login_portal_admin", $id_data_login_portal_admin, PDO::PARAM_INT);

		$count = $statement->execute();

		session_destroy();

		echo "<script>alert('Proses logout sukses!!'); window.location = 'index.php'</script>";
					
	}catch(PDOException $e){
		echo "<script>window.alert('Gagal logout!'); window.location(history.back(-1))</script>";
	}
	

?>
