<?php
//error_reporting(E_PARSE); 

if ($_SESSION['leveladmin']=='Administrator' OR $_SESSION['leveladmin']=='Admin Utama' OR $_SESSION['leveladmin']=='Marketing' OR $_SESSION['leveladmin']=='Penulis'){

	if ($_GET['module']=='dashboard'){
		include "modul/dashboard/index.php";
	}

	elseif ($_GET['module']=='rekap-partisipasi-pencoblos'){
		include "modul/rekap-partisipasi-pencoblos/index.php";
	}

	elseif ($_GET['module']=='rekap-hasil-vote'){
		include "modul/rekap-hasil-vote/index.php";
	}

	elseif ($_GET['module']=='rekap-hasil-vote-per-angkatan'){
		include "modul/rekap-hasil-vote-per-angkatan/index.php";
	}

	elseif ($_GET['module']=='rekap-kritik-saran'){
		include "modul/rekap-kritik-saran/index.php";
	}

	elseif ($_GET['module']=='mahasiswa-aktif'){
		include "modul/mahasiswa-aktif/index.php";
	}
	
	elseif($_GET['module']=='data-admin'){
		include "modul/data-admin/index.php";
	}
	
	elseif($_GET['module']=='modul'){
		include "modul/modul/index.php";
	}

	elseif($_GET['module']=='page'){
		include "modul/page/index.php";
	}
	
	elseif($_GET['module']=='paket-website'){
		include "modul/paket-website/index.php";
	}
	
	elseif($_GET['module']=='konten-halaman'){
		include "modul/konten-halaman/index.php";
	}
	
	elseif($_GET['module']=='seputar-jasa-pembuatan-website'){
		include "modul/seputar-jasa-pembuatan-website/index.php";
	}
	
	elseif($_GET['module']=='contoh-desain-website'){
		include "modul/contoh-desain-website/index.php";
	}
	
	elseif($_GET['module']=='portofolio'){
		include "modul/portofolio/index.php";
	}
	
	elseif($_GET['module']=='paket-layanan-lain'){
		include "modul/paket-layanan-lain/index.php";
	}

	elseif($_GET['module']=='klien-kami'){
		include "modul/klien-kami/index.php";
	}

	elseif($_GET['module']=='mitra-kami'){
		include "modul/mitra-kami/index.php";
	}

	elseif($_GET['module']=='story-arpateam'){
		include "modul/story-arpateam/index.php";
	}

	elseif($_GET['module']=='promosi'){
		include "modul/promosi/index.php";
	}

	elseif($_GET['module']=='sitemap-1'){
		include "modul/sitemap-1/index.php";
	}

	elseif($_GET['module']=='sitemap-2'){
		include "modul/sitemap-2/index.php";
	}
	
	elseif($_GET['module']=='error'){
		include "modul/error/index.php";
	}
	
	// Apabila modul tidak ditemukan
	else{
	  echo "<p><b>not found!</b></p>";
	}

}

?>
