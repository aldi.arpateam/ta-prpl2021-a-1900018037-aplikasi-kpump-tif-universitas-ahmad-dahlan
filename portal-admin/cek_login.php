<?php

	require "../system/koneksi.php";

	$username = $_POST['username'];
	$pass     = $_POST['password'];

	$login = $pdo->query("SELECT * FROM data_admin WHERE username='$username' AND password='$pass' AND status='Aktif'");
	$ketemu = $login->rowCount();
	$r = $login->fetch(PDO::FETCH_ASSOC);

	// Apabila username dan password ditemukan
	if ($ketemu > 0){

		session_start();
	  
		$_SESSION['iddataadmin']     	= $r['id_data_admin'];
		$_SESSION['namaadmin']     		= $r['username'];
		$_SESSION['passadmin']    		= $r['password'];
		$_SESSION['namalengkapadmin']  	= $r['nama'];
		$_SESSION['jeniskelamin']  		= $r['jenis_kelamin'];
		$_SESSION['gambar']  			= $r['gambar'];
		$_SESSION['leveladmin']   		= $r['level'];
		$status_login					= "Berhasil";

		try{

			$stmt = $pdo->prepare("INSERT INTO data_login_portal_admin
							(username,password,nama,level,status_login,waktu_login, waktu_logout)
							VALUES(:username,:password,:nama,:level,:status_login, now(), NULL )" );
					
			$stmt->bindParam(":username", $_SESSION['namaadmin'], PDO::PARAM_STR);
			$stmt->bindParam(":password", $_SESSION['passadmin'], PDO::PARAM_STR);
			$stmt->bindParam(":nama", $_SESSION['namalengkapadmin'], PDO::PARAM_STR);
			$stmt->bindParam(":level", $_SESSION['leveladmin'], PDO::PARAM_STR);
			$stmt->bindParam(":status_login", $status_login, PDO::PARAM_STR);
				
			$count = $stmt->execute();
					
			$insertId = $pdo->lastInsertId();

			$_SESSION['id_data_login_portal_admin'] = $insertId;

			echo "<script>window.alert('Hallo ".$_SESSION['namalengkapadmin'].", anda berhasil login!'); window.location = 'home';</script>";
					
		}catch(PDOException $e){
			echo "<script>window.alert('Gagal di login!'); window.location(history.back(-1))</script>";
		}
		
	}else{

		try{

			$nama 			= NULL;
			$level 			= NULL;
			$status_login	= "Gagal";

			$stmtGagalLogin = $pdo->prepare("INSERT INTO data_login_portal_admin
							(username,password,nama,level,status_login,waktu_login, waktu_logout)
							VALUES(:username,:password,:nama,:level,:status_login, now(), NULL )" );
					
			$stmtGagalLogin->bindParam(":username", $username, PDO::PARAM_STR);
			$stmtGagalLogin->bindParam(":password", $pass, PDO::PARAM_STR);
			$stmtGagalLogin->bindParam(":nama", $nama, PDO::PARAM_STR);
			$stmtGagalLogin->bindParam(":level", $level, PDO::PARAM_STR);
			$stmtGagalLogin->bindParam(":status_login", $status_login, PDO::PARAM_STR);
				
			$count = $stmtGagalLogin->execute();
					
			$insertId = $pdo->lastInsertId();

			echo "<script>window.alert('Maaf anda gagal login, mohon masukkan Username & Password dengan benar!'); window.location(history.back(-1))</script>";
					
		}catch(PDOException $e){
			echo "<script>window.alert('Gagal di login!'); window.location(history.back(-1))</script>";
		}
		
	}

?>