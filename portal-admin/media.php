<?php

session_start();
// error_reporting(0);

if (empty($_SESSION['iddataadmin']) AND empty($_SESSION['namaadmin']) AND empty($_SESSION['passadmin']) AND empty($_SESSION['gambar']) AND empty($_SESSION['leveladmin']) AND empty($_SESSION['namalengkapadmin']) AND empty($_SESSION['leveladmin'])){
	
	echo "<script>window.alert('Untuk mengakses halaman ini ada harus Login!'); window.location = 'login';</script>";
  
}else{
	
	require "../system/koneksi.php";
	require "../system/fungsi_indotgl.php";
    require "../system/fungsi_rupiah.php";
    require "../system/z_setting.php";

?>

<!DOCTYPE html>
<html>
<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <title><?= $namaweb; ?> | <?= $_SESSION['namalengkapadmin']." - ".$_SESSION['leveladmin']; ?></title>
   <!-- Tell the browser to be responsive to screen width -->
   <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

   <?php require 'icon.php'; ?>
	
   <link href="../assets/css/font-awesome.min.css" rel="stylesheet">
   <link href="../assets/css/font-awesome.css" rel="stylesheet">
	
   <!-- Bootstrap 3.3.5 -->
   <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
   <!-- Theme style -->
   <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
   <!-- AdminLTE Skins. Choose a skin from the css/skins
   folder instead of downloading all of them to reduce the load. -->
   <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
   <!-- DataTables -->
   <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">

   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
   <!--[if lt IE 9]>
   <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
   <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
   <![endif]-->
</head>

<body class="hold-transition skin-black sidebar-mini">

    <div class="wrapper">

        <header class="main-header">

            <!-- Logo -->
            <a href="home" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><?= $namaweb; ?></span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg" title="Admin <?= $namaweb; ?>"><b>Admin Panel</b></span>
            </a>

            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">

                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                   <span class="sr-only">Toggle navigation</span>
                </a>

                <div class="navbar-custom-menu">

                   <ul class="nav navbar-nav">

                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                                <?php

                                    if ($_SESSION['gambar'] == NULL) {

                                        if ($_SESSION['jeniskelamin'] == "Laki-Laki") {
                                            echo "<img src='avatar-laki-laki.png' class='user-image' alt='Foto ".$_SESSION['namalengkapadmin']."'>";
                                        }elseif ($_SESSION['jeniskelamin'] == "Perempuan") {
                                            echo "<img src='avatar-perempuan.png' class='user-image' alt='Foto ".$_SESSION['namalengkapadmin']."'>";
                                        }

                                    }elseif ($_SESSION['gambar'] != NULL) {

                                        echo "<img src='images/data-admin/".$_SESSION['gambar']."' class='user-image' alt='Foto ".$_SESSION['namalengkapadmin']."'>";

                                    }

                                ?>

                                <span class="hidden-xs"><?php $_SESSION['namalengkapadmin']; ?></span>
                            </a>

                            <ul class="dropdown-menu">

                                <!-- User image -->
                                <li class="user-header">

                                    <?php

                                        if ($_SESSION['gambar'] == NULL) {

                                            if ($_SESSION['jeniskelamin'] == "Laki-Laki") {
                                                echo "<img src='avatar-laki-laki.png' class='img-circle' alt='Foto ".$_SESSION['namalengkapadmin']."'>";
                                            }elseif ($_SESSION['jeniskelamin'] == "Perempuan") {
                                                echo "<img src='avatar-perempuan.png' class='img-circle' alt='Foto ".$_SESSION['namalengkapadmin']."'>";
                                            }

                                        }elseif ($_SESSION['gambar'] != NULL) {

                                            echo "<img src='images/data-admin/".$_SESSION['gambar']."' class='img-circle' alt='Foto ".$_SESSION['namalengkapadmin']."'>";

                                        }

                                    ?>

                                    <p style="text-transform: capitalize;">
                                        <?= $_SESSION['namalengkapadmin']; ?> - <?= $_SESSION['leveladmin']; ?>
                                        <!-- <small>Member since Nov. 2012</small> -->
                                    </p>

                                </li>

                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="media.php?module=data-admin&act=edit&id=<?= $_SESSION['iddataadmin']; ?>" class="btn btn-primary btn-flat">Profile <i class="fa fa-user-circle-o"></i></a>
                                    </div>
                                    <div class="pull-right">
                                        <a onclick="return confirm('Apakah anda ingin Logout?')" href="logout.php" class="btn btn-primary btn-flat">Logout <i class="fa fa-sign-out"></i></a>
                                    </div>
                                </li>

                            </ul>

                        </li>
                        <!-- Control Sidebar Toggle Button -->

                    </ul>

                </div>

            </nav>

        </header>

        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">

            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">

                <!-- sidebar menu: : style can be found in sidebar.less -->
                <?php require "menu.php"; ?>

            </section>
            <!-- /.sidebar -->

        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            <?php require "content.php"; ?>

        </div>
        <!-- /.content-wrapper -->

        <footer class="main-footer no-print">
            <div class="pull-right hidden-xs">
                <b>Portal Admin Version</b> 2.0
            </div>
            <strong>Copyright &copy; 2018-<?= date('Y'); ?> <a href="https://arpateam.com">#ARPA<span style="font-weight: normal;">TEAM</span></a>.</strong> All rights reserved.
        </footer>

        <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>

    </div>
    <!-- ./wrapper -->
    
    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick
    <script src="plugins/fastclick/fastclick.min.js"></script> -->
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- DataTables -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	
	
    <!-- Select2 -->
    <script src="plugins/select2/select2.full.min.js"></script>
    <!-- InputMask -->
    <script src="plugins/input-mask/jquery.inputmask.js"></script>
    <script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>
	
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- bootstrap time picker -->
    <script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>
	
	
    <script src="ckeditor/ckeditor.js"></script>
    <!--
    <script src="editor/ckeditor/ckeditor.js"></script>
	<script>
		CKEDITOR.replace( 'editor1' );
	</script>
	-->
    <script>

        $(function () {

            //Datemask dd/mm/yyyy
            $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
            //Datemask2 mm/dd/yyyy
            $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
            //Money Euro
            $("[data-mask]").inputmask();

            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });

            $('#example3').DataTable({
                "paging": false,
                "lengthChange": false,
                "searching": false,
                "ordering": false,
                "info": false,
                "autoWidth": false
            });

            //Timepicker
            $(".timepicker").timepicker({
		          showMeridian: false, //format 24jm
                showInputs: false
            });
        });

    </script>
	
</body>

</html>

<?php
    }
?>