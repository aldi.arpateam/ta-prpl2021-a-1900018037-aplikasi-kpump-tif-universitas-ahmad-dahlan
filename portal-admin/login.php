<?php

	session_start();
	
	require "../system/koneksi.php";
	require "../system/z_setting.php";

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Admin <?php echo $namaweb; ?> | Log in</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php require 'icon.php'; ?>
 
	<!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="dist/css/style-login.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
	
	<script language="javascript">

		function validasi(form){

	  		if (form.username.value == ""){
				alert("Anda belum mengisikan Username.");
				form.username.focus();
				return (false);
	  		}
		 
	  		if (form.password.value == ""){
				alert("Anda belum mengisikan Password.");
				form.password.focus();
				return (false);
	  		}

	  		return (true);

		}
	</script>

</head>

<body class="hold-transition login-page login-style">

    <div class="login-box">

		<div class="login-logo login-logoV2">
			<a target="_blank" href="https://arpateam.com" class="text-danger">
				<p>Portal<b>Admin</b> v2 <small style="font-size: 10px;"><u>By</u></small></p>
				<img src="logoV2.png" alt="Logo #ARPATEAM" class="img-fluid">
			</a>
		</div>
		
		<div class="login-box-body">

			<div class="login-logo">
				<a href="">Portal<b>Admin <br/><?php echo $namaweb; ?></b></a>
			</div>
			<!-- /.login-logo -->

			<form name="login" action="cek_login.php" method="post" onSubmit="return validasi(this)">

				<div class="form-group has-feedback">
					<input type="text" name="username" class="form-control" placeholder="Username" onBlur="if (jQuery(this).val() == &quot;&quot;) { jQuery(this).val(&quot;username&quot;); }" onClick="jQuery(this).val(&quot;&quot;);" >
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				</div>

				<div class="form-group has-feedback">
					<input type="password" name="password" class="form-control" placeholder="Password" onBlur="if (jQuery(this).val() == &quot;&quot;) { jQuery(this).val(&quot;asdf1234&quot;); }" onClick="jQuery(this).val(&quot;&quot;);">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>

				<button type="submit" class="btn btn-block btn-primary">LOGIN <i class="glyphicon glyphicon-log-in"></i></button>

			</form>

		</div>
		<!-- /.login-box-body -->

    </div>
    <!-- /.login-box -->

</body>
</html>