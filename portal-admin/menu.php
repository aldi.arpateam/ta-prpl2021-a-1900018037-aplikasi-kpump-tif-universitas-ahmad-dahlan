
	<ul class="sidebar-menu">

		<li class="header">Menu Utama</li>

		<li class="<?php if($_GET['module']=='home'){echo "active";} ?>"><a href="home"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

		<li class="<?php if($_GET['module']=='mahasiswa-aktif'){echo "active";} ?>"><a href="mahasiswa-aktif"><i class="fa fa-book"></i> <span>Mahasiswa Aktif</span></a></li>

		<li class="treeview <?php if(($_GET['module']=='rekap-partisipasi-pencoblos')OR($_GET['module']=='rekap-hasil-vote')OR($_GET['module']=='rekap-hasil-vote-per-angkatan')){echo "active";} ?>">
			<a href="#">
				<i class="fa fa-book"></i> <span>Rekap Data Pencoblosan</span>
				<i class="fa fa-angle-right pull-right"></i>
			</a>

			<ul class="treeview-menu">
				<li class="<?php if($_GET['module']=='rekap-partisipasi-pencoblos'){echo "active";} ?>"><a href="rekap-partisipasi-pencoblos"><i class="fa fa-book"></i> <span>Rekap Partisipasi Pencoblos</span></a></li>
				<li class="<?php if($_GET['module']=='rekap-hasil-vote'){echo "active";} ?>"><a href="rekap-hasil-vote"><i class="fa fa-book"></i> <span>Rekap Hasil Vote</span></a></li>
				<li class="<?php if($_GET['module']=='rekap-hasil-vote-per-angkatan'){echo "active";} ?>"><a href="rekap-hasil-vote-per-angkatan"><i class="fa fa-book"></i> <span>Rekap Hasil Vote (Per Angkatan)</span></a></li>
			</ul>
		</li>

		<li class="<?php if($_GET['module']=='rekap-kritik-saran'){echo "active";} ?>"><a href="rekap-kritik-saran"><i class="fa fa-book"></i> <span>Rekap Data Kritik & Saran</span></a></li>

		<li class="<?php if(($_GET['module']=='page')AND($_GET['id']=='1')){echo "active";} ?>"><a href="page-edit-1"><i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>Mekanisme</span></a></li>

		<br>

		<li class="header">Fitur Web</li>

		<li class="<?php if(($_GET['module']=='modul')){echo "active";} ?>"><a href="moduls"><i class="fa fa-cogs" aria-hidden="true"></i> <span>Pengaturan Website</span></a></li>

		<li class="<?php if(($_GET['module']=='data-admin')AND($_GET['act']=='list')OR($_GET['module']=='data-admin')AND($_GET['act']=='add')OR($_GET['module']=='data-admin')AND($_GET['act']=='edit')){echo "active";} ?>"><a href="data-admin"><i class="fa fa-user"></i> <span>Data Admin</span></a></li>

	</ul>